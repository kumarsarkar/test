/*
    @Name: Reis_TimezoneEvent_Trigger
    @Description: Trigger for the Event object
    @Dependancies: 
    @Version: 1.0.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Barney Holmes |  Initial
*/

trigger Reis_TimezoneEvent_Trigger on Event (before insert, before update) {

    if (trigger.isUpdate || trigger.isInsert) {
        if (trigger.isBefore) { 
            Reis_SetTimeZoneActivity_Handler.processEvents(trigger.new);  
        }    
    }  
}