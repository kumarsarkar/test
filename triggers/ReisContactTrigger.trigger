trigger ReisContactTrigger on Reis_Contact__c (Before insert, After insert, Before update) {
System.Debug('<<<<TriggerFired>>>>');
     if (trigger.isInsert){
        if(trigger.isBefore){
           REISContactTriggerhandlerClass.HandleBeforeInsert(Trigger.New);
        }
        if(trigger.isAfter){
           REISContactTriggerhandlerClass.HandleAfterInsert(Trigger.NewMap);
        }
    }
    if (trigger.isUpdate){
        if(trigger.isBefore){
           REISContactTriggerhandlerClass.HandleBeforeUpdate(Trigger.New,Trigger.OldMap);
        }
    }
}