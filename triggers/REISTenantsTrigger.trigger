trigger REISTenantsTrigger on Reis_Tenant__c (before insert) {
 System.Debug('<<<<TriggerFired>>>>'+Trigger.NewMap);
     if (trigger.isInsert){
        if(trigger.isBefore){
           REISTenantsTriggerHandlerClass.HandleBeforeInsert(Trigger.New);
        }
    }
}