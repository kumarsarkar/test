trigger QueueTrigger on Queue__c (After Update) {
	QueueTriggerHandlerClass.HandleAfterUpdate(Trigger.NewMap,Trigger.OldMap);
}