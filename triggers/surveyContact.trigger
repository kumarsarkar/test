trigger surveyContact on Survey_Contact__c(After Insert) {
    System.Debug('<<<<TriggerFired>>>>');
     if (trigger.isInsert){
        if(trigger.isAfter){
           SurveyContactTriggerHandlerClass.HandleAfterInsert(Trigger.NewMap);
        }
    }
    
}