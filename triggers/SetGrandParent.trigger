trigger SetGrandParent on Account (before insert, before update) {
    List<ID> parentIDs = new List<ID>();
    Map<ID, ID> acctIDs = new Map<ID, ID>();
    for(Account a: trigger.new) {
        Id oldParentId;
        Id gpID;
        if (Trigger.isUpdate) {
            oldParentId = Trigger.oldMap.get(a.Id).ParentId;
        }
        
        if (a.ParentId != oldParentId) 
        {
            parentIDs.add(a.parentId);
        }
    }

    List<Account> parentAccounts = [Select Id, 						//1
                           	Parent.Id,                              //2
                            Parent.Parent.Id,                       //3
                            Parent.Parent.Parent.Id,                //4
                            Parent.Parent.Parent.Parent.Id,         //5
                            Parent.Parent.Parent.Parent.Parent.Id   //6
                            from Account where Id in :parentIDs];
    
    Map<ID, Account> mParentMaps = new Map<ID, Account>(parentAccounts);
    
    for(Account a: trigger.new) {
        Id oldParentId;
        Id gpID;
        if (Trigger.isUpdate) {
            oldParentId = Trigger.oldMap.get(a.Id).ParentId;
        }
        
        if (a.ParentId != oldParentId) 
        {
            if (mParentMaps.containsKey(a.ParentId)) {
     			Account gpAcct = mParentMaps.get(a.ParentId);

	            gpID = gpAcct.Parent.Parent.Parent.Parent.Parent.Id;         
                // TODO:  What if there are more than 6 levels?
                // Here we can do an "Exception" check and query from this point Up since this is the highest level we got
                system.debug(gpID);
                if (!string.isEmpty(gpID) && !string.isBlank(gpID)) {
                    system.debug(gpID);
                    system.debug('inside if');
					Account highParent = [Select Id, 								//1
                           					Parent.Id,                              //2
			                            	Parent.Parent.Id,                       //3
            			            	    Parent.Parent.Parent.Id,                //4
                        				    Parent.Parent.Parent.Parent.Id,         //5
				                            Parent.Parent.Parent.Parent.Parent.Id   //6
                				            from Account where Id = : gpID LIMIT 1];   
                    
                    string xID = highParent.Parent.Parent.Parent.Parent.Parent.Id;

                    if (string.isEmpty(xID) || string.isBlank(xID)) {
                        xID = highParent.Parent.Parent.Parent.Parent.Id;                
                        if (string.isEmpty(xID) || string.isBlank(xID)) {
                            xID = highParent.Parent.Parent.Parent.Id;                
                            if (string.isEmpty(xID) || string.isBlank(xID)) {
                                xID = highParent.Parent.Parent.Id;                
                                if (string.isEmpty(xID) || string.isBlank(xID)) {
                                    xID = highParent.Parent.Id;                
                                    if (string.isEmpty(xID) || string.isBlank(xID)) {
                                        xID = highParent.Id;                                                   
                                    }                                
                                }                            
                            }
                        }                    
                    }
                    gpID = xID;
                    
                }
                
	            if (string.isEmpty(gpID) || string.isBlank(gpID)) {
		            gpID = gpAcct.Parent.Parent.Parent.Parent.Id;                
		            if (string.isEmpty(gpID) || string.isBlank(gpID)) {
			            gpID = gpAcct.Parent.Parent.Parent.Id;                
	    		        if (string.isEmpty(gpID) || string.isBlank(gpID)) {
				            gpID = gpAcct.Parent.Parent.Id;                
	        			    if (string.isEmpty(gpID) || string.isBlank(gpID)) {
					            gpID = gpAcct.Parent.Id;                
	            				if (string.isEmpty(gpID) || string.isBlank(gpID)) {
						            gpID = gpAcct.Id;                                                   
                				}                                
                			}                            
                		}
        	        }                    
                }

            }
			a.Grandparent__c = gpID;        
            if (string.isEmpty(gpID) || string.isBlank(gpID)) {
                acctIDs.put(a.Id, a.Id);            
            } else {
            	acctIDs.put(a.Id, gpID);                
            }
        }
    }
    
    if (Trigger.isUpdate) {
        if (acctIDs.size() > 0) {
        	UpdateGrandParentOnChildAccounts.UpdateChildAccountsX(acctIDs);    
        }        
    }
}