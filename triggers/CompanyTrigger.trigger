trigger CompanyTrigger on Company__c (Before insert,Before update) {
    
    System.Debug('<<<<TriggerFired>>>>');
     if (trigger.isInsert){
        if(trigger.isBefore){
           REISCompanyTriggerhandlerClass.HandleBeforeInsert(Trigger.New);
        }
    }
    if (trigger.isBefore){
        if (trigger.isInsert || trigger.isUpdate){
            REISCompanyTriggerhandlerClass.HandleAfterInsertAfterUpdate(Trigger.New);
        }
    }
}