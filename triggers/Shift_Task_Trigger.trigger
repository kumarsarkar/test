/*
	@Name: Shift_Task_Trigger
	@Description: Trigger for the Task object
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

trigger Shift_Task_Trigger on Task (after insert, after update, before insert, before update) {

	if (trigger.isUpdate || trigger.isInsert) {
		if (trigger.isAfter) { 
			Shift_MeaningfulActivity_Handler.processTasks(trigger.new);  
		}	 
		Else {
			Shift_MeaningfulActivity_Handler.updateCustomTaskFields(trigger.new);
		}
	}
}