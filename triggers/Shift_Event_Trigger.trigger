/*
	@Name: Shift_Event_Trigger
	@Description: Trigger for the Event object
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

trigger Shift_Event_Trigger on Event (after insert, after update) {

	if (trigger.isUpdate || trigger.isInsert) {
		if (trigger.isAfter) { 
			Shift_MeaningfulActivity_Handler.processEvents(trigger.new);  
		}	 
	}  
}