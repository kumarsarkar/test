Global class DeleteQueueBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
  
    String Sector;
    String QueueConfigId;
    Integer BatchSize;
    String ProcessQuery;String PropertiesQuery;String MetricsQuery;
    Integer Month;
    Integer NoOfDaysinQuarter;
    
    public DeleteQueueBatchClass(Integer NoOfDaysinQuarter,Integer Month,String Sector,String QueueConfigId,Integer BatchSize,String ProcessQuery,String PropertiesQuery,String MetricsQuery){
        this.Sector=Sector;
        this.QueueConfigId=QueueConfigId;
        this.BatchSize=BatchSize;
        this.ProcessQuery=ProcessQuery;
        this.PropertiesQuery=PropertiesQuery;
        this.MetricsQuery=MetricsQuery;
        this.Month=Month;
        this.NoOfDaysinQuarter=NoOfDaysinQuarter;
    }
    
    global Database.querylocator start(Database.BatchableContext BC){
        List<String> StatusList=new List<String>{'\'Pending Assignment\'','\'Pending Creation\''};
        String ProcessQuery='select Id, Batch_Id__c from queue__c where Status__c in '+StatusList+' AND Survey_Sector__r.Name=\''+Sector+'\'';
        system.debug('ProcessQuery=>'+ProcessQuery);
        return Database.getquerylocator(ProcessQuery);
      //  return null;
    }
    
     global void execute(Database.BatchableContext BC, List<sObject> scope){
         system.debug('scopesize=>'+scope.size());
         List<String> QueueIdList=new List<String>();
         for(sObject QueueRecord:scope){
          QueueIdList.add(QueueRecord.Id);
         }
          
         system.debug('QueueIdList=>'+QueueIdList);
         //List<Property_Survey__c> SurveyList=[select Id,(Select Id from Survey_Floor_Plans1__r),(select id from Survey_Senior_Housing_Care_Types__r),(Select Id from Survey_Amenities1__r),(Select Id from survey_contacts__r),(Select Id from Tenants__r), (Select Id from Survey_Student_Housing_Space_Types__r),(Select id from Survey_Retail_Options__r) from property_Survey__c where queue_record__c=:QueueIdList]; 
         
         List<Property_Survey__c> SurveyList=[select Id,(Select Id from Survey_Floor_Plans1__r),(Select Id from Survey_Amenities__r),(Select Id from Survey_Contacts__r),(Select Id from Tenants__r) from property_Survey__c where queue_record__c=:QueueIdList];
        
         List<Survey_Space_Type__c> SurveyFloorPlanList=new List<Survey_Space_Type__c>();
         List<Survey_Amenities__c> SurveyAmenityList=new List<Survey_Amenities__c>();
         List<Survey_Contact__c> SurveyContactList=new List<Survey_Contact__c>();
         //List<Survey_Care_Types__c> SurveyCareTypes = new List<Survey_Care_Types__c>();
         List<Tenants__c> Tenants = new List<Tenants__c>();
        // List<Survey_Student_Housing_Space_Type__c> SurveyStudentHousing = new List<Survey_Student_Housing_Space_Type__c>();
         //List<Survey_Retail_Option__c> SurveyRetailOptions = new List<Survey_Retail_Option__c>();
         
         for(Property_Survey__c PropSurvey:SurveyList){
             SurveyFloorPlanList.addAll(PropSurvey.Survey_Floor_Plans1__r);
             SurveyAmenityList.addAll(PropSurvey.Survey_Amenities__r);
             SurveyContactList.addAll(PropSurvey.Survey_Contacts__r);
             //SurveyCareTypes.addAll(PropSurvey.Survey_Senior_Housing_Care_Types__r);
             Tenants.addAll(PropSurvey.Tenants__r);
             //SurveyStudentHousing.addAll(PropSurvey.Survey_Student_Housing_Space_Types__r);
             //SurveyRetailOptions.addAll(PropSurvey.Survey_Retail_Options__r);
         }
      
         Delete SurveyFloorPlanList;
         Delete SurveyAmenityList;
         Delete SurveyContactList;
         //Delete SurveyCareTypes;
         Delete Tenants;
         //Delete SurveyStudentHousing;
         //Delete SurveyRetailOptions;
         Delete SurveyList;
         Delete scope;
     }
    
     global void finish(Database.BatchableContext BC){
         CountPoolDataBatchClass BatchClass=new CountPoolDataBatchClass(NoOfDaysinQuarter,Month,Sector,QueueConfigId,BatchSize,ProcessQuery,PropertiesQuery,MetricsQuery);
         Database.executeBatch(BatchClass, BatchSize);    
       /*  system.debug('QueueConfigId=>'+QueueConfigId);
         Queue_Configuration__c QueueConfig1=new Queue_Configuration__c();
        	QueueConfig1.BatchJobId__c=null;
        	QueueConfig1.Id=QueueConfigId;
        	update QueueConfig1; 
         system.debug('QueueConfig1=>'+QueueConfig1);*/
     }
}