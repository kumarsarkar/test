/*
	@Name: Shift_OppTeamMember_Test
	@Description: Unit tests for the Shift_OppTeamMember_Handler class
	@Dependancies: Capture Lead Generator Id - Workflow field update to set the lead generator on creation of the lead
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

@isTest
public class Shift_OppTeamMember_Test {
	
	public static testMethod void testCreateOppTeamMembeer() {
		List<Lead> leadList = new List<Lead>();
		List<Database.LeadConvert> leadConversions = new List<Database.LeadConvert>();

		//Get valid lead conversion status
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];		

		//Create and insert a generator and owner user
    	Id Profile1 = userinfo.getProfileId(); 
    	User tempGeneratorUser = new User(
    		Username = 'testUser1@shift-crm.com3332',
    		LastName = 'Last',
    		Firstname = 'First',
    		Alias = 'FLTest',
    		CommunityNickname = 'tesdsst',
    		TimeZoneSidKey = 'America/New_York',
    		LocaleSidKey = 'en_CA',
    		ProfileId = Profile1,
    		LanguageLocaleKey = 'en_US',
    		EmailEncodingKey = 'ISO-8859-1',
    		Email = 'test@shift-crm.com'
    	);
    	insert tempGeneratorUser;	
    	User tempOwnerUser = new User(
    		Username = 'testUser1@shift-crm.com33dss32',
    		LastName = 'Last',
    		Firstname = 'First',
    		Alias = 'FLTest',
    		CommunityNickname = 'teddsst',
    		TimeZoneSidKey = 'America/New_York',
    		LocaleSidKey = 'en_CA',
    		ProfileId = Profile1,
    		LanguageLocaleKey = 'en_US',
    		EmailEncodingKey = 'ISO-8859-1',
    		Email = 'test@shift-crm.com'
    	);
    	insert tempOwnerUser;	    	
    	
		
		//Create 50 leads and insert them
		system.runAs(tempGeneratorUser) {
			for (Integer i=0; i<50; i++) {
				Lead lead = new Lead(
					LastName = 'Test',
					Company = 'TestCo',
					Status = 'Untouched',
					Lead_Generator_Id__c = tempGeneratorUser.Id
				);
				leadList.add(lead);
			}
			insert leadList;		
		}
		
		//Set the new ownerid (after workflow has fired) and add record to list of lead converts
		for (Lead lead :leadList) {
			lead.ownerId = tempOwnerUser.Id;
			
			Database.LeadConvert lc = new database.LeadConvert();				
			lc.setLeadId( lead.Id );
			lc.setConvertedStatus( convertStatus.MasterLabel );			
			leadConversions.add( lc );
		}
		update leadList;
		
		//Convert the leads
		List<Database.LeadConvertResult> results = Database.convertLead( leadConversions );
		
		//Assert all the leads were converted, and opps created with the lead generator id populated
		List<Opportunity> assertOppList = [SELECT Id FROM Opportunity WHERE Lead_GeneratorId__c = :tempGeneratorUser.Id];		
		system.assertEquals(50, assertOppList.size());

		//Assert that all opportunity team members were created		
		List<OpportunityTeamMember> assertOppTeamList = [SELECT Id FROM OpportunityTeamMember WHERE userId = :tempGeneratorUser.Id]; 
		system.assertEquals(50, assertOppList.size());	
	
	}    		

}