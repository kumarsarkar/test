@isTest(seeAllData=true)
private class AccountConsoleTest {

    static testMethod void myUnitTest() {
    	Profile tstPrfl = [Select Id from Profile WHERE Name='System Administrator'];
   		User tstUser = New User();
   		
    	tstUser.LastName = 'Test';
    	tstUser.Email = 'test@test.com';
    	tstUser.Alias = 'tst';
    	tstUser.CommunityNickname = 'tst user';
    	tstUser.UserName = 'sttest@testx4r.com';
    	tstUser.LocaleSidKey = 'en_US';
    	tstUser.EmailEncodingkey = 'ISO-8859-1';
    	tstUser.ProfileId = tstPrfl.Id;
    	tstUser.LastName = 'Test';
    	tstUser.LanguageLocaleKey = 'en_US';
    	tstUser.TimeZoneSidKey = 'America/New_York';
		tstUser.IsActive = true;
		//tstUser.UserRoleId = rolevp.id;
    	Insert tstUser;
    	
    	Account tstAcct = New Account();
    	tstAcct.Name = 'Test Account';
    	tstAcct.OwnerId = tstUser.Id;
    	Insert tstAcct;
    	
    	Contact tstCntc = New Contact();
    	tstCntc.AccountId = tstAcct.Id;
    	tstCntc.LastName = 'Test';
    	//tstCntc.LastActivityDate = Date.Today();
    	Insert tstCntc;
    	
    	Task tstTask1 = New Task();
    	tstTask1.Description = 'Test Task1';
    	tstTask1.WhatId = tstAcct.Id;
    	tstTask1.Status = 'Completed';
    	Insert tstTask1;
    	
    	Task tstTask2 = New Task();
    	tstTask2.Description = 'Test Task2';
    	tstTask2.WhatId = tstAcct.Id;
    	tstTask2.WhoId = tstCntc.Id;
    	tstTask2.Status = 'Completed';
    	Insert tstTask2;
    	
    	Contact tstCntc2 = New Contact();
    	tstCntc2.AccountId = tstAcct.Id;
    	tstCntc2.LastName = 'Test';
    	Insert tstCntc2;
    	
    	Test.startTest();
    		Test.setCurrentPage(Page.AcctConsole);
		    ApexPages.StandardController std = new ApexPages.StandardController(tstAcct);
		    AccountActivity ctrlr = new AccountActivity(std);
		    ctrlr.selCntct = tstCntc;
		    ctrlr.getHist();
		    ctrlr.getChildren();
		    ctrlr.getHistWithContact();
		    ctrlr.getAcctOpps();
		    ctrlr.getCntcHist();
		    ctrlr.populateContactHist();
		    ApexPages.currentPage().getParameters().put('showCurCntcHist',tstTask2.id);
		    ctrlr.ShowCurContactHist();
		    ctrlr.getCntctSelHist();
		    ApexPages.currentPage().getParameters().put('showCurActHist',tstTask2.id);
		    ctrlr.ShowCurAccountHist();
		    ctrlr.getAcctSelHist();
		    ctrlr.getTotal_size();
		    ctrlr.AcctLVM();
		    ctrlr.editAccountHist();
		    ctrlr.createAcctTask();
		    ctrlr.getnewHistpage();
		    ctrlr.ViewContact();
		    ctrlr.getCntc_Total_size();
		    ctrlr.getCntcHist_Total_size();
		    ApexPages.currentPage().getParameters().put('showCurCntHist',tstTask2.id);
		    ctrlr.editContactHist();
		    ctrlr.CntchistLVM();
		    ctrlr.createCntctTask();
		    ctrlr.getOpp_Total_size();
		    ctrlr.selCntct = tstCntc;	
		    ctrlr.getCntcHist();
		    ctrlr.populateContactHist();
		    ctrlr.ShowCurContactHist();
		    ctrlr.getCntctSelHist();	    
	    Test.stopTest();
    }
}