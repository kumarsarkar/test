global class UpdateGrandParentOnChildAccounts {
    private static List<Account> acctToUpdate = new List<Account>();
   
	@future
    public static void UpdateChildAccountsX(Map<ID, ID> parentIDs) {
        for(Id acctID : parentIDs.keySet()) {
            ID gp = parentIDs.get(acctID);
            UpdateChildren(acctID, gp);
            update acctToUpdate;
        }        
    }    
    
    private static void UpdateChildren(ID acctID, ID GrandParentID){
        List<Account> childAccts = [Select Id, Grandparent__c from Account where ParentId = :acctID];
        for(Account c : childAccts) {
            c.Grandparent__c = GrandParentID;
            acctToUpdate.add(c);
            UpdateChildren(c.Id, GrandParentID);
        }
    }    
}