public with sharing class BuildQueue {
    
    private string qconfigId = '';
    public Queue_Configuration__c qConfigItem{get;set;}
    public string[] sector{get;set;}
    private ApexPages.StandardController controller;
    public Decimal QueueConfig_Count1;
    List<Queue_Configuration__c> qconfigList;
    Map<String,Map<String,String>> MarketExceptionMap;
    Map<String,Map<String,String>> SubMarketExceptionMap;
    Map<String,Map<String,Decimal>> ExceptionMap;
    public String BatchJobId{get;set;}
    
    public BuildQueue(ApexPages.StandardController controller) {
        this.controller = controller;
        qconfigList = new List<Queue_Configuration__c>();
        qconfigId = System.currentPageReference().getParameters().get('qId');
        qconfigList = [Select ID, Sector__c,BatchJobId__c,Month_Start_Date__c From Queue_Configuration__c Where ID = :controller.getId()];
        
        if(!qconfigList.isEmpty()){
            qConfigItem = qconfigList[0]; 
            sector = qConfigItem.Sector__c.split(';');
            BatchJobId=qConfigItem.BatchJobId__c;
            
            system.debug('Sector' + sector);
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a queue and retry.');
            ApexPages.addMessage(msg);
        }
    }
    
    public PageReference buildingQueue() {
        PageReference page = new PageReference('/' + controller.getId());
        String WhereClause='';
        //Anantha 28/07/2017 RSI-416
        if(BatchJobId!=null && BatchJobId!=''){
     //       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There is already a batch that is in progress. Please try after sometime.');
     //       ApexPages.addMessage(msg);
              throw new REISException('There is already a batch that is in progress. Please try after sometime.');
        }
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c,Do_Not_Call__c,IAG__c,CCRC__c,Client_Services__c,QA__c,Non_Published_MSAs__c,New_Construction__c from Ops_Pool_Data__c ';
        List<CurrentDate_Setting__c> CurrentDateSetting=[SELeCT id, Name,ActiveFlag__c,currentdate__c FROM CurrentDate_Setting__c];
        Date CurrentDate;
        if(CurrentDateSetting.size()>0){
            if(CurrentDateSetting[0].ActiveFlag__c){
               CurrentDate=CurrentDateSetting[0].currentDate__c; 
            }else{
                CurrentDate=Date.today();
            }
        }else{
            CurrentDate=Date.today();
        }
        
       // Integer Month=Date.today().Month();
        Integer Month=CurrentDate.Month();
        Integer BatchSize=Integer.valueOf(BatchSize_Setting__c.getValues('BuildQueueBatch').Size__c);
        //Integer Year=Date.today().Year();
        Integer Year=CurrentDate.Year();
        Integer QuarterStartMonth;
        Integer QuarterStartYear=Year;
        String PropertyStatus='Completed';
        for(String s:sector){
            system.debug('Sector=>'+s);
            if(qConfigItem.Month_Start_Date__c!=1){
                if(CurrentDate.Day()<qConfigItem.Month_Start_Date__c){
                    if(Month==1){
                        Month=12;
                        Year=Year-1;
                    }else{
                        Month-=1;
  //                        Year=Year;
                    }
                }else{
  //                  Year=Date.today().Year();
                }
                if(Month==12 || Month==1 || Month==2){
                    QuarterStartMonth=12;
                    if(Month==12){
                    	QuarterStartYear=Year;    
                    }else{
                    	QuarterStartYear-=1;    
                    }
                    
                    Month=Month==12?1:Month+1;
                }else if(Month==3 || Month==4 || Month==5){
                    QuarterStartMonth=3;
                    Month+=1;
                }else if(Month==6 || Month==7 || Month==8){
                    QuarterStartMonth=6;
                    Month+=1;
                }else if(Month==9 || Month==10 || Month==11){
                    QuarterStartMonth=9;
                    Month+=1;
                }
            }else{
                if(Month==1 || Month==2 || Month==3){
                    QuarterStartMonth=1;
                }else if(Month==4 || Month==5 || Month==6){
                    QuarterStartMonth=4;
                }else if(Month==7 || Month==8 || Month==9){
                    QuarterStartMonth=7;
                }else if(Month==10 || Month==11 || Month==12){
                    QuarterStartMonth=10;
                }
            }
            
           	Date StartDate=Date.newInstance(QuarterStartYear, QuarterStartMonth, Integer.valueOf(qConfigItem.Month_Start_Date__c));
    //        Integer NoOfDaysinQuarter=StartDate.daysBetween(Date.today());
    		  Integer NoOfDaysinQuarter=StartDate.daysBetween(CurrentDate);
            
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' AND Property_Status__c=\''+PropertyStatus+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
        	String MetricCountQuery='Select Id,survey_completed__c,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c,Rent__c,Vacancy__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' AND createddate=LAST_N_DAYS:'+String.valueOf(NoOfDaysinQuarter)+' AND Include_In_Coverage__c=true Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
        //      String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' AND Property_Status__c=\''+PropertyStatus+'\' and msa__c in (\'7B2BEED3-781F-49D5-820A-B5073E012C4C\',\'9909A60C-DF3C-45F9-ADC4-9D290FBCC823\') AND SUB_MARKET_ID__C IN (\'75ED1FD0-4DBC-4DBA-A6E5-22CA96376F3A\',\'7FACBBAA-BE8E-4E03-AD8F-D67CB2289142\',\'54F8A354-65D0-4EE3-BBA2-B1B7D8B42E9C\',\'613605AF-0E03-4D91-92D9-AB9E74281D85\') Order By sector__c,MSA__C,Sub_Market_Id__C';
        //    WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' and msa__c in (\'7B2BEED3-781F-49D5-820A-B5073E012C4C\',\'9909A60C-DF3C-45F9-ADC4-9D290FBCC823\') AND SUB_MARKET_ID__C IN (\'75ED1FD0-4DBC-4DBA-A6E5-22CA96376F3A\',\'7FACBBAA-BE8E-4E03-AD8F-D67CB2289142\',\'54F8A354-65D0-4EE3-BBA2-B1B7D8B42E9C\',\'613605AF-0E03-4D91-92D9-AB9E74281D85\') AND Sector__c=\''+s+'\' AND  (Next_Queue_Date__c=null OR Next_Queue_Date__c<=Today)'   ;
            WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\' AND  (Next_Queue_Date__c=null OR Next_Queue_Date__c<=Today)'   ;  // AND SUB_MARKET_ID__C IN (\'0C37728C-484B-4BB4-95ED-2C12AB295F5D\')
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('ProcessQuery=>'+SearchQuery);
            system.debug('PropertiesCountQuery=>'+PropertiesCountQuery);
            system.debug('MetricCountQuery'+MetricCountQuery);
            system.debug('Month'+Month);
            system.debug('StartDate'+StartDate);
            system.debug('NoOfDaysinQuarter'+NoOfDaysinQuarter);
            // Anantha 05/09 RSI-523
         //   CountPoolDataBatchClass BatchClass=new CountPoolDataBatchClass(NoOfDaysinQuarter,Month,s,controller.getId(),BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
         	DeleteQueueBatchClass BatchClass=new DeleteQueueBatchClass(NoOfDaysinQuarter,Month,s,controller.getId(),BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);   
            Database.executeBatch(BatchClass, Integer.valueOf(BatchSize_Setting__c.getValues('DeleteQueueBatch-'+s).Size__c)); 
        }
        return page;
    }
    
}