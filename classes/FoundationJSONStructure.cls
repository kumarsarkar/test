public class FoundationJSONStructure{
    Public PropertyBuilding buildingField;
    Public PropertySurveyData surveyDataField;
    Public PropertySectorSurveyData sectorSurveyDataField;
    Public PropertySectorSpaceData sectorSpaceDataField;
    Public PropertyNewConstructionSurvey newConstructionSurveyField;
    Public PropertySectorSurveySpaceData sectorSurveySpaceDataField;
    Public PropertyAddress addressField;
    Public PropertyCont[] contactField;
    Public PropertyInsur[] insuranceField;
    Public PropertyAmen[] amenitiesField;
    Public PropertyLeasingIncentives leasingIncentivesField;
    Public PropertyNote[] notesField;
    Public PropertyTenant[] tenantsField;
    Public string surveyField;
    Public string surveySpaceField;
    
    public class PropertyBuilding{
        Public PropertyBuildingBld bldField;
    }
    public class SurveyCodeOutFieldType{}
    public class PropertySurveyData{
        Public String contactOidField;
        Public String surveyDateField;
        Public String surveyQuarterField;
        Public String surveySourceField;
        Public String websiteField;
        Public Integer isTermsGridField;
        Public String surveyorField;
        Public SurveyCodeOutFieldType surveyCodeOutField;
        Public Integer rentalRateRefusedField;
        Public Integer totalVacancyRefusedField;
        Public Integer missingInfoField;
        Public Integer contactRefusedSurveyField;
        Public Integer allInfoNegotiableField;
        Public String qcNoteField;
        Public Integer isNewConstructionField;
        Public Integer incompleteField;
        Public String propertyNoteField;
        Public Integer hasRenovationNoteField;
        Public Integer forSaleOrSoldField;
        Public String saleDateField;
        Public SalePriceFieldType salePricefield;
    }
    public class SalePriceFieldType{}
    
    Public Class PropertySectorSurveyData {
        Public PropertySectorSurveyDataStoSurveyData stoSurveyDataField;
        Public PropertySectorSurveyDataSthSurveyData sthSurveyDataField;
        Public PropertySectorSurveyDataSnrSurveyData snrSurveyDataField;
        Public PropertySectorSurveyDataRetSurveyData retSurveyDataField;
        Public PropertySectorSurveyDataIndSurveyData indSurveyDataField;
        Public PropertySectorSurveyDataOffSurveyData offSurveyDataField;
        Public PropertySectorSurveyDataAptSurveyData aptSurveyDataField;
    }
    Public Class PropertySectorSpaceData {
        Public PropertySectorSpaceDataStoSpaceData[] stoSpaceDataField;
        Public PropertySectorSpaceDataSthSpaceData[] sthSpaceDataField;
        Public PropertySectorSpaceDataSnrSpaceData[] snrSpaceDataField;
        Public PropertySectorSpaceDataRetSpaceData[] retSpaceDataField;
        Public PropertySectorSpaceDataIndSpaceData indSpaceDataField;
        Public PropertySectorSpaceDataOffSpaceData offSpaceDataField;
        Public PropertySectorSpaceDataAptSpaceData[] aptSpaceDataField;
    }
    Public Class PropertyNewConstructionSurvey{
        Public PropertyNewConstructionSurveyNC ncField;
    }
    Public Class PropertySectorSurveySpaceData{
        Public PropertySectorSurveySpaceDataStoSurveySpaceData[] stoSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataSthSurveySpaceData[] sthSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataSnrSurveySpaceData[] snrSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataRetSurveySpaceData retSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataIndSurveySpaceData indSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataOffSurveySpaceData offSurveySpaceDataField;
        Public PropertySectorSurveySpaceDataAptSurveySpaceData[] aptSurveySpaceDataField;
    }
    Public Class PropertyAddress{
        Public PropertyAddressAdd addField;
    }
    Public Class PropertyCont{
        Public String contactOidField;
        Public String titleField;
        Public String firstNameField;
        Public String lastNameField;
        Public String workPhoneField;
        Public String extField;
        Public String mobilePhoneField;
        Public String miscPhoneField;
        Public String faxField;
        Public String eMailField;
        Public String webSiteField;
        Public String noteField;
        Public String sourceField;
        Public String contactSourceField;
        Public String nameField;
        Public String contactDateField;
        Public Integer doNotCallField;
        Public String jobRoleOidField;
        Public String jobRoleField;
        Public String companyOidField;
        Public String companyNameField;
        Public Integer companyDoNotCallField;
        Public String companyDoNotCallDateField;
        Public String companyMainPhoneField;
        Public String companyWebSiteField;
    }
    Public Class PropertyInsur{
        Public Integer insuranceOidField;
        Public String insuranceField;
        Public String selectedField;
        Public Integer insurancePropertyOidField;
        Public Boolean insurancePropertyOidFieldSpecified;
    }
    Public Class PropertyAmen{
        Public Integer amenityPropertyOidField;
        Public Integer amenityOidField;
        Public Integer hasQuantityField;
        Public String descriptionField;
        Public String quantityField;
        Public String selectedField;
        Public String amenityTypeField;
    }
    Public Class PropertyLeasingIncentives{
        Public PropertyLeasingIncentivesLeasingIncentive leasingIncentiveField;
    }
    Public Class PropertyNote{
        Public String noteTypField;
        Public String createDateField;
        Public Integer monthField;
        Public Integer yearField;
        Public Integer isExpansionField;
        Public String notesField;
    }
    Public Class PropertyTenant{
        Public String tenantPropertyOidField;
        Public String tenantOidField;
        Public String nameField;
        Public String sizeField;
        Public String groundLeaseField;
        Public String ownedLeasedField;
        Public String anchField;
        Public String outparcelField;
        Public String tenantTypeField;
    }
    Public Class PropertyBuildingBld{
        Public String propertyOidField;
        Public String propertyIdField;
        Public String nameField;
        Public String sectorField;
        Public String sectorTitleField;
        Public String competativeField;
        Public String noteField;
        Public String enterDateField;
        Public String lastUpdateField;
        Public String printDateField;
        Public String classField;
        Public String websiteField;
        Public Integer yrBuiltField;
        Public Integer monthBuiltField;
        Public Integer estComYrBuiltField;
        Public Integer estComMonthBuiltField;
        Public String estimateComStatusField;
        Public Integer conStartYrField;
        Public Integer conStartMonthField;
        Public String conStatusField;
        Public String ownerField;
        Public String developerField;
        Public String selectCodeField;
        Public String selectCodeDescriptionField;
        Public Integer finalNCStampField;
        Public String reitCodeField;
        Public String reitField;
        Public String propertyTypeCodeField;
        Public String propertyTypeField;
        Public String propStatusField;
        Public PropertySubTypeFieldType propertySubTypeField;
        Public PortfolioFieldType portfolioField;
        Public PropertyBuildingBldStoDetails stoDetailsField;
        Public PropertyBuildingBldSthDetails sthDetailsField;
        Public PropertyBuildingBldSnrDetails snrDetailsField;
        Public PropertyBuildingBldRetDetails retDetailsField;
        Public PropertyBuildingBldIndDetails indDetailsField;
        Public PropertyBuildingBldOffDetails offDetailsField;
        Public PropertyBuildingBldAptDetails aptDetailsField;
    }
    public class PropertySubTypeFieldType{}
    public class PortfolioFieldType{}
    
    Public Class PropertySectorSurveyDataStoSurveyData{
        Public Integer totalCCAvailableField;
        Public Integer totalNCCAvailableField;
        Public Integer moveInField;
        Public Integer moveOutField;
        Public Integer totalAvailableField;
        Public Integer freeRentField;
        Public QcNoteFieldType qcnoteField;
        Public RentForgoneFieldType rentForgoneField;
        Public Integer unitsReceivedFreeRentField;
    }
    public class QcNoteFieldType{}
    public class RentForgoneFieldType{}
    
    Public Class PropertySectorSurveyDataSthSurveyData{
        Public Integer averageRentField;
        Public String recallDateField;
        Public NewTentantsFieldType newTentantsField;
        Public Integer percentStudentsField;
        Public PercentPreleasedFieldType percentPreleasedField;
        Public Integer monthsFreeRentField;
        Public Integer percentRentUtilitiesField;
        Public Integer totalVacantBedsField;
        Public Integer totalVacantUnitsField;
        Public Integer leaseTermField;
        Public Integer freeRentPercentField;
        Public BaseRentFieldType baseRentField;
        Public ReducedLeaseTermFieldType reducedLeaseTermField;
        Public ReducedRentDiscountFieldType reducedRentDiscountField;
        Public ReducedFreeRentPercentFieldType reducedFreeRentPercentField;
        Public Integer preleaseField;
        Public String rentTypeField;
    }
    public class NewTentantsFieldType{}
    public class PercentPreleasedFieldType{}
    public class BaseRentFieldType{}
    public class ReducedLeaseTermFieldType{}
    public class ReducedRentDiscountFieldType{}
    public class ReducedFreeRentPercentFieldType{}
    
    Public Class PropertySectorSurveyDataSnrSurveyData{
        Public Integer totalAvailableField;
        Public String annualRentSetDayMonthField;
        Public Integer entryFeeLowField;
        Public Integer entryFeeHighField;
        Public String entryFeeAvgField;
        Public String expectedPercentIncreaseField;
        Public InsuranceAcceptedFieldType insuranceAcceptedField;
        Public Integer holdField;
        Public Integer availableField;
    }
    public class InsuranceAcceptedFieldType{}
    
    public class AverageRentFieldType{}
    public class GroundAverageRentFieldType{}
    public class GroundRentHighFieldType{}
    public class GroundRentLowFieldType{}
    public class GroundLeaseTermFieldType{}
    
    public class OutPclAverageRentFieldType{}
    public class OutPclRentLowFieldType{}
    public class RentLowFieldType{}
    public class RentHighFieldType{}
    public class AnchorSizeFieldType{}
    public class NonAnchorSizeFieldType{}
    public class LeaseTermFieldType{}
    public class AvgFlagFieldType{}
    public class RentTermFieldTYpe{}
    public class OutPclRentHighFieldType{}
    public class RenewalCommissionAmountFieldType{}
    public class AnchorCommissionAmountFieldType{}
    public class NonAnchorCommissionAmountFieldType{}
    Public Class EstSizeFieldType{}
    Public Class PctRentFieldType{}
    Public Class FoodAvailFieldType{}
    Public Class PassThruFieldType{}
    Public Class FoodLeaseTermFieldType{}
    Public Class OutPclLeaseTermFieldType{}
    Public Class FoodRentLowFieldType{}
    Public Class FoodRentHighFieldType{}
    Public Class FoodRentAvgFieldType{}
    Public Class CrdAnchFieldType{}
    Public Class PtCodeFieldType{}
    
    
    Public Class PropertySectorSurveyDataRetSurveyData{
        Public Integer totalSizeField;
        Public AverageRentFieldType averageRentField;
        Public String operatingExpenseField;
        Public String reTaxField;
        Public String freeRentAnchorField;
        Public String freeRentNonAnchorField;
        Public String anchorCommissionPercentField;
        Public Integer amountField;
        Public String groundAvailField;
        Public String tiCodeField;
        Public GroundAverageRentFieldType groundAverageRentField;
        Public GroundRentHighFieldType groundRentHighField;
        Public GroundRentLowFieldType groundRentLowField;
        Public GroundLeaseTermFieldType groundLeaseTermField;
        
        Public String anchorLeaseTermField;
        Public String nonAnchorLeaseTermField;
        Public String anchRentAvgField;
        Public String anchRentHighField;
        Public String anchRentLowField;
        Public String nonAnchRentAvgField;
        Public String nonAnchRentHighField;
        Public String nonAnchRentLowField;
        Public String outPclAvailField;
        Public OutPclAverageRentFieldType outPclAverageRentField;
        Public OutPclRentLowFieldType outPclRentLowType;
        Public RentLowFieldType rentLowField;
        Public RentHighFieldType rentHighField;
        
        Public Integer sizeField;
        
        Public AnchorSizeFieldType anchorSizeField;
        Public NonAnchorSizeFieldType nonAnchorSizeField;
        Public LeaseTermFieldType leaseTermField;
        
        Public String tiAnchorField;
        Public String tiNonAnchorField;
        Public String contractRentNonAnchorField;
        Public String contractRentAnchorField;
        
        Public AvgFlagFieldType avgFlagField;
        Public RentTermFieldType rentTermField;
        Public Integer dAvgFlagField;
        Public Integer sAvgFlagField;
        
        Public OutPclRentHighFieldType outPclRentHighField;
        Public RenewalCommissionAmountFieldType renewalCommissionAmountField;
        Public AnchorCommissionAmountFieldType anchorCommissionAmountField;
        Public NonAnchorCommissionAmountFieldType nonAnchorCommissionAmountField;
        Public String avgNonAnchFlagField;
        Public String avgAnchFlagField;
        Public EstSizeFieldType estSizeField;
        Public Integer availNonAnchorField;
        Public Integer availAnchorField;
        Public String nonAnchorCommissionPercentField;
        Public PctRentFieldType pctRentField;
        Public String rentBasisField;
        Public String commonAreaMaintField;
        Public FoodAvailFieldType foodAvailFood;
        
        Public PassThruFieldType passThruField;
        Public String renewalCommissionPercentField;
        Public FoodLeaseTermFieldType foodLeaseTermField;
        Public OutPclLeaseTermFieldType outPclLeaseTermField;
        Public FoodRentLowFieldType foodRentLowField;
        Public FoodRentHighFieldType foodRentHighFieldField;
        Public FoodRentAvgFieldType foodRentAvgField;
        Public CrdAnchFieldType crdAnchFieldField;
        Public PtCodeFieldType ptCodeField;
    }
    Public Class LossFactorFieldType{}
    Public Class RawTiFieldType{}
    Public Class PropertySectorSurveyDataIndSurveyData{
        Public LossFactorFieldType lossFactorField;
        Public Integer passThroughField;
        Public Integer elecField;
        Public Integer janitorialField;
        Public Integer leaseField;
        Public RawTiFieldType rawField;
        Public Integer newTiField;
        Public Integer renewalField;
        Public Integer monthsField;
        Public Integer contactRentField;
        Public Integer commissionNewField;
        Public Integer commissionRenewField;
        Public QcNoteFieldType qcNoteField;
        Public Integer isSubletOnlyField;
    }
    
    Public Class PropertySectorSurveyDataOffSurveyData{
        Public Integer elecField;
        Public Integer janitorialField;
        Public Integer leaseField;
        Public RawTiFieldType rawField;
        Public String newTiField;
        Public String renewalField;
        Public String monthsField;
        Public String contactRentField;
        Public Integer commissionNewField;
        Public Integer commissionRenewField;
        Public LossFactorFieldType lossFactorField;
        Public Integer passThroughField;
        Public Integer escalatorField;
        Public Integer isSubletOnlyField;
    }
    
    
    Public Class PropertySectorSurveyDataAptSurveyData{
        Public String freeRentField;
        Public Integer isAffordableHousingSurveyField;
        Public String lihtcFreeRentField;
        Public PercentPreleasedFieldType percentPreleaseField;
        Public Integer totalVacantUnitsField;
    }
    Public Class PropertySectorSpaceDataStoSpaceData{
        Public String spaceOidField;
        Public String descriptionField;
        Public Integer lengthField;
        Public Integer widthField;
        Public String heightField;
        Public Integer quantityField;
        Public String dimensionsField;
        Public Integer climateControlledField;
        Public String sqftField;
        Public Integer floorField;
        Public Integer rentableField;
        Public Integer boatRVField;
        Public Integer elevatorAccessField;
        Public Integer driveUpAccessField;
        Public Integer insideField;
        Public String estimateStatusField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public String spaceTypeField;
        
    }
    Public Class PropertySectorSpaceDataSthSpaceData{
        Public String spaceOidField;
        Public String descriptionField;
        Public Integer totalUnitsField;
        Public Integer totalBedsField;
        Public String spaceTypeField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public String estimateStatusField;
        Public Integer sQFTLowField;
        Public Integer sQFTHighField;
        Public String sQFTAvgField;
        Public String bathParityField;
        Public Integer brSQFTLowField;
        Public Integer brSQFTHighField;
        Public Integer brSQFTAvgField;
        
    }
    
    Public Class PropertySectorSpaceDataSnrSpaceData{
        Public String spaceOidField;
        Public String descriptionField;
        Public Integer unitsField;
        Public Integer sQFTHighField;
        Public Integer sQFTLowField;
        Public String sQFTAvgField;
        Public String estimateStatusField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public Integer hasUnitsField;
        Public String spaceTypeField;
        
    }
    Public Class StringnonAnchorSizeFieldType{}
    Public Class PropertySectorSpaceDataRetSpaceData{
        Public String spaceOidField;
        Public String estimateStatusField;
        Public String propertyStatusField;
        Public Integer descriptionField;
        Public Integer totalField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public Integer rentableField;
        Public Integer isOutParcelField;
        Public String spaceTypeField;
        Public String anchorSizeField;
        Public StringnonAnchorSizeFieldType stringnonAnchorSizeField;
        Public Integer yearBuiltField;
        Public Integer monthBuiltField;
        Public Integer groundBreakYearField;
        Public Integer groundBreakMonthField;
        Public Integer outParcelField;
        Public Integer foodCourtField;
        
    }
    Public Class PropertySectorSpaceDataIndSpaceData{
        Public String spaceOidField;
        Public Integer descriptionField;
        Public Integer totalField;
        Public Integer rentableField;
        Public String estimateStatusField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public String spaceTypeField;
    }
    Public Class PropertySectorSpaceDataOffSpaceData{
        Public String spaceOidField;
        Public Integer descriptionField;
        Public Integer totalField;
        Public Integer rentableField;
        Public String estimateStatusField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String groundBreakDateField;
        Public String startDateField;
        Public String spaceTypeField;
    }
    Public Class PropertySectorSpaceDataAptSpaceData{
        Public String spaceOidField;
        Public String descriptionField;
        Public Integer compTotalField;
        Public Integer nonCompTotalField;
        Public Integer unitsField;
        Public Integer sqFtField;
        Public String spaceTypeField;
        Public Integer expansionField;
        Public Integer verifiedField;
        Public String expansionDateField;
        Public String startDateField;
        Public String estimateStatusField;
        Public Integer sQFTLowField;
        Public Integer sQFTHighField;
        Public String sQFTAvgField;
        Public Integer ami30Field;
        Public Integer ami40Field;
        Public Integer ami50Field;
        Public Integer ami60Field;
        Public Integer secEightUnitsField;
        Public Integer totalUnitsField;
        Public Integer ami70Field;
        Public Integer amiNonConventionalField;
        Public String utilityCapField;
        Public Integer hasAmi30Field;
        Public Integer hasAmi40Field;
        Public Integer hasAmi50Field;
        Public Integer hasAmi60Field;
        Public Integer hasAmi70Field;
        Public Integer hasAmiNonConventionalField;
        Public Integer unknownAmiField;
        Public Integer hasUnknownAmiField;
        Public Integer hasSecEightUnitsField;
        Public Integer hasUtilityCapField;
        Public Integer lihtcSqftLowField;
        Public Integer lihtcSqftHighField;
        Public String lihtcSQFTAvgField;
        Public Integer secEightSqftLowField;
        Public Integer secEightSqftHighField;
        Public String secEightSQFTAvgField;
        
    }
    Public Class PropertyNewConstructionSurveyNC{
        Public String nCOidField;
        Public Integer completeMonthField;
        Public String completeYearField;
        Public Integer startMonthField;
        Public Integer startYearField;
        Public String startEstimateField;
        Public String completeEstimateField;
        Public String percentPreleasedField;
        Public String rentTypeField;
        Public String unitsAvailableField;
        Public String propertyStatusField;
        Public String proprtyStatusDescField;
    }
    
    Public Class PropertySectorSurveySpaceDataStoSurveySpaceData{
        Public String spaceOidField;
        Public Integer directAvailField;
        Public String directHighField;
        Public String directLowField;
        Public String directAvgField;
        Public String dAvgFlagField;
        Public Decimal vacancyRateField;
        Public String streetRentField;
        Public Integer vacantUnitsField;
        Public Integer modeField;
        Public Integer quantityField;
    }
    Public Class PropertySectorSurveySpaceDataSthSurveySpaceData{
        Public String spaceOidField;
        Public Integer vacantUnitsField;
        Public Integer vacantBedsField;
        Public String currentRentLowField;
        Public String currentRentHighField;
        Public Decimal currentRentAvgField;
        Public Integer currentPeriodIndexField;
        Public Integer currentPeriodYearField;
        Public String futureRentLowField;
        Public String futureRentHighField;
        Public String futureRentAvgField;
        Public Integer futurePeriodIndexField;
        Public Integer futurePeriodYearField;
        Public String currentRentAvgFlagField;
        Public String futureRentAvgFlagField;
    }
    Public Class PropertySectorSurveySpaceDataSnrSurveySpaceData{
        Public String spaceOidField;
        Public Integer availField;
        Public String avgFlagField;
        Public Integer marketRentHighField;
        Public Integer marketRentLowField;
        Public String marketRentAvgField;
        Public Integer baseRentLowField;
        Public Integer baseRentHighField;
        Public String baseRentAvgField;
        Public String baseAvgFlagField;
        Public String totalAvgField;
        Public Integer holdField;
    }
    Public Class PropertySectorSurveySpaceDataRetSurveySpaceData{
        Public String spaceOidField;
        Public Decimal availField;
        Public Decimal rentLowField;
        Public Decimal rentHighField;
        Public Decimal averageRentField;
        Public Integer sizeField;
        Public Decimal leaseTermField;
        Public Decimal tiField;
        Public Integer tiCodeField;
        Public Integer avgFlagField;
        Public Integer rentTermField;
        Public Integer dAvgFlagField;
        Public Integer sAvgFlagField;
    }
    Public Class PropertySectorSurveySpaceDataIndSurveySpaceData{
        Public String spaceOidField;
        Public Integer directAvailField;
        Public Integer directContigField;
        Public Decimal directLowField;
        Public Decimal directHighField;
        Public Decimal directAvgField;
        Public Integer subletAvailField;
        Public String subletLowField;
        Public String subletHighField;
        Public String subletAvgField;
        Public String grossField;
        Public Decimal operatingField;
        Public String taxesField;
        Public String rentTermField;
        Public String dAvgFlagField;
        Public String sAvgFlagField;
        Public Integer directVacancyRateField;
        Public Integer subletVacancyRateField;
    }
    Public Class PropertySectorSurveySpaceDataOffSurveySpaceData{
        Public String spaceOidField;
        Public Integer directAvailField;
        Public Integer directContigField;
        Public String directLowField;
        Public String directHighField;
        Public Decimal directAvgField;
        Public Integer subletAvailField;
        Public String subletLowField;
        Public String subletHighField;
        Public String subletAvgField;
        Public String grossField;
        Public String operatingField;
        Public String taxesField;
        Public String rentTermField;
        Public String dAvgFlagField;
        Public String sAvgFlagField;
        Public Decimal directVacancyRateField;
        Public Integer subletVacancyRateField;
    }
    Public Class PropertySectorSurveySpaceDataAptSurveySpaceData{
        Public String spaceOidField;
        Public Integer unitsField;
        Public String sizeField;
        Public Integer availField;
        Public String avgFlagField;
        Public String leaseTermField;
        Public Integer monthsField;
        Public String discountField;
        Public Decimal averageRentField;
        Public Integer vacantField;
        Public String rentLowField;
        Public String rentHighField;
        Public String sec8RentAvgField;
        Public String ami30RentAvgField;
        Public String ami40RentAvgField;
        Public String ami50RentAvgField;
        Public String ami60RentAvgField;
        Public String ami70RentAvgField;
        Public String amiNonConventionalRentAvgField;
        Public Integer sec8VacField;
        Public Integer ami30VacField;
        Public Integer ami40VacField;
        Public Integer ami50VacField;
        Public Integer ami60VacField;
        Public Integer ami70VacField;
        Public Integer totalAmiVacField;
        Public Integer amiNonConventionalVacField;
        Public String leaseTermLihtcField;
        Public String discountLihtcField;
        Public String unknownAmiRentAvgField;
        Public Integer unknownAmiVacField;
    }
    Public Class PropertyAddressAdd{
        Public String addressOidField;
        Public String streetAddressField;
        Public String cityField;
        Public String countyField;
        Public Integer zipField;
        Public Decimal latitudeField;
        Public Decimal longitudeField;
        Public String mSAField;
        Public String subNameField;
        Public String stateField;
        Public Integer subIdField;
        Public Boolean subIdFieldSpecified;
        Public String timeZoneField;
        Public String betaAddressField;
        Public String affordableMsaField;
        Public String affordableSubNameField;
        Public Integer affordableSubIdField;
        Public Boolean affordableSubIdFieldSpecified;
    }
    Public Class PropertyLeasingIncentivesLeasingIncentive{
        Public String lIOidField;
        Public String lIDescriptionField;
    }
    Public Class PropertyBuildingBldStoDetails{
        Public Integer floorsField;
        Public Integer bldgsField;
        Public Integer climateControlUnitsField;
        Public Integer nonClimateControlUnitsField;
        Public Integer totalUnitsField;
        Public Integer totalBoatRVField;
        Public Integer acresField;
        Public Integer gLAField;
    }
    Public Class PropertyBuildingBldSthDetails{
        Public Integer totalSizeField;
        Public Integer bldgsField;
        Public String nonPurposeBuiltField;
        Public String onCampusField;
        Public Integer totalUnitsField;
        Public Integer floorsField;
        Public Integer totalBedCapacityField;
        Public Integer bedsField;
        Public String totalNonCompField;
        Public String totalCompField;
        Public String rentTypeField;
        Public String universityNameField;
        Public String universityOwnedField;
        Public PropertyBuildingBldSthDetailsPT[] propertyTypesField;
    }
    Public Class PropertyBuildingBldSnrDetails{
        Public Integer floorsField;
        Public Integer bldgsField;
        Public Integer totalUnitsField;
        Public Integer totalLicensedUnitsField;
    }
    Public Class ShoppingCenterPhoneFieldType{}
    Public Class PropertyBuildingBldRetDetails{
        Public Integer superIDField;
        Public ShoppingCenterPhoneFieldType shoppingCenterPhoneField;
        Public Integer totalSizeField;
    }
    
    Public Class PropertyBuildingBldIndDetails{
        Public Integer floorsField;
        Public Integer bldgsField;
        Public Integer typeValidatedField;
        Public String tenancyField;
    }
    
    Public Class PropertyBuildingBldOffDetails{
        Public Integer floorsField;
        Public Integer bldgsField;
        Public String tenancyField;
    }
    Public Class PricingSystemCodeFieldType{}
    Public Class PricingSystemFieldType{}
    Public Class PropertyBuildingBldAptDetails{
        Public Decimal avgSizeField;
        Public Integer nonCompUnitsField;
        Public Integer floorsField;
        Public Integer totalUnitsField;
        Public PricingSystemCodeFieldType pricingSystemCodeField;
        Public PricingSystemFieldType pricingSystemField;
        Public Integer bldgsField;
        Public String unitMixEstimatedField;
        Public Integer hasAffordableUnitsField;
        Public String resTypeField;
        Public Integer taxCreditYearPlacedInServiceField;
        Public Integer taxCreditComplianceYearsRemainingField;
        Public String contractStartDateField;
        Public String contractEndDateField;
        Public String projectOriginField;
    }
    
    Public Class PropertyBuildingBldSthDetailsPT{
        Public String codeField;
        Public String shortDescField;
        Public String myTypeField;
    }
    
}