Global class CountPoolDataBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
    Map<String,Integer> SubMarketCounterMap;
    Map<String,Integer> MarketCounterMap;
    String Sector;
    String QueueConfigId;
    Integer BatchSize;
    String ProcessQuery;String PropertiesQuery;String MetricsQuery;
    Integer Month;
     Integer NoOfDaysinQuarter;
    public CountPoolDataBatchClass(Integer NoOfDaysinQuarter,Integer Month,String Sector,String QueueConfigId,Integer BatchSize,String ProcessQuery,String PropertiesQuery,String MetricsQuery){
        this.Sector=Sector;
        this.SubMarketCounterMap=new Map<String,Integer>();
        this.MarketCounterMap=new Map<String,Integer>();
        this.QueueConfigId=QueueConfigId;
        this.BatchSize=BatchSize;
        this.ProcessQuery=ProcessQuery;
        this.PropertiesQuery=PropertiesQuery;
        this.MetricsQuery=MetricsQuery;
        this.Month=Month;
        this.NoOfDaysinQuarter=NoOfDaysinQuarter;
    }
    global Database.querylocator start(Database.BatchableContext BC){
        //String Query='Select Id,sector__c,MSA__C,Sub_Market_Id__C from ops_pool_Data__C where sector__c=\''+Sector+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
        system.debug('PropertiesQuery=>'+PropertiesQuery);
        system.debug('MetricsQuery=>'+MetricsQuery);
        return Database.getQueryLocator(PropertiesQuery);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('CountPoolDataBatchClasssize=>'+scope.size());
        for(sObject scopeitem:scope){
            ops_pool_Data__c PoolRecord=(ops_Pool_Data__c)scopeitem;
            
            String SubMarketKeyString=PoolRecord.sector__c+';'+PoolRecord.Property_Type__c+';'+PoolRecord.MSA__c+';'+PoolRecord.Sub_Market_Id__c;
            String MarketKeyString=PoolRecord.sector__c+';'+PoolRecord.Property_Type__c+';'+PoolRecord.MSA__c;
            
            if(SubMarketCounterMap.containsKey(SubMarketKeyString)){
                Integer Tempcount=SubMarketCounterMap.get(SubMarketKeyString);
                SubMarketCounterMap.put(SubMarketKeyString, TempCount+1);
            }else{
                SubMarketCounterMap.put(SubMarketKeyString, 1);
            }
            
            if(MarketCounterMap.containsKey(MarketKeyString)){
                Integer Tempcount=MarketCounterMap.get(MarketKeyString);
                MarketCounterMap.put(MarketKeyString, TempCount+1);
            }else{
                MarketCounterMap.put(MarketKeyString, 1);
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        System.debug('SubMarketCounterMapsize=>'+SubMarketCounterMap.size());
        system.debug('SubMarketCounterMap=>'+SubMarketCounterMap);
        CountMetricDataBatchClass BatchClass=new CountMetricDataBatchClass(NoOfDaysinQuarter,Month,Sector,QueueConfigId,BatchSize,MarketCounterMap,SubMarketCounterMap,ProcessQuery,MetricsQuery);
        Database.executeBatch(BatchClass, BatchSize);
        
        
    }
   
}