/*
	@Name: Shift_MeaningfulActivity_Test
	@Description: Test method for the Shift_MeaningfulActivity_Handler
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

@isTest
public class Shift_MeaningfulActivity_Test {
	
	public static testMethod void testEvent() {
		List<Event> eventList = new List<Event>();
		
    	Id Profile1 = userinfo.getProfileId(); 
    	User tempUser = new User(
    		Username = 'testUser1@shift-crm.com33632',
    		LastName = 'Last',
    		Firstname = 'First',
    		Alias = 'FLTest',
    		CommunityNickname = 'test2342333',
    		TimeZoneSidKey = 'America/New_York',
    		LocaleSidKey = 'en_CA',
    		ProfileId = Profile1,
    		LanguageLocaleKey = 'en_US',
    		EmailEncodingKey = 'ISO-8859-1',
    		Email = 'test@shift-crm.com'
    	);
    	insert tempUser;		
		
		Account tempAcc = new Account(Name='Shift Test Account');
		insert tempAcc;
		
		//Create 50 Special Waste Profiles and set the Landfill to tempLandfill1
		for (Integer i=0; i<50; i++) {
			Event event = new Event(
				WhatId = tempAcc.Id,
				Meaningful_Activity__c = true,
				OwnerId = tempUser.Id,
				StartDateTime = DateTime.Now(),
				EndDateTime = DateTime.Now().AddHours(2)
			);
			eventList.add(event);
		}
		insert eventList;

		Account assertAcc1 = [SELECT Last_Touch_Date__c, Last_Touch_By__c FROM Account WHERE Id = :tempAcc.Id];
		system.assertEquals(assertAcc1.Last_Touch_Date__c, Date.Today());
		system.assertEquals(assertAcc1.Last_Touch_By__c, tempUser.Id);
			
	}
	
	public static testMethod void testTask() {
		List<Task> taskList = new List<Task>();
		
    	Id Profile1 = userinfo.getProfileId(); 
    	User tempUser = new User(
    		Username = 'testUser1@shift-crm.com3332',
    		LastName = 'Last',
    		Firstname = 'First',
    		Alias = 'FLTest',
    		CommunityNickname = 'test234234',
    		TimeZoneSidKey = 'America/New_York',
    		LocaleSidKey = 'en_CA',
    		ProfileId = Profile1,
    		LanguageLocaleKey = 'en_US',
    		EmailEncodingKey = 'ISO-8859-1',
    		Email = 'test@shift-crm.com'
    	);
    	insert tempUser;		
		
		Account tempAcc = new Account(Name='Shift Test Account');
		insert tempAcc;
		
		//Create 50 Special Waste Profiles and set the Landfill to tempLandfill1
		for (Integer i=0; i<50; i++) {
			Task task = new Task(
				WhatId = tempAcc.Id,
				Meaningful_Activity__c = true,
				Touched_Date__c = Date.Today(),
				ActivityDate = Date.Today(),
				OwnerId = tempUser.Id,
				Subject = 'TEST',
				Status = 'Completed',
				Priority = 'Normal'
			);
			taskList.add(task);
		}
		insert taskList;
		
		Task temptask = [SELECT Id, Meaningful_Activity__c, Touched_Date__c, OwnerId, AccountId, isClosed FROM Task WHERE Id = :taskList[4].Id];		
		
		Account assertAcc1 = [SELECT Last_Touch_Date__c, Last_Touch_By__c FROM Account WHERE Id = :tempAcc.Id];
		system.assertEquals(assertAcc1.Last_Touch_Date__c, Date.Today());
		system.assertEquals(assertAcc1.Last_Touch_By__c, tempUser.Id); 
			
	}	

}