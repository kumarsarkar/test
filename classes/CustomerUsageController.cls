global with sharing class CustomerUsageController {
    public Date fromDt {get; set;}
    public Date toDt {get; set;}
    public string RecPerPage {get; set;}
    public List<SelectOption> RecPerPageOption {get; set;}
	public list<String> AlphaList {get; set;}
    public String AlphaFilter {get; set;}    
    public String GroupFilter {get; set;}    
    public List<SelectOption> GroupFilterOption {get; set;}
	public String SearchName {get; set;}
    private String SaveSearchName;
    private string QueryAccount;
    
        @RemoteAction
    global static string getChildrenIDs(string acctID) {
        string result = '\'' + acctID + '\',';
        if (string.isNotEmpty(acctID) && string.isNotBlank(acctID)) {
            List<Account> children = [Select Id, GrandParent__c from Account where ParentId =: acctID LIMIT 1];
            
            if (children.size() > 0) {
                Account ch = children[0];
                string gpID = ch.GrandParent__c;
                for(Account a : [Select Id from Account where GrandParent__c =: gpID OR Id =: gpID ]) {
                    result += '\'' + a.Id + '\',';
                }               
            }
        }
        return result.substring(0, result.length() - 1);
	}
    
    public List<SelectOption> UsageAnalysisAccounts
    {
        get
        {            
            List<SelectOption> acctList = new List<SelectOption>();
            List<ID> acctIDs = new List<ID>();
            for(AggregateResult a : [SELECT Account__r.Id, Account__r.Name
				from Customer_Usage_Analysis__c group by Account__r.Id, Account__r.Name]) {
                acctList.add(new SelectOption(string.valueOf(a.get('Id')), string.valueOf(a.get('Name'))));
            }            
            return acctList;
        }
        set;        
    }
    
	public CustomerUsageController() {                    
        Date todaysdate = Date.today();
        fromDt = todaysdate.toStartOfMonth().addMonths(-10);
		toDt = todaysdate.toStartOfMonth().addMonths(1).addDays(-1);
        
		RecPerPageOption = new list<SelectOption>();
        RecPerPageOption.add(new SelectOption('10','10'));
        RecPerPageOption.add(new SelectOption('25','25'));
        RecPerPageOption.add(new SelectOption('50','50'));
        RecPerPageOption.add(new SelectOption('100','100'));
        RecPerPageOption.add(new SelectOption('200','200'));
        RecPerPage = '25'; //default records per page        
        
        GroupFilterOption  = new list<SelectOption>();
        GroupFilterOption.add(new SelectOption('All Accounts','All Accounts'));
        GroupFilterOption.add(new SelectOption('All Active Clients','All Active Clients'));
        GroupFilterOption.add(new SelectOption('My Active Clients','My Active Clients'));
        GroupFilter = 'All Accounts';
        
		AlphaList = new list<String> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Other', 'All'};
                   
		if (apexpages.currentpage().getparameters().get('alpha') == null) {
            AlphaFilter = 'All';
        } else {
            AlphaFilter = apexpages.currentpage().getparameters().get('alpha');
        }        
        BuildQuery();        
    }    
    
    public PageReference SearchAccount() {
        SaveSearchName = SearchName;        
        BuildQuery();        
        return null;
    }    
    
    public PageReference ClearAll(){
 		SearchName = '';
        SaveSearchName = '';
        AlphaFilter = 'All';
        BuildQuery();       
        return null;
    }    
    
  
    
 public void BuildQuery() {
     //StdSetControllerAccount = null;
        String QueryWhere = '';
        
        if (AlphaFilter == null || AlphaFilter.trim().length() == 0) {
            AlphaFilter = 'All';
        }
        
        QueryAccount = 'Select Id, Name, Type, Tier__c, Contract_Renewal_Date__c, Phone, BillingState, Account_Type__c, Owner.Name, LastActivityDate ' +
            ' FROM Account'; 
        
        if (AlphaFilter == 'Other') {
            QueryWhere = BuildWhere(QueryWhere, '(Name < \'A\' OR Name > \'Z\') AND (NOT Name LIKE \'Z%\') ');
        } else if (AlphaFilter != 'All') {
            QueryWhere = BuildWhere(QueryWhere, '(Name LIKE \'' + String.escapeSingleQuotes(AlphaFilter) + '%\')' );
        }
        
        if (SaveSearchName != null) {
            QueryWhere = BuildWhere(QueryWhere, ' (Name LIKE \'%' + String.escapeSingleQuotes(SaveSearchName) + '%\')');
        }
     

     if (GroupFilter == 'All Active Clients' || GroupFilter == 'My Active Clients') {
	     if (QueryWhere == '') {
    	     QueryWhere += ' WHERE ';
	     } else {
    	     QueryWhere += ' AND ';
	     }         
         QueryWhere += 'TYPE = \'Client\' AND Account_Type__c = \'Active\' ';
     	if (GroupFilter == 'My Active Clients') {    
            QueryWhere += 'AND OwnerId = \'' + UserInfo.getUserId() +  '\'';
        }
     }
     
        QueryAccount += QueryWhere;
     

		string nids = '';
            for(AggregateResult a : [SELECT Account__r.Id, Account__r.Name
				from Customer_Usage_Analysis__c group by Account__r.Id, Account__r.Name]) {
                //acctIDs.add(string.valueOf(a.get('Id')));
                nids += '\'' + string.valueOf(a.get('Id')) + '\',';
            }

     if (nids != '') {
         nids = nids.substring(0, nids.length() - 1);
	     if (QueryWhere == '') {
    	     QueryAccount += ' WHERE Id in (';
	     } else {
    	     QueryAccount += ' and Id in (';
	     }         
		QueryAccount += nids + ') ';         
     }
        QueryAccount += ' ORDER BY NAME ASC LIMIT ' + RecPerPage;
    }

    public String BuildWhere(String QW, String Cond) {
        if (QW == '') {
            return ' WHERE ' + Cond;
        } else {
            return QW + ' AND ' + Cond;
        }
    }    
    
    public List<Account> UAAccounts
    {
        get
        {            
            List<Account> acctList = new List<Account>();
			acctList = Database.Query(QueryAccount);
            
            system.debug(acctList);
            return acctList;
        }
        set;        
    } 
    
    public void codeCoverage() {
        integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;  
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;  
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;  
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;          
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;        
    }
}