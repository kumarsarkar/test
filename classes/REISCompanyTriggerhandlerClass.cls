public class REISCompanyTriggerhandlerClass {

public static void HandleBeforeInsert(List<Company__c> NewList){
        
        populateUniqueString(NewList);
    }
public static void HandleAfterInsertAfterUpdate(List<Company__c> NewList){
        
        populateCompanyName(NewList);
    }
    
    //populate 36characters long string
    public static void populateUniqueString(List<Company__c> NewList){
        for(Company__c Company: NewList){
            if((Company.Oid__c== '' || Company.Oid__c== null)){
                Blob b = Crypto.GenerateAESKey(128);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                system.debug(guid);
                Company.OID__c=guid.touppercase();
                Company.Salesforce_Created__c=true;
            }
            
        }
    } 
    public static void populateCompanyName(List<Company__c> NewList){
        for(Company__c company : NewList){
            if(company.Name==null || company.Name== '' || company.Name.startsWith('a2v')){
                company.Name = 'No Name Provided';
                System.Debug('<<<<COMPANYNAME>>>>'+company.Name);
            }
        }
    }
}