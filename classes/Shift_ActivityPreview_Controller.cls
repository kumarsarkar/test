/*
    @Name: Shift_ActivityPreview_Controller
    @Description: Controller for dynamic activity page
    @Dependancies: Shift_LeadForm.page
    @Version: 1.0.0 
    
    === VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial
*/ 

global with sharing class Shift_ActivityPreview_Controller {
    
    public String theObjectType {get;set;}
    public String theObjectId {get;set;}
    public List<activityWrapper> activitySortedList {get;set;} 
    
    public list<event> eventList;
    public list<task> taskList;    
    public List<activityWrapper> activityList;
    public map<Id, Task> taskMap; 
    public map<Id, Event> eventMap; 
    public Map<Date, DateTime> lastModifiedMap = new Map<Date, Datetime>();

    public DateTime todayDate = System.now();

    //Contains the default number of rows to be displayed
    public Decimal numRows {get;set;}
    
    //Contrustor for wrapper class
    public class activityWrapper {
        public sObject sObj {get; private set;}
        public String sObjType {get;set;}
        public String ActivityDate {get;set;}
        public String OwnerId {get;set;}
        public String WhatId {get;set;}
        public String WhoId {get;set;}
        public String Subject {get;set;}
        public String Type {get;set;}
        public String Description {get;set;}
        public String LastModifiedDate {get;set;}
        public String TaskSubject {get;set;}      
        public String TaskType {get;set;}
               
        public activityWrapper(sObject objRecord) {
            this.sObj = objRecord;
            this.sObjType = string.valueOf(objRecord.getsObjectType());
            this.ActivityDate = 'ActivityDate';
            this.OwnerId = 'OwnerId';
            this.WhatId = 'WhatId';
            this.WhoId = 'WhoId';
            this.Subject = 'Subject';
            this.Type = 'Type';
            this.Description = 'Description';
            this.LastModifiedDate = 'LastModifiedDate';
            this.TaskSubject = 'Task_Subject__c';
            this.TaskType = 'Task_Type__c';           
        }
    }

    public pageReference init() {

        Integer maxEvents = integer.valueOf(Shift_ActivitiesTableSetting__c.getInstance().Max_Events__c);
        Integer maxTasks = integer.valueOf(Shift_ActivitiesTableSetting__c.getInstance().Max_Tasks__c);
           
        //Query out the event & task records for the object Id the page is called from
        eventList = Database.Query(genEventQuery());
        taskList = Database.Query(genTaskQuery());
        
        //Convert to maps
        eventMap = new map<Id, Event>(eventList);
        taskMap = new map<Id, Task>(taskList);

        numRows = Shift_ActivitiesTableSetting__c.getInstance().Numer_of_Rows__c;
        if (numRows == null) numRows = 30;
        
        //Generate activity wrapper for the event and task maps
        this.activityList = new List<activityWrapper>();
        this.activitySortedList = new List<activityWrapper>();
        for (Id eId :eventMap.KeySet()) {
            this.activityList.add(new activityWrapper(
                eventMap.get(eId)               
            ));
        }
        for (Id tId :taskMap.KeySet()) {
            this.activityList.add(new activityWrapper(
                taskMap.get(tId)                
            ));
        }
        
        //Sort the list        
        sortList();
        
        return null;     
    }  
    
    /*** Generate the event query string **/
    public string genEventQuery() {
        String queryStr = 'SELECT Id, Description, ActivityDate, OwnerId, Subject, WhoId, WhatId, Type, LastModifiedDate, Task_Subject__c, Task_Type__c';
        queryStr += ' FROM Event WHERE (WhoId = :theObjectId OR WhatId = :theObjectId OR AccountId = :theObjectId) AND EndDateTime <= :todayDate ORDER BY EndDateTime DESC, LastModifiedDate DESC LIMIT :maxEvents';
        return queryStr;
    }
    
    /*** Generate the task query string **/
    public string genTaskQuery() {
        String queryStr = 'SELECT Id, Description, ActivityDate, OwnerId, Subject, WhoId, WhatId, Type, LastModifiedDate, Task_Subject__c, Task_Type__c';
        queryStr += ' FROM Task WHERE (WhoId = :theObjectId OR WhatId = :theObjectId OR AccountId = :theObjectId) AND isClosed = true ORDER BY ActivityDate DESC, LastModifiedDate DESC LIMIT :maxTasks';
        return queryStr;
    }   
        
    public List<SelectOption> getObjectSelections() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Task','Task'));
        options.add(new SelectOption('Event','Event'));
        return options;
    } 
    
    /*** Methods to sort the activity wrapper list by activity date ***/
    private void sortList() {
        integer targetsize = activityList.size();
        while(activitySortedList.size() != targetsize){
          findMinDateValue();
        }       
    }
    private void findMinDateValue(){    
        datetime dt = Datetime.newInstance(1900, 12, 31, 00, 00, 00); // if decending
        datetime dtLM = Datetime.newInstance(1900, 12, 31, 00, 00, 00); // if decending
        
        integer i = 0;
        integer mini = 0;           
        for (i = 0; i != activityList.size() ; i ++){

            Date activityDate = date.valueOf(activityList[i].sObj.get('ActivityDate'));
            DateTime lastModifiedDt = DateTime.valueOf(activityList[i].sObj.get('LastModifiedDate'));
           
            if(activityDate == dt){
                if (lastModifiedMap.containsKey(activityDate)) {
                    if (lastModifiedMap.get(activityDate) < lastModifiedDt) {
                        mini = i;
                        lastModifiedMap.put(activityDate, lastModifiedDt);
                    }
                } else {
                   mini = i; 
                   lastModifiedMap.put(activityDate, lastModifiedDt);
                } 
            } else if(activityDate > dt){ // if decending
                lastModifiedMap.put(activityDate, lastModifiedDt);
                dt = activityDate;
                mini = i;
            }
        }

        activitySortedList.add(activityList[mini]);
        activityList.remove(mini);         

    }

}