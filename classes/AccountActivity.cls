public without sharing class AccountActivity {
	private integer counter=0;  //keeps track of the offset
	private integer list_size=10; //sets the page size or number of rows
   	public integer total_size=0; //used to show user the total size of the list
   	
   	private integer cntc_counter=0;  //keeps track of the offset
	private integer cntc_list_size=20; //sets the page size or number of rows
   	public integer cntc_total_size=0; //used to show user the total size of the list
   	
   	private integer cntchist_counter=0;  //keeps track of the offset
	private integer cntchist_list_size=10; //sets the page size or number of rows
   	public integer cntchist_total_size=0; //used to show user the total size of the list
   	
   	private integer opp_counter=0;  //keeps track of the offset
	private integer opp_list_size=4; //sets the page size or number of rows
   	public integer opp_total_size=0; //used to show user the total size of the list
   	
   	private final Account acct{get; set;}
   	public Account[] AcctHistCnt;
   	public Contact[] AcctCntct{get; set;}
   	public Account[] SelAccountHist;
   	public Contact selCntct;
   	public Contact HistCntct;
   	public Account HistAcct;
   	public Contact[] SelContactHist{get; set;}
   	Public String CurHistId;
   	Public String CurAcctHistId;
   	Public String BillAddress{get; set;}
   	Public Account myAccount;
   	public List<Contact> contactSect {get; set;}
   	public List<Contact> childList {get; set;}
   	public String myActHistId {get; set;}
   	public String myCntTskPge {get; set;}
   	
   	public Contact[] cntc{get; set;}
   	
   	//public boolean chkbox{get; set;}
   	
   	Public list<ActivityHistory> HistList;
   	Public ActivityHistory hist;
   	
   	Public list<ActivityHistory> CnHistList;
   	Public ActivityHistory cnhist;
   	
   	Public Class HistAndContact
   	{
   		Public String Id{get; set;}
   		Public String Name{get; set;}
   		Public String Title{get; set;}
   		Public String Email{get; set;}
   		Public String Phone{get; set;}
   		Public String MobilePhone{get; set;}
   		Public String Subject{get; set;}
   		Public Date ActivityDate{get; set;}
   	}
   	
   	Public HistAndContact myHistAndContact;
   	Public List<HistAndContact> myList;
   	Public Map<id, Contact> ActivityMapToContact;
   	Public String[] insertResults;
   	
   	public AccountActivity(ApexPages.StandardController stdController){		
   		
		this.acct = (Account)stdController.getRecord();
		myActHistId = 'test';
		myAccount = [Select BillingStreet, BillingCity, BillingState, BillingPostalCode From Account Where Id= :acct.id];
		
		cntc_total_size = [Select Count() From Contact Where AccountId = :acct.id];
		If(cntc_total_size > 0){
			selCntct = [Select Id From Contact Where AccountId = :acct.id Limit 1];
		}
		
		//system.debug(acct.id);
		
		//chkbox = false;
		//system.debug(total_size);
		//system.debug(cntc_total_size);
	    //system.debug(ActivityMapToContact);
	    this.contactSect = [Select Id From Contact Where AccountId = :acct.id];
	    
	    this.childList = [SELECT FirstName, LastName, Title, Phone, MobilePhone, Email, Workphone_Extension__c FROM Contact WHERE AccountId =: acct.Id order by LastName, FirstName];
   	}
   	
   	public Account[] getHist(){
   		//system.debug(list_size);
   		//system.debug(counter);
   		//Account[] AcctHistCnt = [Select Name, (Select Subject, ActivityDate, Description, Whoid From ActivityHistories Order By ActivityDate Desc limit :list_size offset :counter) From Account where id =:acct.id limit 1];
   		Account[] AcctHistCnt = [Select Name, (Select Subject, ActivityDate, Description, Whoid From ActivityHistories Order By ActivityDate Desc Limit 199) From Account where id =:acct.id];
   		//system.debug(AcctHistCnt);
   		Return AcctHistCnt;
   	}
   	
   	
  public List<Contact> getChildren()
  {
    return (List<Contact>)childList;
  }
   	
   	public HistAndContact[] getHistWithContact(){
   		//system.debug(list_size);
   		//system.debug(counter);
   		Map<id, Contact> ActivityMapToContact = New  Map<id, Contact>();

		For (Contact cn : [Select FirstName, LastName, Phone, Email, id, (Select Id, Subject, ActivityDate, Description From ActivityHistories Limit 199) From Contact where accountid = :acct.id And LastActivityDate != null]){
			CnHistList = cn.ActivityHistories;
   		//system.debug(cn);
	   		for(ActivityHistory cnhist : CnHistList)
	   		{
	   	//system.debug(cnhist);
	   	//system.debug(cn);
	   			ActivityMapToContact.put(cnhist.id, cn);
	   		}
	     }
	     
   		myList = New List<HistAndContact>();
   		   		
   		//Account[] AcctHistCnt = [Select Name, (Select id, ActivityDate, subject From ActivityHistories Order By ActivityDate Desc limit :list_size offset :counter) From Account where id =:acct.id limit 1];
   		Account[] AcctHistCnt = [Select Name, (Select id, ActivityDate, subject From ActivityHistories Order By ActivityDate Desc Limit 199) From Account where id =:acct.id];
   		
   		//system.debug(AcctHistCnt[0].ActivityHistories);
   		HistList = AcctHistCnt[0].ActivityHistories;
   		
   		for(ActivityHistory hist : HistList)
   		{
   	//system.debug(hist);
   	//system.debug(hist.whoid);
   			myHistAndContact = New HistAndContact();
   	
   	//system.debug(hist.id);
   	//system.debug(ActivityMapToContact.get(hist.id));
   	//system.debug(ActivityMapToContact);
   	
   			If (ActivityMapToContact.get(hist.id) == null){
   				
   			}
			Else
			{
				Contact IdContact = ActivityMapToContact.get(hist.id);
   				//Contact thisContact = [Select FirstName, LastName, Phone, Email from Contact Where id = :IdContact.id];
   				myHistAndContact.Name = IdContact.FirstName + ' ' + IdContact.LastName;
   				myHistAndContact.Phone = IdContact.Phone;
   				myHistAndContact.Email = IdContact.Email;
			}

   			myHistAndContact.Subject = hist.Subject;
   			myHistAndContact.ActivityDate = hist.ActivityDate;
   			myHistAndContact.Id = hist.id;
   			myList.Add(myHistAndContact);
   		}
   		
   		//Integer iCnt = 0;
   		//myList = New List<HistAndContact>();
  
   		//for(Account AcctHistCnt : [Select Name, (Select Id, Subject, ActivityDate, Description, Whoid From ActivityHistories limit :list_size offset :counter)
   		// From Account where id =:acct.id limit 1])
   		//    {
   		// 	system.debug(AcctHistCnt);
		//	iCnt += 1;
		//    myHistAndContact = New HistAndContact();
		//    //ActivityHistories myActHist = AcctHistCont!ActivityHistories;
		//    myHistAndContact.Id = myActHist[ActivityHistories].Id;
		//    //myHistAndContact.Subject = AcctHistCnt.ActivityHistories.Subject;
		//    myHistAndContact.Name = AcctHistCnt.Name;
   		//    myHistAndContact.Title = 'Title' + iCnt;
	   	//    //myHistAndContact.Email
	   	//    //myHistAndContact.Phone
	   	//    //myHistAndContact.MobilePhone
	   	//   
		//   //system.debug(acctObj);
		//   myList.Add(myHistAndContact);
		//}
		//myHistAndContact = New HistAndContact();
		//myHistAndContact.Name = 'Test2';
   		//myHistAndContact.Title = 'Title2';
   		//myList.Add(myHistAndContact);
   		Return myList;
   	}
   	
   	/*public Contact[] getAcctContacts(){
   		Contact[] AcctCntct = [Select FirstName, LastName, Phone, Email, Title, MobilePhone From Contact where Accountid =:acct.id limit :cntc_list_size offset :cntc_counter];
   		Return AcctCntct;
   	}
   	*/
   	
   	public Opportunity[] getAcctOpps(){
   		Opportunity[] AcctOpp = [Select Name From Opportunity where Accountid =:acct.id limit :opp_list_size offset :opp_counter];
   		Return AcctOpp;
   	}
   	
   	public Contact[] getCntcHist(){
   		Map<Id, Integer> mapCntActv = new Map<Id, Integer>();
   		system.debug(selCntct);
   		If (cntc_total_size > 0){
	   		//system.debug(list_size);
	   		//system.debug(counter);
	   		//Contact[] SelContactHist = [Select Name, (Select Subject, ActivityDate, Description From ActivityHistories Order By ActivityDate Desc limit :cntchist_list_size offset :cntchist_counter) From Contact where id =:selCntct.id limit 1];
	   		Contact[] SelContactHist = [Select Name, (Select Subject, ActivityDate, Description From ActivityHistories Order By ActivityDate Desc) From Contact where id =:selCntct.id];
	   		system.debug(SelContactHist);
	    		
	    	for(Contact cntctObj : [SELECT Id, (SELECT Id FROM ActivityHistories) 
			                     FROM Contact  WHERE Id = :selCntct.id])
			{
			  mapCntActv.put(cntctObj.Id, cntctObj.ActivityHistories.size());
			  //system.debug(acctObj);
			}
			cntchist_total_size = mapCntActv.get(selCntct.id);
			SelContactHist = [Select Name, (Select Subject, ActivityDate, Description From ActivityHistories Order By ActivityDate Desc) From Contact where id =:selCntct.id];
    		system.debug(SelContactHist);	
   			Return SelContactHist;
   		}
		Else{
			cntchist_total_size = 0;
			Return null;
		}
		

   	}
   	
   	/*public String getBillingAddress(){
    	BillAddress = myAccount.BillingStreet + ' ' + myAccount.BillingCity + ', ' + myAccount.BillingState + ' ' + myAccount.BillingPostalCode;
		Return BillAddress;
   	}
   	*/
   	
   	public PageReference populateContactHist(){
	    if(ApexPages.currentPage().getParameters().get('showHist') != null && ApexPages.currentPage().getParameters().get('showHist') != '' && ApexPages.currentPage().getParameters().get('showHist') != 'null'){
	    	this.selCntct = [Select id from Contact Where id = :ApexPages.currentPage().getParameters().get('showHist')];
	    	system.debug(selCntct);
	    }
	    return null;
    }
    
    public PageReference ShowCurContactHist(){
	    if(ApexPages.currentPage().getParameters().get('showCurCntcHist') != null && ApexPages.currentPage().getParameters().get('showCurCntcHist') != '' && ApexPages.currentPage().getParameters().get('showCurCntcHist') != 'null'){
	    	this.HistCntct = [Select id, (Select Id, Description From ActivityHistories Where Id = :ApexPages.currentPage().getParameters().get('showCurCntcHist')) from Contact Where id = :SelCntct.id];
	    	CurHistId = ApexPages.currentPage().getParameters().get('showCurCntcHist');
	    	system.debug(CurHistId);
	    }
	    return null;
    }
    
    public Contact[] getCntctSelHist(){
    	If (cntc_total_size > 0){
	   		//system.debug(list_size);
	   		//system.debug(counter);
	   		system.debug(selcntct);
	   		system.debug(CurHistId);
	   		Contact[] SelContactHist = [Select Name, (Select Subject, ActivityDate, Description From ActivityHistories Where id = :CurHistId) From Contact where id =:selCntct.id limit 1];
	    	system.debug(SelContactHist);
	    	Return SelContactHist;
    	}
    	system.debug(SelContactHist);
   		Return SelContactHist;
   	}
   	
    public PageReference ShowCurAccountHist(){
	    if(ApexPages.currentPage().getParameters().get('showCurActHist') != null && ApexPages.currentPage().getParameters().get('showCurActHist') != '' && ApexPages.currentPage().getParameters().get('showCurActHist') != 'null'){
	    	this.HistAcct = [Select id, (Select Id, Description From ActivityHistories Where Id = :ApexPages.currentPage().getParameters().get('showCurActHist')) from Account Where id = :acct.id];
	    	CurHistId = ApexPages.currentPage().getParameters().get('showCurActHist');
	    	system.debug(CurHistId);
	    }
	    return null;
    }
   	
   	public Account[] getAcctSelHist(){
   		Account[] SelAccountHist = [Select Name, (Select Subject, ActivityDate, Description From ActivityHistories Where id = :CurHistId) From Account where id =:acct.id limit 1];
    	//system.debug(SelAccountHist);
   		Return SelAccountHist;
   	}
   	
   	/*public PageReference Beginning() { //user clicked beginning
      counter = 0;
      return null;
   }
 
   public PageReference Previous() { //user clicked previous button
      counter -= list_size;
      return null;
   }
   
   public PageReference Next() { //user clicked next button
      counter += list_size;
      return null;
   }
      
   public PageReference End() { //user clicked end
   		if (math.mod(total_size, list_size) > 0) {
   			counter = total_size - math.mod(total_size, list_size);
   		}
   		else {
   			counter = total_size - math.mod(total_size, list_size) - list_size;
   		}
      
      //system.debug(counter);
      //system.debug(total_size);
      //system.debug(list_size);
      return null;
   }
   
   public Boolean getDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
   
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   } 
   */   
      
   public Integer getTotal_size() {
   		Map<Id, Integer> mapActv = new Map<Id, Integer>();
		for(Account acctObj : [SELECT Id, (SELECT Id FROM ActivityHistories Limit 199) FROM Account  WHERE Id = :acct.id])
		{
		  mapActv.put(acctObj.Id, acctObj.ActivityHistories.size());
		  //system.debug(acctObj);
		}
			
		total_size = mapActv.get(acct.id);
      return total_size;
   }
 
   /*public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
   	
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
   */
   
   public PageReference AcctLVM() { //user clicked Account Left Voice Mail
	  //Task newAct = New Task();
	  //newAct.Subject = 'Left Voice Mail';
	  //newAct.AccountId = acct.id;
	  //newAct.Status = 'Completed';
	  //newAct.WhatId = acct.id;
	  //newAct.ActivityDate = date.today();
	  //newAct.Description = 'Left Voice Mail';
	  //Insert newAct;
	  //PageReference page = new PageReference('https://cs10.salesforce.com/' + newAct.id + '/e?cancelURL=%2F' + newAct.id);
	  system.debug(acct.id);
	  PageReference page = new PageReference('/00T/e?title=Call&what_id='+acct.id+'&followup=1&tsk5=Left%20Voicemail&retURL=%2F'+acct.id);
	  //https://cs10.salesforce.com/00TJ000000U9UpuMAF/e?cancelURL=%2F00TJ000000U9UpuMAF
	  ///00T/e?title=Call&what_id={!Account.Id}&followup=1&tsk5=Left%20Voicemail&retURL=%2F{!Account.Id}
	  page.setRedirect(true);
      return page;
   }
   
   
   public PageReference editAccountHist(){
	    if(ApexPages.currentPage().getParameters().get('showCurActHist') != null && ApexPages.currentPage().getParameters().get('showCurActHist') != '' && ApexPages.currentPage().getParameters().get('showCurActHist') != 'null'){
	    	this.HistAcct = [Select id, (Select Id, Description From ActivityHistories Where Id = :ApexPages.currentPage().getParameters().get('showCurActHist')) from Account Where id = :acct.id];
	    	ActivityHistory actacthist = HistAcct.ActivityHistories;
	    	//delete actacthist;
	    	system.debug(actacthist.id);
	    }
	    return null;
    }
    
    public string createAcctTask(){
       Task NewAcctTask = new Task();
       String Instance;
       try{
       	   NewAcctTask.Whatid = acct.id;
           insert NewAcctTask;
            List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
            if (parts.size() == 3) Instance = parts[0];
            else if (parts.size() == 5) Instance = parts[1];
            else Instance = null;
           myActHistId = 'https://' + Instance + '.salesforce.com/' + NewAcctTask.Id + '/e';
       	   system.debug(myActHistId);
       }
       catch(Exception ex){
        /** some actions  **/
       }
       return myActHistId;
       //Return NewAcctTask;
       //return new PageReference ('/' + NewAcctTask.Id + '/e');
       //PageReference pageRef = new PageReference('/' + NewAcctTask.Id + '/e');
       //pageRef.setRedirect(true);
       //return pageRef;
   }
   
   public string getnewHistpage(){
   		Return myActHistId;
   }
    
    /*public PageReference SaveContact(){
		for (Contact upCntc :contactSect ){
			Update UpCntc;
		}
		Return null;
    }
    */
    
    public PageReference ViewContact(){
	    if(ApexPages.currentPage().getParameters().get('showCntctId') != null && ApexPages.currentPage().getParameters().get('showCntctId') != '' && ApexPages.currentPage().getParameters().get('showCntctId') != 'null'){
	    	this.HistAcct = [Select id, (Select Id, Description From ActivityHistories Where Id = :ApexPages.currentPage().getParameters().get('showCntctId')) from Account Where id = :acct.id];
	    	ActivityHistory actacthist = HistAcct.ActivityHistories;
	    	//delete actacthist;
	    	system.debug(actacthist.id);
	    }
	    return null;
    }
 
  /* 
   public void DelHist() {
   		String result;
  		Boolean success = true;

  		result = success ? 'Data created with success.' : 'Problems were encountered creating sandbox data. Refer to logs for details.';
  		this.setInsertResults(result);
   }
   
	private void setInsertResults(String newString) {
	  insertResults.add(newString);
	}
	
	public List<String> getInsertResults() {
	  return insertResults;
	}
	*/
        
    /*public PageReference CntcBeginning() { //user clicked beginning
      cntc_counter = 0;
      return null;
   }
   
   public PageReference CntcPrevious() { //user clicked previous button
      cntc_counter -= cntc_list_size;
      return null;
   }
 
   public PageReference CntcNext() { //user clicked next button
      cntc_counter += cntc_list_size;
      return null;
   }
   
   public PageReference CntcEnd() { //user clicked end
        if (math.mod(cntc_total_size, cntc_list_size) > 0) {
   			cntc_counter = cntc_total_size - math.mod(cntc_total_size, cntc_list_size);
   		}
   		else {
   			cntc_counter = cntc_total_size - math.mod(cntc_total_size, cntc_list_size) - cntc_list_size;
   		}
      return null;
   }
 
   public Boolean getCntcDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (cntc_counter>0) return false; else return true;
   }
   
   public Boolean getCntcDisableNext() { //this will disable the next and end buttons
      if (cntc_counter + cntc_list_size < cntc_total_size) return false; else return true;
   }
   */
      
   public Integer getCntc_Total_size() {
      return cntc_total_size;
   }
   
   public PageReference NewCntc() {
   		///003/e?retURL=%2F001J000001SZVvG&accid=001J000001SZVvG
   	  PageReference page = new PageReference('/003/e?accid='+acct.id);
	  //https://cs10.salesforce.com/00TJ000000U9UpuMAF/e?cancelURL=%2F00TJ000000U9UpuMAF
	  ///00T/e?title=Call&what_id={!Account.Id}&followup=1&tsk5=Left%20Voicemail&retURL=%2F{!Account.Id}
	  page.setRedirect(true);
      return page;
   }
    
    /*public Integer getCntcPageNumber() {
      return cntc_counter/cntc_list_size + 1;
   }
 
   public Integer getCntcTotalPages() {
      if (math.mod(cntc_total_size, cntc_list_size) > 0) {
         return cntc_total_size/cntc_list_size + 1;
      } else {
         return (cntc_total_size/cntc_list_size);
      }
   } 
      
   public PageReference CntcHistBeginning() { //user clicked beginning
      cntchist_counter = 0;
      return null;
   }
   
   public PageReference CntcHistPrevious() { //user clicked previous button
      cntchist_counter -= cntchist_list_size;
      return null;
   }
 
   public PageReference CntcHistNext() { //user clicked next button
      cntchist_counter += cntchist_list_size;
      return null;
   }
   
   public PageReference CntcHistEnd() { //user clicked end
        if (math.mod(cntchist_total_size, cntchist_list_size) > 0) {
   			cntchist_counter = cntchist_total_size - math.mod(cntchist_total_size, cntchist_list_size);
   		}
   		else {
   			cntchist_counter = cntchist_total_size - math.mod(cntchist_total_size, cntchist_list_size) - cntchist_list_size;
   		}      return null;
   }

 public Boolean getCntcHistDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (cntchist_counter>0) return false; else return true;
   }
   
   public Boolean getCntcHistDisableNext() { //this will disable the next and end buttons
      if (cntchist_counter + cntchist_list_size < cntchist_total_size) return false; else return true;
   }  
   */ 
         
   public Integer getCntcHist_Total_size() {
      return cntchist_total_size;
   }
    
    /*public Integer getCntcHistPageNumber() {
      return cntchist_counter/cntchist_list_size + 1;
   }
 
   public Integer getCntcHistTotalPages() {
      if (math.mod(cntchist_total_size, cntchist_list_size) > 0) {
         return cntchist_total_size/cntchist_list_size + 1;
      } else {
         return (cntchist_total_size/cntchist_list_size);
      }
   }
   */
   
   public PageReference editContactHist(){
	    if(ApexPages.currentPage().getParameters().get('showCurCntHist') != null && ApexPages.currentPage().getParameters().get('showCurCntHist') != '' && ApexPages.currentPage().getParameters().get('showCurCntHist') != 'null'){
	    	this.HistCntct = [Select id, (Select Id, Description From ActivityHistories Where Id = :ApexPages.currentPage().getParameters().get('showCurCntHist')) from Contact Where id = :selcntct.id];
	    	ActivityHistory cntacthist = HistCntct.ActivityHistories;
	    	//delete actacthist;
	    	system.debug(cntacthist.id);
	    }
	    return null;
    }
   
   public PageReference CntchistLVM() { //user clicked Contact Left Voice Mail
	  //Task newCnt = New Task();
	  //newCnt.Subject = 'Left Voice Mail';
	  //newAct.AccountId = acct.id;
	  //newCnt.Status = 'Completed';
	  //newCnt.WhatId = acct.id;
	  //newCnt.Whoid = selcntct.id;
	  //newCnt.Description = 'Left Voice Mail';
	  //newCnt.ActivityDate = date.today();
	  //Insert newCnt;
	  
	  //PageReference page = new PageReference('https://cs10.salesforce.com/' + newCnt.id + '/e?cancelURL=%2F' + newCnt.id);
      //return null;
      
      PageReference page = new PageReference('/00T/e?title=Call&who_id='+selcntct.id+'&followup=1&tsk5=Left%20Voicemail&retURL=%2F'+selcntct.id);
	  //00T/e?title=Call&who_id={!Contact.Id}&followup=1&tsk5=Left%20Voicemail&retURL=%2F{!Contact.Id}
	  page.setRedirect(true);
      return page;
   }
   
   public String createCntctTask(){    
       Task NewCntTask = new Task();
       String Instance;
       try{
       	   NewCntTask.Whatid = acct.id;
       		NewCntTask.Whoid = selCntct.id;
            insert NewCntTask;
            List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
            if (parts.size() == 3) Instance = parts[0];
            else if (parts.size() == 5) Instance = parts[1];
            else Instance = null;
           	myCntTskPge = 'https://' + Instance + '.salesforce.com/' + NewCntTask.Id + '/e';
       	   system.debug(myCntTskPge);
       }
       catch(Exception ex){
        /** some actions  **/
       }
       return myCntTskPge;
   }
   
   /*public PageReference CntcLogCall() { //user clicked Account Left Voice Mail
   	  return null;
   }
   
   public PageReference OppBeginning() { //user clicked beginning
      opp_counter = 0;
      return null;
   }
   
   public PageReference OppPrevious() { //user clicked previous button
      opp_counter -= opp_list_size;
      return null;
   }
 
   public PageReference OppNext() { //user clicked next button
      opp_counter += opp_list_size;
      return null;
   }
   
   public PageReference OppEnd() { //user clicked end
        if (math.mod(opp_total_size, opp_list_size) > 0) {
   			opp_counter = opp_total_size - math.mod(opp_total_size, opp_list_size);
   		}
   		else {
   			opp_counter = opp_total_size - math.mod(opp_total_size, opp_list_size) - opp_list_size;
   		}
      return null;
   }
 
   public Boolean getOppDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (opp_counter>0) return false; else return true;
   }
   
   public Boolean getOppDisableNext() { //this will disable the next and end buttons
      if (opp_counter + opp_list_size < opp_total_size) return false; else return true;
   }
   */
      
   public Integer getOpp_Total_size() {
      return opp_total_size;
   }
    
    /*public Integer getOppPageNumber() {
      return opp_counter/opp_list_size + 1;
   }
 
   public Integer getOppTotalPages() {
      if (math.mod(opp_total_size, opp_list_size) > 0) {
         return opp_total_size/opp_list_size + 1;
      } else {
         return (opp_total_size/opp_list_size);
      }
   } 
*/
   
   public PageReference save()
  {
    Savepoint sp = Database.setSavepoint();
    List<Contact> updateChildList = new List<Contact>();
      Upsert acct;
        
	  for (Contact thsUpCntc : childList){
	  	updateChildList.Add(thsUpCntc);      	
	  }
	  update updateChildList;
	  return null;
  }
}