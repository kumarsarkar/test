public class BuildQueueScheduleClass {
    
    public static void BuildQueue(){
        List<Queue_Configuration__c> qconfigList = [Select ID, Sector__c,Month_Start_Date__c,BatchJobId__c From Queue_Configuration__c];
        for(Queue_Configuration__c QueueConfig:qconfigList){   
            String[] sector = QueueConfig.Sector__c.split(';');
            system.debug('Sector' + sector);
            String WhereClause='';
            String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c,Do_Not_Call__c,IAG__c,CCRC__c,Client_Services__c,QA__c,Non_Published_MSAs__c,New_Construction__c from Ops_Pool_Data__c ';
            List<CurrentDate_Setting__c> CurrentDateSetting=[SELeCT id, Name,ActiveFlag__c,currentdate__c FROM CurrentDate_Setting__c];
            Date CurrentDate;
            if(CurrentDateSetting.size()>0){
                if(CurrentDateSetting[0].ActiveFlag__c){
                    CurrentDate=CurrentDateSetting[0].currentDate__c; 
                }else{
                    CurrentDate=Date.today();
                }
            }else{
                CurrentDate=Date.today();
            }
            
            Integer Month=CurrentDate.Month();
            Integer BatchSize=Integer.valueOf(BatchSize_Setting__c.getValues('BuildQueueBatch').Size__c);
            //Integer Year=Date.today().Year();
            Integer Year=CurrentDate.Year();
            Integer QuarterStartMonth;
            Integer QuarterStartYear=Year;
            String PropertyStatus='Completed';
            for(String s:sector){
                system.debug('Sector=>'+s);
                if(QueueConfig.Month_Start_Date__c!=1){
                    if(CurrentDate.Day()<QueueConfig.Month_Start_Date__c){
                        if(Month==1){
                            Month=12;
                            Year=Year-1;
                        }else{
                            Month-=1;
                            //Year=Year;
                        }
                    }else{
                        //Year=Date.today().Year();
                    }
                    if(Month==12 || Month==1 || Month==2){
                        QuarterStartMonth=12;
                        if(Month==12){
                            QuarterStartYear=Year;    
                        }else{
                            QuarterStartYear-=1;    
                        }
                        
                        Month=Month==12?1:Month+1;
                    }else if(Month==3 || Month==4 || Month==5){
                        QuarterStartMonth=3;
                        Month+=1;
                    }else if(Month==6 || Month==7 || Month==8){
                        QuarterStartMonth=6;
                        Month+=1;
                    }else if(Month==9 || Month==10 || Month==11){
                        QuarterStartMonth=9;
                        Month+=1;
                    }
                }else{
                    if(Month==1 || Month==2 || Month==3){
                        QuarterStartMonth=1;
                    }else if(Month==4 || Month==5 || Month==6){
                        QuarterStartMonth=4;
                    }else if(Month==7 || Month==8 || Month==9){
                        QuarterStartMonth=7;
                    }else if(Month==10 || Month==11 || Month==12){
                        QuarterStartMonth=10;
                    }
                }
                
                Date StartDate=Date.newInstance(QuarterStartYear, QuarterStartMonth, Integer.valueOf(QueueConfig.Month_Start_Date__c));
                Integer NoOfDaysinQuarter=StartDate.daysBetween(CurrentDate);
                String SearchQuery;
                String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' AND Property_Status__c=\''+PropertyStatus+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
                String MetricCountQuery='Select Id,survey_completed__c,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c,Rent__c,Vacancy__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' AND createddate=LAST_N_DAYS:'+String.valueOf(NoOfDaysinQuarter)+' AND Include_In_Coverage__c=true Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
                // WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' and msa__c in (\'39884811-F394-4471-8F8A-011826239807\',\'00662920-DEC3-47D1-8425-88D97AE6084A\') AND SUB_MARKET_ID__C IN (\'9C4C8FEA-0B76-4478-87EA-56656F78E55C\',\'974B8701-520B-4CF5-B5CC-03D64C80F765\',\'E5AAC081-DD4A-4218-A750-859A60EBACFC\',\'D462C34F-0E09-4F40-BD24-A0561B95BD0F\') AND Sector__c=\''+s+'\' AND  (Next_Queue_Date__c = null  OR  Next_Queue_Date__c<=Today)' ;
                WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\' AND  (Next_Queue_Date__c = null  OR  Next_Queue_Date__c<=Today)' ;
                SearchQuery=ProcessQuery+WhereClause;
                system.debug('SearchQuery'+SearchQuery);
                system.debug('Month'+Month);
                system.debug('StartDate'+StartDate);
                system.debug('StartDate'+StartDate);
                system.debug('NoOfDaysinQuarter'+NoOfDaysinQuarter);
                // Anantha 05/09 RSI-523
              //  CountPoolDataBatchClass BatchClass=new CountPoolDataBatchClass(NoOfDaysinQuarter,Month,s,QueueConfig.Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
                DeleteQueueBatchClass BatchClass=new DeleteQueueBatchClass(NoOfDaysinQuarter,Month,s,QueueConfig.Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);   
            	Database.executeBatch(BatchClass, Integer.valueOf(BatchSize_Setting__c.getValues('DeleteQueueBatch-'+s).Size__c));   
                
            }
        }
    }
    
}