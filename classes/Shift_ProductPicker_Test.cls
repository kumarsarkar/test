/*
	@Name: Shift_ProductPicker_Test
	@Description: Test class for the Product Picker page and extension
	@Dependancies: Shift_ProductPicker_Ext.cls, Shift_ProductPicker.page
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/ 

@isTest(seeAllData=true)
private class Shift_ProductPicker_Test {
	
	@isTest static void testPage() {
		
		Pricebook2 tempPB = [SELECT Id, Name from Pricebook2 where isStandard = true and isActive=true LIMIT 1];

		Product2 tempProduct = new Product2(
			Name = 'Increase',
			isActive = true
		);
		insert tempProduct;

		Pricebookentry tempPBE = new Pricebookentry(
			Product2Id = tempProduct.Id,
			Pricebook2Id = tempPB.Id,
			UnitPrice = 100.00,
			isActive = true,
			UseStandardPrice = false			
		);
		Insert tempPBE;				

		Account tempAcc = new Account(
			Name = 'Shift Test Account'
		);
		insert tempAcc;

		Opportunity tempOpp = new Opportunity(
			AccountId = tempAcc.Id,
			Name = 'Shift Test Opp',
			StageName = 'Closed Won',
			CloseDate = Date.Today()
		);
		Insert tempOpp;

		//Create temp reis reports/secotrs/regions/msa
		Sector__c tempSector = new Sector__c(
			Active__c = true,
			Abbreviation__c = 'Apt99',
			Name = 'Apartment99'
		);
		insert tempSector;

		Region__c tempRegion = new Region__c(
			Name = 'Ontario22',
			Abbreviation__c = 'ON22',
			Active__c =  true
		);
		insert tempRegion;

		MSA__c tempMSA = new MSA__c(
			Name = 'Toronto22',
			Abbreviation__c = 'TOR22',
			Active__c =  true
		);
		insert tempMSA;

		Report__c tempReport = new Report__c(
			Name = 'Test Report22',
			Abbreviation__c = 'TREP22',
			Active__c =  true
		);
		insert tempReport; 

		Supported_Region__c suppReg = new Supported_Region__c(
			MSA__c = tempMSA.Id,
			Region__c = tempRegion.Id
		);
		insert suppReg;

		Supported_Region_Report__c suppRegRep = new Supported_Region_Report__c(
			Region__c =  tempRegion.Id,
			Reis_Report__c = tempReport.Id	
		);
		insert suppRegRep;

		Supported_Report_Sector__c suppRepSec = new Supported_Report_Sector__c(
			Sector__c =  tempSector.Id,
			Reis_Report__c = tempReport.Id	
		);
		insert suppRepSec;	
		
		Supported_Sector_Region__c suppSecReg = new Supported_Sector_Region__c(
			Region__c =  tempRegion.Id,
			Sector__c = tempSector.Id	
		);
		insert suppSecReg;			

		List<String> tempSelectedSectorIds = new List<String>();
		tempSelectedSectorIds.add(tempSector.Id);

		List<String> tempSelectedRegionIds= new List<String>();
		tempSelectedRegionIds.add(tempRegion.Id);
		Set<String> tempSelectedRegionIdsSET= new Set<String>();
		tempSelectedRegionIdsSET.add(tempRegion.Id);

		List<String> tempSelectedMSAIds = new List<String>();
		tempSelectedMSAIds.add(tempMSA.Id);

		List<String> tempSelectedReportIds = new List<String>();
		tempSelectedReportIds.add(tempReport.Id);						

		//Set the current page
		PageReference pageRef = page.Shift_ProductPicker;
		Test.SetCurrentPageReference(pageRef);
		//pageRef.getParameters().put('id', tempAcc.Id);
		
		//Instiatiate the controller for successful operation
		Shift_ProductPicker_Ext controller = new Shift_ProductPicker_Ext(new ApexPages.StandardController(tempOpp));
		//Call controller methods 
		controller.selectedProductId = tempProduct.Id;

		controller.selectedSectorIds = tempSelectedSectorIds;
		controller.selectedRegionIds = tempSelectedRegionIds;
		controller.selectedMSAIds = tempSelectedMSAIds;
		controller.selectedReportIds = tempSelectedReportIds;

		controller.reportStr = tempReport.Id +';';
		controller.sectorStr = tempSector.Id +';';
		controller.msaStr = tempMSA.Id +';';
		controller.msaShortStr = tempMSA.Id +';';
		controller.msaSearchText = tempMSA.Abbreviation__c;		

		controller.init(); 
		controller.isCapped = true;
		controller.editMode = false;
		controller.isIncreaseProduct = true;
		controller.isAnalyticReport = true;
		controller.prePopulateCheckList();
		controller.checkDisableSelectList();
		controller.generateProductValue();
		controller.generateSectorValues();
		controller.selectRegionMSAs();
		controller.generateMSAValues();
		controller.generateMSAShortValues();
		controller.generateReportValues();
		controller.theSaveNew();
		controller.theSave();
		controller.theCancel();
		controller.saveTheRecord();
		controller.editExistingOLI();
		controller.validateProductTypeSettings();
		controller.theMSASearchSelect();
		controller.theMSASearchDeSelect();
		controller.genReportList(tempSelectedRegionIdsSET);
		controller.getSectorList();
		controller.genRegionList(tempSelectedRegionIdsSET);
		controller.genMSAList(tempSelectedRegionIdsSET);
		controller.getProductList();	

		OpportunityLineItem tempOLI = new OpportunityLineItem(
			PricebookentryId =  tempPBE.Id,
			OpportunityId = tempOpp.Id,
			Quantity = 1,
			UnitPrice = 100.00,
			MSA__c = tempMSA.Id,
			Full_MSA__c = tempMSA.Id,
			Reports__c = tempReport.Id,
			Sector__c = tempSector.Id,
			Analytic__c = 'TEST ANALYTIC'
		);
		insert tempOLI;

		pageRef.getParameters().put('selOLIId', tempOLI.Id);	

		Shift_ProductPicker_Ext controller2 = new Shift_ProductPicker_Ext(new ApexPages.StandardController(tempOpp));
		//Call controller methods 
		controller2.selectedProductId = tempProduct.Id;
		controller2.OLI = tempOLI;
		controller2.OppLineId = tempOLI.Id;
		controller2.opp = tempOpp;
		controller2.selectedProductName = tempProduct.Name;
		controller2.selectedSectorIds = tempSelectedSectorIds;
		controller2.selectedRegionIds = tempSelectedRegionIds;
		controller2.selectedMSAIds = tempSelectedMSAIds;
		controller2.selectedReportIds = tempSelectedReportIds;

		controller2.reportStr = tempReport.Id +';';
		controller2.sectorStr = tempSector.Id +';';
		controller2.msaStr = tempMSA.Id +';';
		controller2.msaShortStr = tempMSA.Id +';';
		controller2.msaSearchText = tempMSA.Abbreviation__c;

		controller2.init();
		controller2.isCapped = false;
		controller2.editMode = true;
		controller2.isIncreaseProduct = false;
		controller2.isAnalyticReport = false;		 
		controller2.theSaveNew();
		controller2.theSave();
		controller2.theCancel();
		controller2.saveTheRecord();
		controller2.editExistingOLI();
		controller2.validateProductTypeSettings(); 


		OpportunityLineItem tempOLI2 = new OpportunityLineItem(
			PricebookentryId =  tempPBE.Id,
			OpportunityId = tempOpp.Id,
			Quantity = 1,
			UnitPrice = 100.00,
			ServiceDate = Date.Today()
		);
		insert tempOLI2;
		pageRef.getParameters().put('selOLIId', tempOLI2.Id);
		controller2.OLI = tempOLI2;
		controller2.OppLineId = tempOLI2.Id;
		controller2.opp = tempOpp;			
		controller2.theSave();
		controller2.theSaveNew();

		//Test the save validation
		tempOLI2.Site_End_Date__c = Date.Today();
		Update tempOLI2;
		controller2.theSave();
		controller2.theSaveNew();				

		tempOLI2.Sector__c = NULL;
		Update tempOLI2;
		controller2.theSave();
		controller2.theSaveNew();	

		tempOLI2.Sector__c = 'TEST';
		tempOLI2.Reports__c = NULL;
		Update tempOLI2;
		controller2.theSave();
		controller2.theSaveNew();						
	}	
}