public class SurveySpaceTypeTriggerHandlerClass {
   
    public static void HandleAfterInsert(Map<Id,Survey_Space_Type__c> NewMap){
          
       populateUniqueString(NewMap);

    }
  
    //populate 36characters long string
    public static void populateUniqueString(Map<Id,Survey_Space_Type__c> NewMap){
    List<Survey_Space_Type__c> SurveySpaceList=new List<Survey_Space_Type__c>();
    for(String Space: NewMap.keyset()){
        if(NewMap.get(Space).Id !=null && (NewMap.get(Space).Space_Oid__c== '' || NewMap.get(Space).Space_Oid__c== null)){
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
            system.debug(guid);
            Survey_Space_Type__c SurveySpace=new Survey_Space_Type__c(Id=NewMap.get(Space).Id ,Space_Oid__c= guid.touppercase());
            system.debug('**SurveySpace' +SurveySpace);
            SurveySpaceList.add(SurveySpace);
        }
       
    }
    update SurveySpaceList;
 }
    
 }