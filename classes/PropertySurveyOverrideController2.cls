public class PropertySurveyOverrideController2 {
	public id recordId;
	public Property_Survey__c tempRecord {get;set;}
	public string saveResults {get;set;}
	public List<contactWrapper> contactList {get;set;}
	public static id staticRecordId{get;set;}
	public Map<String, List<amenitiesWrapper>> amenitiesList {get;set;}
	public List<Schema.FieldSetMember> secondFloorPlan {get;set;}
	public Survey_Amenities__c newAm {get;set;}
	public Property_Survey__c unChangedRecord {get;set;}

	//custom setter and getter to ensure when we save all the records are properly updated
	public List<sstWrapper> floorplans {
		get{
			return floorplans;
		}
		set{
			floorplans = value;
		}}

	//Methods to get the fields from the fieldsets
	public List<Schema.FieldSetMember> getSurveyContactFields(){
		return SObjectType.Survey_Contact__c.FieldSets.SurveyContactFields.getFields();
	}
	public List<Schema.FieldSetMember> getReisContactFields(){
		return sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields();
	}

	public List<Schema.FieldSetMember> getLocationFields(){
		return SObjectType.Property_Survey__c.FieldSets.PropertyLocation.getFields();
	}

	public List<Schema.FieldSetMember> getInformationFields(){
		return SObjectType.Property_Survey__c.FieldSets.PropertyInformation.getFields();
	}

	public List<Schema.FieldSetMember> getFloorpanHeaderFields(){
		Return SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurveyHeader.getFields();
	}

	//Sumedha: Added for Office and Industrial
	public List<Schema.FieldSetMember> getOfficeFloorpanHeaderFields(){
			Return SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeader.getFields();
	}

	//Sumedha: Added for Office and Industrial
	public List<Schema.FieldSetMember> getOfficeFloorpanHeaderAllFields(){
			Return SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeaderNew.getFields();
	}

	public List<Schema.FieldSetMember> getSeniorSurveypanHeaderFields(){
    Return SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeader.getFields();
  }

	public List<fieldWrapper> getFloorPlanFields(){
		List<fieldWrapper> returnList = new List<fieldWrapper>();
		List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		tempList = SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields();
		//Sumedha: Added for Office.
		if(tempRecord.RecordType.name == 'Office' || tempRecord.RecordType.name == 'Industrial'){
				tempList = SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeaderNew.getFields();
		}
		if(tempRecord.RecordType.name == 'Apartments'){
				tempList = SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields();
		}
		Double counter = 0;
		Double total = tempList.size();
		for(Schema.FieldSetMember fsm :tempList){
			fieldWrapper fw = new fieldWrapper();
			if(counter/total < .5){
				fw.afield = fsm;
				fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
			}
			else{
				break;
			}
			counter++;
			returnList.add(fw);
		}
		return returnList;
	}

	//fieldWrapper is used with field sets for the survey space type records related list, allows us to set editable and readonly fields seperatly
	public class fieldWrapper{
		public Schema.FieldSetMember afield {get;set;}
		public Schema.FieldSetMember bfield {get;set;}

		public fieldWrapper(){
			//this.afield = new Schema.FieldSetMember();
			//this.bfield = new Schema.FieldSetMember();
		}
	}
	//getFields method only returns to us the fields of the property survey record
	public List<Schema.FieldSetMember> getFields(){
		List<Schema.FieldSetMember> returnList = new List<Schema.FieldSetMember>();
		returnList.addAll(getLocationFields());
		returnList.addAll(getInformationFields());
		return returnList;
	}
	//gets us the survey space type records we need to use in the related list, also uses field sets to get data
	//we can expand this to get different fieldsets based on record type
	public List<sstWrapper> getFloorPlanMap(){
		List<sstWrapper> returnMap = new List<sstWrapper>();
		String queryString = 'Select ';
		for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
		QueryString += 'Id, Name,Delete_By_Foundation__c,Apt_Space_Type__c,Free_Rent_Notes__c,Reis_Space_Type__c, Reis_Space_Type__r.name From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
		for(Survey_Space_Type__c sst :Database.query(QueryString)){
				returnMap.add(new sstWrapper(sst));
		}
		return returnMap;
	}

	public List<sstWrapper> getAffordableFloorPlanMap(){
			List<sstWrapper> returnMap = new List<sstWrapper>();
			String queryString = 'Select ';
			for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.AffordableRentFields.getFields()){
					QueryString += f.getFieldPath()+', ';
			}
			for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitFields.getFields()){
					QueryString += f.getFieldPath()+', ';
			}
			for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitSizes.getFields()){
					QueryString += f.getFieldPath()+', ';
			}
			for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.AffordableVacancyFields.getFields()){
					QueryString += f.getFieldPath()+', ';
			}
			QueryString += 'Id, Name,Delete_By_Foundation__c,Apt_Space_Type__c,Apt_Space_Type__r.name,Reis_Space_Type__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
			for(Survey_Space_Type__c sst :Database.query(QueryString)){
					returnMap.add(new sstWrapper(sst));
			}
			return returnMap;
	}

	//added by sumedha for senior housing
  public List<fieldWrapper> getSeniorSurveyHousingFields(){
    List<fieldWrapper> returnList = new List<fieldWrapper>();
    List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
    tempList = SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeaderFields.getFields();
    Double counter = 0;
    Double total = tempList.size();
    for(Schema.FieldSetMember fsm :tempList){
      fieldWrapper fw = new fieldWrapper();
      if(counter/total < .5){
        fw.afield = fsm;
        fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
      }
      else{
        break;
      }
      counter++;
      returnList.add(fw);
    }

    system.debug('returnList*' + returnList);
    return returnList;
  }
	//added by sumedha for senior housing
   public List<sstWrapper> getSeniorSurveyMap(){
     List<sstWrapper> returnMap = new List<sstWrapper>();
     String queryString = 'Select ';

     for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeaderFields.getFields()){
       QueryString += f.getFieldPath()+', ';
     }

     QueryString += 'Id,Name,SR_Housing_Has_Units__c,Delete_By_Foundation__c,Apt_Space_Type__c,Reis_Space_Type__c,Senior_Housing_Space_Type__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
     for(Survey_Space_Type__c sst :Database.query(QueryString)){
         returnMap.add(new sstWrapper(sst));
     }

     system.debug('prop survey**' + QueryString);
     system.debug('returnMap sst wrapper**' + returnMap);
     return returnMap;
   }
	//uses custom contact wrapper, we get all survey contacts and reis contacts and put them into the wrapper
	// inoder for us to end the information we need to update the reis contact directly, there are also fields on the survey contact that need to be updated at times as well
	// this helps with that.
	public list<contactWrapper> oldCons{get;set;}
	public List<contactWrapper> getContacts(){
		list<contactWrapper> returnList = new list<contactWrapper>();
		oldCons = new list<contactWrapper>();
		map<String,ContactWrapper> emailMap = new map<String,ContactWrapper>();
		List<id> idList = new List<id>();
		String queryString = 'Select ';
		For(Schema.FieldSetMember f: This.getSurveyContactFields()){
			QueryString += f.getFieldPath()+', ';
		}
		QueryString += 'Id, Contact__c,Contact__r.oid__c From Survey_Contact__c Where Property_Survey__c = :recordId ORDER BY Most_Recent_Contact__c DESC';
		for(Survey_Contact__c s :Database.query(QueryString)){
			if(emailMap.get(s.Contact__r.oid__c) == null){
				emailMap.put(s.Contact__r.oid__c, new contactWrapper(s));
				idList.add(s.contact__c);
			}
		}
		String newQueryString = 'Select ';
		for(Schema.FieldSetMember f :sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields()){
			newQueryString += f.getFieldPath()+', ';
		}
		newQueryString += 'Id, Name, first_name__c,oid__c, last_name__c from Reis_Contact__c where id in :idList';
		for(Reis_Contact__c r:Database.query(newQueryString)){
			if(emailMap.get(r.oid__c) != null){
				emailMap.get(r.oid__c).rCon = r;
			}
		}
		oldCons = emailMap.values();
		Return emailMap.values();
		//Return Database.query(QueryString);
	}

	public Property_Survey__c getTheRecord(){
		//gets us the fields we need to use for the viewing survey contact record, we use a field set, and some fields we think will always be on
		String QueryString = 'Select Id, ';
		For(Schema.FieldSetMember f: This.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
		For(Schema.FieldSetMember fs: This.getOfficeFloorpanHeaderAllFields()){
				QueryString += fs.getFieldPath()+', ';
		}
		QueryString += 'Name,Unit_Mix_Estimated__c,Total_Units__c ,Total_NC_Units__c ,Leasing_Incentives_New__c, Licensed_Unit_Total_New__c ,Rent_ref__c,Rest_neg__c,Vac_Ref__c,Ref_Rest__c,DK_rest__c, Current_Available__c,Available_2_4_weeks__c,Total_Available_Units__c, Total_NC_Units_New__c,Total_NC_Units_Current__c, Facility_Name__c,Select_Code_New__c , RecordType.Name, Total_Units_Current__c,Total_Units_New__c, Lease_Term_Current__c,Minimum_Qualifying_Income__c,Vacancy_MR_Total__c ,Vacancy_TC_30_Totals__c,Comp_Totals__c ,Section_8_Contract_End_Date__c, Vacancy_TC_40_Totals__c,Comp_Totals_Avail__c ,Vacancy_S8_Total__c,cam__c,Vacancy_Tax_Credit_Total__c,Expected_Increase__c,Entry_Fee_High_New__c,Total_Available_New__c,Entry_Fee_Average_New__c,Comp_Totals_Avail_Hold__c,Total_Units_Available_New__c, Vacancy_NonConv_Total__c,Total_Vacancy__c,Commercial_Space__c,Sold_Amt__c,Project_Origin_Current__c,Project_Origin_New__c,Status__c,Call_Status__c,Vacancy_Unknown_Totals__c,Vacancy_TC_50_Totals__c,Vacancy_TC_60_Totals__c,Maximum_Qualifying_Income__c,Heating_Source_New__c,Unit_Designation_New__c,Tax_Credit_Year_Placed_Into_Service__c,Tax_Credit_Compliance_Years_Remaining__c,Accepts_Voucher_Tenants__c,Has_Wait_List__c,Wait_List_Length__c,Accepts_Section_8_Vouchers__c,Wait_List_Units__c,Opex_New__c,Last_Surveyed__c,Section_8_Contract_Start_Date__c,Unit_Sec8_Totals__c,Unit_TC_Totals__c,Unit_Unknown_Totals__c,Unit_NonConven_Totals__c,Unit_TC_60_Totals__c,Unit_TC_50_Totals__c,Unit_TC_40_Totals__c,Unit_TC_30_Totals__c,Unit_MR_Totals__c,Expected_Month__c,PropertyOwnerNew__c,Real_Estate_Taxes_New__c, Lease_Term_New__c, Office_Space_Size__c,Tenancy__c,Insurance_New__c ,Entry_Fee_Low_New__c ,TI_Allowance_New_New__c, TI_Allowance_Renew_New__c, Free_Rent_New__c, Contract_Rent_Discount_New__c, Commission_New_New__c, Commission_Renew_New__c,Sold__c,Sold_Date__c,Sold_Amount_New__c,Occupancy__c From Property_Survey__c Where ID = \''+recordId+'\' LIMIT 1';
		Return Database.query(QueryString);
	}

	public boolean goodToClose {get;set;}
	public void saveRecord(){
		Boolean errorFound = false;
		system.debug(recordCompare(unChangedRecord,tempRecord));
		if(recordCompare(unChangedRecord,tempRecord) && recordCompare(floorplansOrigin,floorplans) && recordCompare(oldCons, contactList) && recordCompare(orginalAmenList,ameneityMapToList(amenitiesList))){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Data Has Changed, The Record Has Not Been Updated');
			ApexPages.addMessage(myMsg);
			return;
		}
		try{
			if((tempRecord.Survey_Source__c != null && tempRecord.Survey_Source__c != '') && tempRecord.Survey_Source__c.contains('Web') && (tempRecord.Survey_Website_New__c == null || tempRecord.Survey_Website_New__c == '')){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You need a web address when the Survey Source is related to Web');
	      ApexPages.addMessage(myMsg);
				return;
			}
            //errorFound = true;
			errorFound = updateAmenities();
			if(errorFound){
				errorFound = insertContacts();
			}
			if(errorFound && (tempRecord.recordtype.name == 'Senior Housing' || tempRecord.recordtype.name == 'Affordable' || tempRecord.recordtype.name == 'Apartments')){
				errorFound = apartmentSave();
			}
			if(errorFound){
				update tempRecord;
				loadInformation();
			}
			goodToClose = errorFound;
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+' at line: '+e.getLineNumber());
      ApexPages.addMessage(myMsg);
		}
		//Boolean errorFound = false;
		for(ApexPages.Message m : apexPAges.getMessages()){
			if(m.getSeverity() == ApexPages.Severity.ERROR){
				errorFound = True;
			}
		}
		if(goodToClose){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully');
      ApexPages.addMessage(myMsg);
		}
	}

	public void newSST(){
		//adds a new survey space type to our wrapper/related list
		if(floorplans == null){
			floorplans = new List<sstWrapper>();
		}
		floorplans.add(new sstWrapper( new Survey_Space_Type__c(Property_Survey__c = recordid)));
		system.debug(floorplans.size());
	}

//office Save

	public Boolean apartmentSave(){
		Boolean isError = false;
		//spesific save logic for the apartments record type
		List<Survey_Space_Type__c> insertList = new List<Survey_Space_Type__c>();
		Map<id, Survey_Space_Type__c> dupeCheck = new Map<id, Survey_Space_Type__c>();
		if(floorplans == null){
			return FALSE;
        //Need to distinguish between different kinds of space types - this only works for affordable and apartment
		}
        if(tempRecord.recordtype.name == 'Apartments' || tempRecord.recordtype.name == 'Affordable'){
            for(sstWrapper s :floorplans){
                if(dupeCheck.get(s.sst.Apt_Space_Type__c) == null){
                    dupeCheck.put(s.sst.Apt_Space_Type__c, s.sst);
                }
                else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Space Type Found');
                    ApexPages.addMessage(myMsg);
                    isError = true;
                }
			if(s.sst.Apt_Space_Type__c != null){
      s.sst.Reis_Space_Type__c = s.sst.Apt_Space_Type__c;
			}
				insertList.add(s.sst);
			}
        }else if(tempRecord.recordtype.name == 'Senior Housing'){
            for(sstWrapper s :floorplans){
                if(dupeCheck.get(s.sst.Senior_Housing_Space_Type__c) == null){
                    dupeCheck.put(s.sst.Senior_Housing_Space_Type__c, s.sst);
                }
                else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Space Type Found');
                    ApexPages.addMessage(myMsg);
                    isError = true;
                }

                  if(s.sst.Senior_Housing_Space_Type__c != null){
                      s.sst.Reis_Space_Type__c = s.sst.Senior_Housing_Space_Type__c;
                  }
			    insertList.add(s.sst);
			    }
        }
		if(!isError){
			upsert insertList;
		}
		return !isError;
	}

	public void AddToConList(){
		//adds a new contact to the contact wrapper / related list
		contactList = contactWrapper.addNewCon(contactList);
	}

	public Boolean insertContacts(){
		//save method for the contacts, there was no indication that we had to check for items to change
		//so we just update all the records with the values we have
		List<Survey_Contact__c> insertList = new List<Survey_Contact__c>();
		List<Survey_Contact__c> insertList2 = new List<Survey_Contact__c>();
		List<Survey_Contact__c> updateSCList = new List<Survey_Contact__c>();
		List<Reis_Contact__c> updateRCList = new List<Reis_Contact__c>();
		List<Reis_Contact__c> newContactList = new List<Reis_Contact__c>();
		for(contactWrapper c :contactList){
			system.debug(c);
			if(c.newcon && c.con.Contact__c == null){
				Reis_Contact__c r = c.rCon;
				r.name = r.first_name__c+' '+r.last_name__c;
				newContactList.add(r);
				insertList.add(c.con);
				c.newCon = false;
			}
			else if(c.newcon && c.con.Contact__c != null){
				c.con.Property_Survey__c = recordId;
				insertList2.add(c.con);
				c.newCon = false;
			}
			else{
				if(c.con.id != null && c.rcon.id != null){
					updateSCList.add(c.con);
					updateRCList.add(c.rcon);
				}
			}
		}

		if(insertList2.size() > 0){
			try{
				insert insertList2;
				system.debug(insertList2);
			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
				ApexPages.addMessage(myMsg);
				return FALSE;
			}
		}

		if(insertList.size() > 0 && newContactList.size() > 0){
			try{
				insert newContactList;
				Integer count = 0;
				for(Survey_Contact__c c :insertList){
						c.Property_Survey__c = recordId;
						c.Contact__c = newContactList[count].id;
						count++;
				}
				insert insertList;

			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
	      ApexPages.addMessage(myMsg);
				return FALSE;
			}
		}
		if(updateSCList.size() > 0 && updateRCList.size() > 0){
			try{
				update updateSCList;
				update updateRCList;
			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
	      ApexPages.addMessage(myMsg);
			}
		}
		getContacts();
		return TRUE;
	}

	public PageReference sumbitForApproval(){
		//baisc submit for approval
		if(recordCompare(unChangedRecord,tempRecord) && recordCompare(floorplansOrigin,floorplans) && recordCompare(oldCons, contactList)){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Data Has Changed, The Record Has Not Been Submitted');
			ApexPages.addMessage(myMsg);
			return null;
		}
	try{
		tempRecord.Status__c = 'In Progress';
		update tempRecord;
		Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		req.setComments('Submitting request for approval'); // You can make comments dynamic
		req.setObjectId(RecordId);
		Approval.ProcessResult result = Approval.process(req);
	}
	catch (Exception ex){
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()+ 'At Line: '+ex.getLineNumber());
		ApexPages.addMessage(myMsg);
	}
	Boolean errorFound = false;
	for(ApexPages.Message m : apexPAges.getMessages()){
		if(m.getSeverity() == ApexPages.Severity.ERROR){
			errorFound = True;
		}
	}
	if(!errorFound){
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully');
		ApexPages.addMessage(myMsg);
		goodToClose = true;
	}
	//return submitPageRef;
	return null;
}

	public PropertySurveyOverrideController2(ApexPages.StandardController controller) {
		//constructor
		system.debug('new run of the controller');
		recordId = controller.getId();
		staticRecordId = controller.getId();
		loadInformation();
		goodToClose = false;
	}

	public list<sstWrapper> floorplansOrigin{get;set;}
	public void loadInformation(){
		tempRecord = getTheRecord();
		floorplansOrigin = new list<sstWrapper>();
		unChangedRecord = tempRecord.clone(true, true, true, true);
		system.debug(recordCompare(unChangedRecord,tempRecord));
		contactList = getContacts();
		generateAmenityList();
		if(tempRecord.recordtype.name == 'Apartments'){
			floorplans = getFloorPlanMap();
			floorplansOrigin = getFloorPlanMap();
		}
		if(tempRecord.recordtype.name == 'Affordable'){
			floorplans = getAffordableFloorPlanMap();
			floorplansOrigin = getAffordableFloorPlanMap();
		}
		if(tempRecord.recordtype.name == 'Senior Housing'){
			floorplans = getSeniorSurveyMap();
			floorplansOrigin = getSeniorSurveyMap();
		}

		system.debug(recordCompare(floorplansOrigin,floorplans));
	}

	public class sstWrapper{
		//survey space tpye wrapper so we can create new survey space type records in our related list
		public Survey_Space_Type__c sst {Get;set;}
		public boolean newSst {get;set;}

		public sstWrapper(){
			this.sst = new Survey_Space_Type__c();
			this.newSst =false;
		}

		public sstWrapper(Survey_Space_Type__c s){
			this.sst = s;
			this.newSst =false;
		}
	}

	@remoteAction
    public static void contactInfo(){

    }

	@remoteAction
	public static void logACall(String a, String rid){
		//log a call remote action for use on the VF Page
		//depricated
		task t = new task();
		t.whatId = rid;
		t.subject = 'Call';
		t.subject =a;
		t.status = 'Completed';
		t.description = 'Call at: '+system.now();
		t.activityDate = system.today();
		insert t;
		system.debug(t.id);
	}

	@remoteAction
	public static void remoteSave(Boolean b, String rid){
		//used in out reocrd type change validation
		Property_Survey__c a = [select id, needs_qa_review__c from Property_Survey__c where id = :rid limit 1];
		a.needs_qa_review__c = b;
		update a;
	}

	@remoteAction
	public static void remoteAddComment(string newComment, String rid){
		//used on all most all of our validations
		Property_Survey__c a = [select id, needs_qa_review__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
		//a.QC_Notes_New__c += ' ('+system.now()+') '+newComment;
		if(a.Property_Notes_New__c == null){
			a.Property_Notes_New__c = ' ('+system.now()+') '+newComment;
		}
		else{
			a.Property_Notes_New__c += ' ('+system.now()+') '+newComment;
		}

		update a;
	}

	@remoteAction
public static void remoteAddQCComment(string newComment, String rid){
	system.debug(rid);
	system.debug(newComment);
	Property_Survey__c a = [select id, needs_qa_review__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
	if(a.QC_Notes_New__c == null){
		a.QC_Notes_New__c = ' ('+system.now()+') '+newComment;
	}
	else{
		a.QC_Notes_New__c += ' ('+system.now()+') '+newComment;
	}
	system.debug(a);
	update a;
	system.debug(a);
}

	@remoteAction
public static void remoteAddQAComment(string newComment, String rid){
	Property_Survey__c a = [select id, needs_qa_review__c, QA_Reason_Notes__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
			if(a.QA_Reason_Notes__c == null){
		a.QA_Reason_Notes__c = ' ('+system.now()+') '+newComment;
	}
	else{
		a.QA_Reason_Notes__c += ' ('+system.now()+') '+newComment;
	}

	update a;
}

	@remoteAction
	public static Boolean remoteSiteCheck(String toCheck, Boolean propertySite){
		//used on site fields to determine if they are on our restricted list, if they are, we blank out on the client side
		system.debug(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()));
		system.debug(toCheck);
		if(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()) != null){
			if(propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).property_site__c){
				return true;
			}
			else if(!propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).Survey_Site__c){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	@remoteAction
	public static string remoteCompanyAdd(String name, String webAddress, String phone){
		Company__c c = new Company__c(name = name, Company_Website__c = webAddress, Main_Phone__c = phone, active__c = true);
		insert c;
		return c.id;
	}

	@remoteAction
	public static Boolean amenityHasQuantaty(String amenityId){
		Amenity__c ra = [select id,Has_Quantity__c from Amenity__c where id = :amenityId limit 1];
		return ra.Has_Quantity__c;
	}

	@remoteAction
	public static decimal remoteContactJob(String stName){
		Reis_Space_Type__c st = [select id,Bedrooms__c from Reis_Space_Type__c where name = :stName limit 1];
		return st.Bedrooms__c;
	}

	public class amenitiesWrapper{
		//amenitiesWrapper is used to add new records to our related lists
		// also contains the type field so we can determine which list to populate with which record
		public Survey_Amenities__c sa {get;set;}
		public String type {get;set;}

		public amenitiesWrapper(){
			this.sa = new Survey_Amenities__c();
			this.type = '';
		}

		public amenitiesWrapper(Survey_Amenities__c s, String t){
			this.sa = s;
			this.type = t;
		}
	}

List<amenitiesWrapper> orginalAmenList {get;set;}
	public void generateAmenityList(){
		//populate our amenity wrapper list
		//set our new amenity to a blank amenity so we can create new ones quickly
		newAm = new Survey_Amenities__c();
		amenitiesList = new Map<String,List<amenitiesWrapper>>();
		amenitiesList.put('Complex', new List<amenitiesWrapper>());
		amenitiesList.put('Unit', new List<amenitiesWrapper>());
		amenitiesList.put('Utilities', new List<amenitiesWrapper>());
		amenitiesList.put('Social Services', new List<amenitiesWrapper>());
		amenitiesList.put('Office', new List<amenitiesWrapper>());
		orginalAmenList = new list<amenitiesWrapper>();
		for(Survey_Amenities__c s :[select id,Amenity__r.Has_Quantity__c,/*Sr_Housing_Amenity__c,*/Industrial_Amenities__c, Delete_By_Foundation__c,Apartment_Amenity__c,Quantity_New__c, Amenity__r.Name, Amenity__r.Type__c from Survey_Amenities__c where Property_Survey__c = :recordId AND Amenity__c != null]){
			if(amenitiesList.get(s.Amenity__r.Type__c) == null){
				amenitiesList.put( s.Amenity__r.Type__c,new List<amenitiesWrapper>());
				amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
			}else{
				amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
			}
			orginalAmenList.add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
		}
	}

	public List<amenitiesWrapper> ameneityMapToList(Map<string,list<amenitiesWrapper>> convertMap){
		List<amenitiesWrapper> returnList = new List<amenitiesWrapper>();
		for(String s :convertMap.keyset()){
				returnList.addAll(convertMap.get(s));
		}

		return returnList;
	}

	// Sumedha : Added for the Industrial Sector
	public void addNewAmmenityIndustry(){
			//inserts our new amenity and updates our pages viewing list
			List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
			For(List<amenitiesWrapper> a :amenitiesList.values()){
				fulllist.addAll(a);
			}

			newAm.Property_Survey__c = recordId;
			newAm.Amenity__c = newAm.Industrial_Amenities__c;
			Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Industrial_Amenities__c limit 1];
			for(amenitiesWrapper a :fulllist){
					if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
							return;
					}
					else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
							return;
					}
					else if(a.sa.Amenity__r.Name == am.name){
						return;
					}
			}
			insert newAm;
			generateAmenityList();
}

	public void addNewAmmenity(){
		//inserts our new amenity and updates our pages viewing list
		List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
		For(List<amenitiesWrapper> a :amenitiesList.values()){
			fulllist.addAll(a);
		}

		newAm.Property_Survey__c = recordId;
		if(tempRecord.recordtype.Name == 'Apartments' || tempRecord.recordtype.Name == 'Affordable'){
			newAm.Amenity__c = newAm.Apartment_Amenity__c;
		}
		else if( tempRecord.recordType.Name == 'Industrial'){
			newAm.Amenity__c = newAm.Industrial_Amenities__c;
		}
/*		else if(tempRecord.recordType.Name == 'Senior Housing'){
			newAm.Amenity__c = newAm.Sr_Housing_Amenity__c;
		} */

		Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Apartment_Amenity__c limit 1];
		for(amenitiesWrapper a :fulllist){
			if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
				return;
			}
			else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
				return;
			}
			else if(a.sa.Amenity__r.Name == am.name){
				return;
			}
		}
		try{
			insert newAm;
			generateAmenityList();
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
		}
	}

	public Boolean updateAmenities(){
		//updates any changes we made to existing amenities
		List<Survey_Amenities__c> updateList = new List<Survey_Amenities__c>();
		List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
		For(List<amenitiesWrapper> a :amenitiesList.values()){
			fulllist.addAll(a);
		}
		for(amenitiesWrapper aw :fullList){
			if((aw.sa.Quantity_New__c == 0 || aw.sa.Quantity_New__c == null) && aw.sa.Amenity__r.Has_Quantity__c){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,aw.sa.Amenity__r.Name+': Amenity Needs a Quantity Greater than 0');
	      ApexPages.addMessage(myMsg);
				return FALSE;
			}
			aw.sa.Amenity__c = aw.sa.Apartment_Amenity__c;
			updateList.add(aw.sa);
		}

		try{
			update updateList;
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
			return FALSE;
		}
		return TRUE;
	}

//depricated
	public String removalId {get;set;}
	public void removeAmenity(){
		system.debug(removalId);
		Survey_Amenities__c ps = [select id from Survey_Amenities__c where id = :removalId limit 1];
		delete ps;
		generateAmenityList();
	}


	/////////////AFFORDABLE///////////////////////

	//Sumedha: Added for Affordable
	public List<Schema.FieldSetMember> getAffordableRentnHeaderFields(){
			Return SObjectType.Survey_Space_Type__c.FieldSets.AffordableRentHeader.getFields();
	}

	//Sumedha: Added for Affordable
	public List<Schema.FieldSetMember> getAffordableVacancyHeaderFields(){
			Return SObjectType.Survey_Space_Type__c.FieldSets.AffordableVacancyHeader.getFields();
	}

	//Sumedha: Added for Affordable
	public List<Schema.FieldSetMember> getAffordableUnitHeaderFields(){
			Return SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitHeader.getFields();
	}

	public List<Schema.FieldSetMember> getAffordableUnitSizeHeaderFields(){
			Return SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitSizesHearders.getFields();
	}


	//Sumedha: Added for Affordable
	public List<fieldWrapper> getFloorPlanRentFields(){
			List<fieldWrapper> returnList = new List<fieldWrapper>();
			List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
			tempList = SObjectType.Survey_Space_Type__c.FieldSets.AffordableRentFields.getFields();
			Double counter = 0;
			Double total = tempList.size();
			for(Schema.FieldSetMember fsm :tempList){
					fieldWrapper fw = new fieldWrapper();
					if(counter/total < .5){
							fw.afield = fsm;
							fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
					}
					else{
							break;
					}
					counter++;
					returnList.add(fw);
			}
			system.debug('**********FloorPlanRentFields' +returnList);
			return returnList;
	}

	//Sumedha: Added for Affordable
	 public List<fieldWrapper> getFloorPlanVacancyFields(){
			List<fieldWrapper> returnList = new List<fieldWrapper>();
			List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
			tempList = SObjectType.Survey_Space_Type__c.FieldSets.AffordableVacancyFields.getFields();
			Double counter = 0;
			Double total = tempList.size();
			for(Schema.FieldSetMember fsm :tempList){
					fieldWrapper fw = new fieldWrapper();
					if(counter/total < .5){
							fw.afield = fsm;
							fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
					}
					else{
							break;
					}
					counter++;
					returnList.add(fw);
			}
			system.debug('**********FloorPlanVacancyFields' +returnList);
			return returnList;
	}

	//Sumedha: Added for Affordable
	 public List<fieldWrapper> getFloorPlanUnitFields(){
			List<fieldWrapper> returnList = new List<fieldWrapper>();
			List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
			tempList = SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitFields.getFields();
			Double counter = 0;
			Double total = tempList.size();
			for(Schema.FieldSetMember fsm :tempList){
					fieldWrapper fw = new fieldWrapper();
					if(counter/total < .5){
							fw.afield = fsm;
							fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
					}
					else{
							break;
					}
					counter++;
					returnList.add(fw);
			}
			system.debug('**********getFloorPlanUnitFields' + returnList);
			return returnList;
	}

	public List<fieldWrapper> getFloorPlanUnitsSizeFields(){
		 List<fieldWrapper> returnList = new List<fieldWrapper>();
		 List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		 tempList = SObjectType.Survey_Space_Type__c.FieldSets.AffordableUnitSizes.getFields();
		 Double counter = 0;
		 Double total = tempList.size();
		 for(Schema.FieldSetMember fsm :tempList){
				 fieldWrapper fw = new fieldWrapper();
				 if(counter/total < .5){
						 fw.afield = fsm;
						 fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
				 }
				 else{
						 break;
				 }
				 counter++;
				 returnList.add(fw);
		 }
		 return returnList;
 }

	public static boolean recordCompare(list<Object> orginial, list<Object> vfRecord){
		system.debug(orginial);
		system.debug(vfRecord);
		if((orginial != null && vfRecord != null) && orginial.size() == vfRecord.size()){
			Integer size = orginial.size();
			for(Integer i = 0; i != size; i++){
				if(!recordCompare(orginial[i],vfRecord[i])){
					return false;
				}
			}
			return true;
		}
		else{
			return false;
		}
	}


	public static boolean recordCompare(Object orginial, Object vfRecord){
		if(orginial != vfRecord){
			if(orginial != null && vfRecord != null){
				try{
					map<string,object> orgMap = (map<string,object>)JSON.deserializeUntyped(JSON.serialize(orginial));
					map<string,object> vfMap = (map<string,object>)JSON.deserializeUntyped(JSON.serialize(vfRecord));
					for(String key :vfMap.keySet()){
						if(orgMap.containsKey(key)?vfMap.get(key)!=orgMap.get(key):vfMap.get(key)!=null){
							if(orgMap.containsKey(key)){
								system.debug('compareSobjects: failed - before/after missmatch for key: '+key);
							}
							else{
								system.debug('compareSobjects: failed - vfMap is not null for key: '+key);
							}
							return false;
						}
					}
				}
				catch(Exception e){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'compareSobjects validation error. Please contact a system administrator. Error: '+e.getMessage()));
					return false;
				}
			}
			else{
				system.debug('compareSobjects: failed - on or more SObjects were null');
				return false;
			}
		}
		system.debug('compare success');
		return true;
	}
}