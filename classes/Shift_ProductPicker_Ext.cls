/*
	@Name: Shift_ProductPicker_Ext
	@Description: Extension for Product Picker
	@Dependancies: Shift_LeadForm.page
	@Version: 1.0.0

	===VERSION HISTORY ===
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

public class Shift_ProductPicker_Ext {

	public static final String CAPPED_SE = 'Capped SE';
	public static final String RENEWAL_PRODUCT = 'Renewal';
	public static final String INCREASE_PRODUCT = 'Increase';
	public static final String NEWBUSINESS_PRODUCT = 'New Business';
	public static final String TRIAL_PRODUCT = 'Trial';
	public static final String MOBIUSS_REVENUETYPE = 'Mobiuss';
	public static final String ALL_REPORTS = 'All Reports';
	public static final String ALL_SECTORS = 'All Sectors';
	public static final set<String> productNameSet = new set<String> {
		RENEWAL_PRODUCT,
		INCREASE_PRODUCT,
		NEWBUSINESS_PRODUCT,
		TRIAL_PRODUCT
	};
	public list<string> selectedMSAIdsFULL = new list<string>(); 
	public Map<string, Set<String>> RegionHasMSAMap = new Map <string, Set <string>> ();
	set<String> fullMSASet = new set<string>(); 
	public Opportunity opp;
	public Id OppLineId;
	public OpportunityLineItem newOLI;

	//public map<Id, MSA__c> completeMSAMap = new map<Id, MSA__c>();
	public map<Id, MSA__c> completeMSAMap {get;set;}
	public map<Id, Report__c> completeReportMap = new map<Id, Report__c>();
	public map<Id, Sector__c> completeSectorMap = new map<Id, Sector__c>();
	public map<Id, Region__c> completeRegionMap = new map<Id, Region__c>();
	public map<Id, Product2> productMap = new map<Id, Product2>();
	public map <Id, OpportunityLineItem> existingOLIMap {get;set;}
	public boolean editMode {get;set;}
	public boolean isCapped {get;set;}
	public boolean isIncreaseProduct {get;set;}
	public boolean isAnalyticReport {get;set;}
	public list<SelectOption> MSAFullList {get;set;}
	public list<selectOption> MSADisplayList {get;set;}
	public list<selectOption> MSAFilteredList {get;set;}
	public list<selectOption> ReportList {get;set;}
	public list<selectOption> RegionList {get;set;}
	public list<string> selectedReportIds {get;set;}
	public list<string> selectedSectorIds {get;set;}
	public list<string> selectedMSAIds {get;set;}
	public list<string> selectedRegionIds {get; set;}
	public string selectedProductId {get;set;}
	public string selectedProductName {get;set;}
	public OpportunityLineItem OLI {get;set;}
	public string reportStr {get; set;}
	public string sectorStr {get; set;}
	public string msaStr {get; set;}
	public string msaShortStr {get; set;}
	public string msaSearchText {get; set;}

	ApexPages.StandardController m_controller;

	public Shift_ProductPicker_Ext(ApexPages.StandardController controller) {
		m_controller = controller;
		isCapped = false;
		editMode = true;
		isIncreaseProduct = false;
		isAnalyticReport = false;

		//Field are hardcoded because they are used before the list is processed (for validiation rules)
		List<String> addFieldsList = new List<String>();
		if(!Test.isRunningTest()) {
			addFieldsList.add('Pricebook2Id');
			controller.addFields(addFieldsList);
		}
		opp = (opportunity) controller.getrecord();

		//Query out all the active Sectors, Reports and MSAs
		completeMSAMap = new Map<Id, MSA__c>([SELECT Id, Name, Abbreviation__c, Market_Type__c  FROM MSA__c WHERE Active__c = TRUE ORDER BY Name ASC]);
		completeReportMap = new Map<Id, Report__c>([SELECT Id, Name, Abbreviation__c, Aggregation_Level__c, Sort_Order__c FROM Report__c WHERE Active__c = TRUE ORDER BY Name ASC]);
		completeSectorMap = new Map<Id, Sector__c>([SELECT Id, Name, Abbreviation__c, Sort_Order__c  FROM Sector__c WHERE Active__c = TRUE ORDER BY Name DESC]);
		completeRegionMap = new Map<Id, Region__c>([SELECT Id, Name, Abbreviation__c, Sort_Order__c FROM Region__c WHERE Active__c = TRUE ORDER BY Name ASC]);

		productMap = new Map<Id, Product2>([SELECT Id, Name, Family FROM Product2 WHERE Name IN :productNameSet]);
		existingOLIMap = new Map<Id, OpportunityLineItem> ([SELECT Id, Full_MSA__c, Reports__c, Sector__c, Product_Type__c, ServiceDate, Site_End_Date__c, UnitPrice, Frequency__c,
																		PricebookEntry.Product2Id, Capped_Amount__c, MSA__c, Notes__c, Analytic__c
																		FROM OpportunityLineItem WHERE OpportunityId = :opp.Id]);

		//call page load OLI details
		init();
	}

	/** Init functions for the page load **/
	public void init() {
		//Initialize variables for multi-select
		selectedReportIds = new list<string>();
		selectedSectorIds = new list<string>();
		selectedMSAIds = new list<string>();
		selectedRegionIds = new list<string>();

		//Instantiate new OLi record and default the qty to 1
		//If on edit screen set the OLI to the one selected for edit
		OppLineId = ApexPages.currentPage().getParameters().get('selOLIId') ;
		if(OppLineId == null) {
			OLI = new OpportunityLineItem(Quantity=1);
			OLI.Frequency__c = 'Quarterly';
			editMode=true; //set the edit mode flag
		} else {
			editMode=false; //set this to false to disable them from editing the revenue type
			OLI = existingOLIMap.get(OppLineId);
			selectedProductId=string.valueOf(OLI.PricebookEntry.Product2Id);
			selectedProductName=productMap.get(OLI.PricebookEntry.Product2Id).Name;
			reportStr=OLI.Reports__c;
			sectorStr=OLI.Sector__c;
			msaShortStr=OLI.MSA__c;
			msaStr=OLI.Full_MSA__c;
			checkDisableSelectList();
			prePopulateCheckList();
		}
	}

	/** On launch of page, if editing an existing OLI - pre-populate dynamic lists **/
	public void prePopulateCheckList() {

		//Set the analytic report boolean if an analytic report is chosen
		if (OLI.Analytic__c!=NULL) {
			isAnalyticReport=true;
		}

		//Prepopulate datatable and checkboxes for sectors
		if (OLI.Sector__c!=null) {
			if (OLI.Sector__c != ALL_SECTORS) {
				set<String> tempSet = new set<String>();
				for(String key : OLI.Sector__c.split(';')){
					string tempKey = key.trim();
					tempSet.add(tempKey);
				}
		        for (Id sectorId :completeSectorMap.KeySet()) {
		        	if (tempSet.contains(completeSectorMap.get(sectorId).Abbreviation__c)){
		        		selectedSectorIds.add(sectorId);
		        	}
		        }
			} else {
				for (Id sectorId :completeSectorMap.Keyset()) {
					selectedSectorIds.add(sectorId);
				}
			}

			generateSectorValues();
		}

		//Prepopulate datatable and checkboxes for region
		if ( OLI.MSA__c!=null) {
			set<String> tempSet = new set<String>();
			for(String key : OLI.MSA__c.split(';')){
				string tempKey = key.trim();
				tempSet.add(tempKey);
			}
	        for (Id regionId :completeRegionMap.KeySet()) {
	        	if (tempSet.contains(completeRegionMap.get(regionId).Abbreviation__c)){
	        		selectedRegionIds.add(regionId);
	        	}
	        }

		}

		//Prepopulate datatable and checkboxes for MSA
		if (OLI.Full_MSA__c!=null) {
			set<String> tempSet = new set<String>();
			for(String key : OLI.Full_MSA__c.split(';')){
				string tempKey = key.trim();
				tempSet.add(tempKey);
			}
	        for (Id msaId :completeMSAMap.KeySet()) {
	        	if (tempSet.contains(completeMSAMap.get(msaId).Abbreviation__c)){
	        		selectedMSAIds.add(msaId);
	        	}
	        }
		}

		//Prepopulate datatable and checkboxes for reports
		//If all reports is selected, select all reports from the complete report map
		if (OLI.Reports__c!=null) {
			if (OLI.Reports__c != ALL_REPORTS) {
				set<String> tempSet = new set<String>();
				for(String key : OLI.Reports__c.split(';')){
					string tempKey = key.trim();
					tempSet.add(tempKey);
				}
		        for (Id reportId :completeReportMap.KeySet()) {
		        	if (tempSet.contains(completeReportMap.get(reportId).Abbreviation__c)){
		        		selectedReportIds.add(reportId);
		        	}
		        }
	        } else {
	        	for (Id reportId :completeReportMap.KeySet()) {
	        		selectedReportIds.add(reportId);	
	        	}	
	        }
		}

		generateMSAValues();

	}

	public void checkDisableSelectList(){
		//If the selected product = increase or revenue type = mobiuss, switch the boolean
		//Disables select lists for this product
		String pName = '';
		if (productMap.containsKey(selectedProductId)) {
			pName = productMap.get(selectedProductId).Name;
		}

		if (oli.Product_Type__c == MOBIUSS_REVENUETYPE || pName == INCREASE_PRODUCT) {
			isIncreaseProduct = true;

		} else {
			isIncreaseProduct = false;
		}

		if (oli.Analytic__c != NULL) {
			isAnalyticReport = true;
			selectedReportIds = new List<String>(); //reset the selected reports
			reportStr = '';
		} else {
			isAnalyticReport = false;
		}
	}

	/** Onchange of the product PL, put the product name in str format for the preview table **/
	public pageReference generateProductValue() {
		checkDisableSelectList(); //check to see if select lists need to be disabled

		//selectedProductId!=''
		if (productMap.containsKey(selectedProductId)){
			selectedProductName = productMap.get(selectedProductId).Name;
		} else {
			selectedProductName='';
		}
		return null;
	}

	/** On selection of the sector checkboxes, put the sector abbrev into string and render in preview table **/
	public pageReference generateSectorValues() {
		set<string> displayRegionIdSet = new set<string>();
		set<string> displayMSAIdSet = new set<string>();

		//Reset the selections if all sectors are deselected
		if (selectedSectorIds.isEmpty()) {
			selectedRegionIds = new list<string>();
			selectedMSAIds = new list<string>();
			selectedReportIds = new list<string>();
			reportStr='';
			msaShortStr='';
		}

		sectorStr='';

		if (selectedSectorIds.size()==completeSectorMap.size()) {
			sectorStr = ALL_SECTORS;
        } else {
	        //Loop through the selected sectors to generate delimited string
	        for (String sectorId :selectedSectorIds) {
	        	sectorStr += completeSectorMap.get(sectorId).Abbreviation__c;
	        	sectorStr += ';';
	        }
	        if(sectorStr.length()!=0){sectorStr = sectorStr.Substring(0,sectorStr.length()-1);}        	
        }

        //Loop through sectors to find available regions, if not null generate the region & msa display list
        for (Supported_Sector_Region__c secReg :[SELECT Region__c FROM Supported_Sector_Region__c WHERE Sector__c IN :selectedSectorIds]) {
        	displayRegionIdSet.add(secReg.Region__c);
        }
        if (!displayRegionIdSet.isEmpty()) {
        	RegionList = genRegionList(displayRegionIdSet);
        	MSAFullList = genMSAList(displayRegionIdSet);
        	MSADisplayList = MSAFullList; //Set the display list to the full list
    	}

		return null;
	}

	/** on change of the region field auto select all the MSAs that are for tha region, disabled the select list for the region **/
	public pageReference selectRegionMSAs() {
		List<SelectOption> options = new List<SelectOption>();
		selectedMSAIds = new List<String>();

		//If no region is selected, reset the list back to the full list
		if (selectedRegionIds.isEmpty()) {
			selectedMSAIds = new list<string>();
			msaShortStr='';
			return null;
		}

		//Loop through all junctions to find applicable MSA's
		for (Supported_Region__c suppReg :[SELECT Region__c, MSA__c FROM Supported_Region__c WHERE Region__c IN :selectedRegionIds]) {
			selectedMSAIds.add(suppReg.MSA__c);
		}

		generateMSAValues(); //fire visibility of reports

		return null;
	}

	/** On selection of the msa checkboxes, put the msa abbrev into string and render in preview table **/
	public pageReference generateMSAValues() {
		msaStr='';
		fullMSASet = new set<string>(selectedMSAIdsFULL);
		set<String> partMSASet = new set<string>(selectedMSAIds);

		//If all MSA's are deselected then uncheck the regions
		if (selectedMSAIds.isEmpty()) {
			selectedRegionIds = new List<String>();
		}

		for (string partStr :partMSASet) {
			if (!fullMSASet.contains(partStr)) {
				fullMSASet.add(partStr);
			}
		}
		selectedMSAIds = new list<string>(fullMSASet);

        for (String msaId :selectedMSAIds) {
        	msaStr += completeMSAMap.get(msaId).Abbreviation__c;
        	msaStr += ';';
        }
        if(msaStr.length()!=0){msaStr = msaStr.Substring(0,msaStr.length()-1);}
        generateMSAShortValues ();

        //Generate the set of regions that are applicable to the reports
        Set<String> reportsFromRegionIds = new Set<String>();
        for (String msaId :selectedMSAIds) {
	        for (String regionId :RegionHasMSAMap.KeySet()) {
	        	if (RegionHasMSAMap.get(regionId).contains(msaId)) {
	        		reportsFromRegionIds.add(regionId);	//Regions for reports to display
	        	}
	        }
        }

		//Generate the report list
		reportList = genReportList(reportsFromRegionIds);

        //Generate the abbreviated MSA list with regions
        generateMSAShortValues();

		return null;
	}

	/** On selection of a grouping, put the grouping selection into the MSA checkboxes **/
	public pageReference generateMSAShortValues() {
		set<string> exemptRegionIds = new set<string>();
		set<string> exemptMSAIds = new set<string>();
		set<string> msaSet = new set<string>(selectedMSAIds);
		msaShortStr='';

		//check to make sure all MSA's for the region are selected, if so then we display the region
        for (String regionId :selectedRegionIds) {
        	if (RegionHasMSAMap.containsKey(regionId)){
	        	if (msaSet.containsAll(RegionHasMSAMap.get(regionId))) {
		        	msaShortStr += completeRegionMap.get(regionId).Abbreviation__c;
		        	msaShortStr += ';';
		        	exemptRegionIds.add(regionId);
	        	}
        	}
        }

        //now loop through msa to find individuals that do not belong to a complete region
        for (string msaId :selectedMSAIds) {
        	for (string regionId :exemptRegionIds) {
        		if (RegionHasMSAMap.get(regionId).contains(msaId)) {
        			exemptMSAIds.add(msaId);
        		}
        	}
        }

        //re-loop through and compare the exempt msaids to identify msa across all regions
        for (string msaId :selectedMSAIds) {
        	if (!exemptMSAIds.contains(msaId)) {
        		msaShortStr += completeMSAMap.get(msaId).Abbreviation__c;
        		msaShortStr += ';';
        	}
        }

     	if(msaShortStr.length()!=0){msaShortStr = msaShortStr.Substring(0,msaShortStr.length()-1);}

		return null;
	}

	/** On selection of the report checkboxes, put the report name into string and render in preview table **/
	public pageReference generateReportValues() {
		reportStr='';

		//If All Reports is selected
        //Loop through the selected reports to generate delimited string
        for (String rprtId :selectedReportIds) {
	    	reportStr += completeReportMap.get(rprtId).Abbreviation__c;
	    	reportStr += ';';
        }

        if(reportStr.length()!=0){reportStr = reportStr.Substring(0,reportStr.length()-1);}

		return null;
	}

	public pageReference defaultEndDate() {
		
		if (OLI.ServiceDate!=NULL) {
			OLI.Site_End_Date__c = OLI.ServiceDate.AddMonths(12);
			OLI.Site_End_Date__c = OLI.Site_End_Date__c.AddDays(-1);
		}
		return null;
	}

	public pageReference SelectAllReports() {

		reportStr = ALL_REPORTS;

		for (Id rprtId : completeReportMap.KeySet()) {
			selectedReportIds.add(rprtId);
		}

		return null;
	}


	public pageReference SelectAllSectors() {

		selectedSectorIds.clear();
		for (Id sectorId : completeSectorMap.KeySet()) {
			selectedSectorIds.add(sectorId);
		}

		generateSectorValues(); 

		return null;
	}

	public pageReference theSaveNew() {
		//Validate that a product was selected
		if (selectedProductId==''||selectedProductId==null) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_ProdMatrixNoProduct_ErrMsg));
			return null;
		}

		saveTheRecord();

		if (OLI.UnitPrice==NULL) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Amount_Validation));
			return null;
		}
		if ((OLI.Sector__c==NULL || sectorStr=='') && isIncreaseProduct==false) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Sector_Validation));
			return null;
		}
		if ((OLI.MSA__c==NULL || msaShortStr=='') && isIncreaseProduct==false) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_MSA_Validation));
			return null;
		}
		if ((OLI.Reports__c==NULL || reportStr=='') && OLI.Analytic__c==NULL) {
			if (isIncreaseProduct==false) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Report_Validation));
				return null;
			}
		}

        try {

        	if (newOLI!=NULL) {
        		upsert newOLI;
        		delete OLI;
        		newOLI = NULL;
        	} else {
        		upsert OLI;
    		} 

        } catch (dmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
			return null;
        }

        //Redirect page back to the created SWP
        PageReference pageRef = new PageReference('/apex/Shift_ProductPicker?id='+Opp.Id);
        pageRef.setRedirect(true);
        return pageRef;
	}

	/** handles the save (upsert) of the OLI **/
	public pageReference theSave() {
		//Validate that a product was selected
		if (selectedProductId==''||selectedProductId==null) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_ProdMatrixNoProduct_ErrMsg));
			return null;
		}

		saveTheRecord();

		if (OLI.UnitPrice==NULL) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Amount_Validation));
			return null;
		}
		if ((OLI.Sector__c==NULL || sectorStr=='') && isIncreaseProduct==false) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Sector_Validation));
			return null;
		}
		if ((OLI.MSA__c==NULL || msaShortStr=='') && isIncreaseProduct==false) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_MSA_Validation));
			return null;
		}
		if ((OLI.Reports__c==NULL || reportStr=='') && OLI.Analytic__c==NULL) {
			if (isIncreaseProduct==false) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Shift_Report_Validation));
				return null;
			}
		} 

        try {

        	if (newOLI!=NULL) {
        		upsert newOLI;
        		delete OLI;
        		newOLI = NULL;
        	} else {
        		upsert OLI;
    		}

        } catch (dmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
			return null;
        }

        //Redirect page back to the created SWP
        PageReference pageRef = new PageReference('/'+Opp.Id);
        pageRef.setRedirect(true);
        return pageRef;
	}

	public void saveTheRecord() {
		Id PricebookId = Opp.pricebook2id;

		//If no pricebook for opp, we will default it to the standard pricebook
		if (PricebookId==null) {
			List<Pricebook2> templist = [SELECT Id FROM Pricebook2 WHERE IsStandard = true LIMIT 1];
			PricebookId = templist[0].Id;
		}
		//Query out the pricebook entry for the Opps pricebook and selected product
        pricebookentry pb = [Select id,unitprice from pricebookentry
                             where product2id = :selectedProductId AND Pricebook2id = :PricebookId];

		//Set the values for the Opportunity Line Item
		//If in edit mode, don't attempt to update the OppId and PricebookEntryId
        if(OppLineId == null) {
	        OLI.OpportunityId = Opp.Id;
	        OLI.PricebookEntryId = pb.Id;
	        OLI.Reports__c = reportStr;
	        OLI.Sector__c = sectorStr;
	        OLI.Full_MSA__c = msaStr;
	        OLI.MSA__c = msaShortStr;
        } else {
        	OLI.Reports__c = reportStr;
        	OLI.Sector__c = sectorStr;
        	OLI.Full_MSA__c = msaStr;
        	OLI.MSA__c = msaShortStr;
        	if (OLI.PricebookEntryId != pb.Id) {
        		newOLI = OLI.clone(false, true, true, true);
        		newOLI.PricebookEntryId = pb.Id;
        		newOLI.Quantity=1;
        		newOLI.OpportunityId = Opp.Id;
        	}
        }
	}

	/** Cancel button - return to opp **/
	public pageReference theCancel() {
		return m_controller.cancel();
	}

	/** Place OLI parameter in the URL when selected from existing table **/
    public pageReference editExistingOLI() {
      	OppLineId=null; //reset the apex param
      	init();
        return null;
    }

	/** Validate what fields and any deaults based on the Product Type - fires on change of the fields **/
	public pageReference validateProductTypeSettings() {

		//If product type requires a capped amount, set bool to render field on page
		if (OLI.Product_Type__c == CAPPED_SE) {
			isCapped = true;
		} else {
			isCapped = false;
		}

		checkDisableSelectList(); //check to see if select lists need to be disabled

		return null;
	}

	/** Method to handle search addition functionality within MSA list **/
	public pageReference theMSASearchSelect() {

		//Loop through the MSA's and if the text match add it
		for (Id msaId :completeMSAMap.KeySet()) {
			string tempName = completeMSAMap.get(msaId).Name.toUpperCase();
			string tempSearchTxt = msaSearchText.toUpperCase();
			if (tempName.contains(tempSearchTxt)) {
				selectedMSAIds.add(msaId);
			}
		}
		generateMSAValues();

		return null;
	}

	/** Method to handle search removealfunctionality within MSA list **/
	public pageReference theMSASearchDeSelect() {
		Integer i = 0;
		//Loop through the MSA's and if the text match remove it
		while (i < selectedMSAIds.size()) {
			string tempName = completeMSAMap.get(selectedMSAIds.get(i)).Name.toUpperCase();
			string tempSearchTxt = msaSearchText.toUpperCase();
			if (tempName.contains(tempSearchTxt)) {
				selectedMSAIds.remove(i);
			} else {
				i++;
			}
		}
		generateMSAValues();

		return null;
	}

	/** Return select list for all Reports **/
	public List<SelectOption> genReportList(Set<String> regionIds) {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	List<sObject> reportList = new List<sObject>();
 	 	Set<String> displayReportFromRegionIdSet = new Set<String>();
 	 	Set<String> displayReportIdSet = new Set<String>();

 	 	//Loop through report to sector and region junctions to determine whether the report is available for both
 	 	for (Supported_Region_Report__c regRep :[SELECT Reis_Report__c FROM Supported_Region_Report__c WHERE Region__c IN :regionIds]) {
 	 		displayReportFromRegionIdSet.add(regRep.Reis_Report__c);
 	 	}

 	 	if (!displayReportFromRegionIdSet.isEmpty()){
	 	 	for (Supported_Report_Sector__c secReg :[SELECT Reis_Report__c FROM Supported_Report_Sector__c WHERE Sector__c IN :selectedSectorIds]) {
	 	 		if (displayReportFromRegionIdSet.contains(secReg.Reis_Report__c)) {
	 	 			displayReportIdSet.add(secReg.Reis_Report__c);
	 	 		}
	 	 	}
 	 	} 	

 	 	for (Id rprtId :completeReportMap.KeySet()) {
 	 		if (displayReportIdSet.contains(rprtId)) {
 	 			//options.add(new SelectOption(rprtId,completeReportMap.get(rprtId).Name));
 	 			reportList.add(completeReportMap.get(rprtId));
 	 		}
 	 	}

 	 	Shift_UtilityClass.sortList(reportList, 'Sort_Order__c', 'asc');

 	 	for (sObject obj :reportList) {
 	 		options.add(new SelectOption(string.valueOf(obj.get('Id')), string.valueOf(obj.get('Name'))));
 	 	}

 	 	return options;
  	}

  	/** Return select list for all Sectors **/
	public List<SelectOption> getSectorList() {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	List<sObject> sectorList = new List<sObject>();

 	 	for (Id sctrId :completeSectorMap.KeySet()) {
 	 		sectorList.add(completeSectorMap.get(sctrId));
 	 	}

 	 	Shift_UtilityClass.sortList(sectorList, 'Sort_Order__c', 'asc');

 	 	for (sObject obj :sectorList) {
 	 		options.add(new SelectOption(string.valueOf(obj.get('Id')), string.valueOf(obj.get('Name'))));
 	 	}

 	 	return options;
  	}

    /** Return select list for Regions **/
	public List<SelectOption> genRegionList(Set<String> regionIds) {
 	 	List<sObject> regionList = new List<sObject>();
 	 	List<SelectOption> options = new List<SelectOption>();

 	 	for (Id regionID :completeRegionMap.KeySet()) {
 	 		if (regionIds.contains(regionID)) { //If the regionis avilable based on the selected sectors
 	 			//options.add(new SelectOption(regionID,completeRegionMap.get(regionID).Name));
 	 			regionList.add(completeRegionMap.get(regionID));
 	 		}
 	 	}

 	 	Shift_UtilityClass.sortList(regionList, 'Sort_Order__c', 'asc');

 	 	for (sObject obj :regionList) {
 	 		options.add(new SelectOption(string.valueOf(obj.get('Id')), string.valueOf(obj.get('Name'))));
 	 	}

 	 	return options;
  	}

    /** Return select list for MSAs **/
	public List<SelectOption> genMSAList(Set<String> regionIds) {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	Set<String> msaIds = new Set<String>();

 	 	//Loop through the junction between region and MSA to find MSA's that are available in the displayed regions
 	 	for (Supported_Region__c suppReg :[SELECT MSA__c, Region__c FROM Supported_Region__c WHERE Region__c IN :regionIds]) {
 	 		msaIds.add(suppReg.MSA__c);

			if (RegionHasMSAMap.containsKey(suppReg.Region__c)){
				RegionHasMSAMap.get(suppReg.Region__c).add(suppReg.MSA__c);
			} else {
				set<string> tempSet = new set<string>();
				tempSet.add(suppReg.MSA__c);
				RegionHasMSAMap.put(suppReg.Region__c, tempSet);
			}
 	 	}

 	 	//Loop through complete MSA map and if the MSA is available for any of the regions add it to the list
 	 	for (Id msaId :completeMSAMap.KeySet()) {
 	 		if (msaIds.contains(msaId)) {
 	 			options.add(new SelectOption(msaId,completeMSAMap.get(msaId).Name));
 	 		}
 	 	}

 	 	options = Shift_UtilityClass.SortOptionList(options);
 	 	return options;
  	}

    /** Return select list for products **/
	public List<SelectOption> getProductList() {
 	 	List<SelectOption> bOptions = new List<SelectOption>();
 	 	bOptions.add(new SelectOption('','--None--'));

 	 	List<SelectOption> options = new List<SelectOption>();
 	 	for (Id prodId :productMap.KeySet()) {
 	 		options.add(new SelectOption(prodId,productMap.get(prodId).Name));
 	 	}
 	 	options = Shift_UtilityClass.SortOptionList(options);
 	 	bOptions.addAll(options);
 	 	return bOptions;
  	}

}