Global class UpdateSurveyDataNeededBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
    String sQuery,Sector,QueueConfigId;
    List<Survey_Log__c> lSurveyLogList;
    
    public UpdateSurveyDataNeededBatch(String sQuery,String Sector,String QueueConfigId){
        this.sQuery=sQuery;
        this.Sector=Sector;
        this.QueueConfigId=QueueConfigId;
        lSurveyLogList=new List<Survey_Log__c>();
    }
    
    global Database.querylocator start(Database.BatchableContext BC){
        system.debug('sQuery=>'+sQuery);
        return Database.getQueryLocator(sQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            List<Property_Survey__c> PropertySurveyList=new List<Property_Survey__c>();
            for(sObject ScopeRecord: Scope){
                Queue__c QueueRec=(Queue__c)ScopeRecord;
                for(Property_Survey__c PropSurvey: QueueRec.Property_Surveys__r){
                    if(PropSurvey.Status__c=='Pending Creation'){
                        PropSurvey.Survey_Data_Needed__c=true;
                        PropertySurveyList.add(PropSurvey);    
                    }
                }
            }
            update PropertySurveyList;
        }catch(Exception e){
            Survey_Log__c oLog=new Survey_Log__c();
            oLog.Error_Message__c=e.getMessage() + string.valueof(e.getLineNumber()) + e.getStackTraceString();
            oLog.Batch_Id__c=BC.getJobId();
            oLog.Batch_Name__c='UpdateSurveyDataNeededBatch';
            oLog.Status__c='Error';
            oLog.Sector__c=Sector;
            oLog.Additional_Information__c='Error While updating the Survey Data Needed Flag. Query is =>' +sQuery;
            lSurveyLogList.add(oLog);
        }   
    }
    
    global void finish(Database.BatchableContext BC){
        if(lSurveyLogList.size()>0){
            insert lSurveyLogList;
        }
        Queue_Configuration__c QueueConfig1=new Queue_Configuration__c();
        QueueConfig1.BatchJobId__c=null;
        QueueConfig1.Id=QueueConfigId;
        update QueueConfig1;
    }
}