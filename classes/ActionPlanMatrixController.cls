public class ActionPlanMatrixController {
	public string JSActionPlanDetails {get; set;}
    public string JSXFuncActionPlanDetails {get; set;}
    public string planID {get; set;}
    
    public ActionPlanMatrixController(ApexPages.StandardController ctrl) {
    	planID = (string)ctrl.getRecord().get('id');
        JSActionPlanDetails = Json.serialize(getPlanDetails(planID, getRecordTypeId('Action Plan Detail - Sales')));
        JSXFuncActionPlanDetails = Json.serialize(getPlanDetails(planID, getRecordTypeId('Action Plan Detail - XFunctional')));
    }

    private List<Action_Plan_Detail__c> getPlanDetails(ID planID, ID RecordTypeId) {
        return ([Select Id, Date__c, Role__c, Owner_Name__c, Notes__c from Action_Plan_Detail__c where RecordTypeId =: RecordTypeId  AND Action_Plan_Sales__c =: planID ORDER BY Date__c Desc]);
    }
    private string getRecordTypeId(string TypeName) {
        RecordType rec = [Select Id from RecordType where SObjectType = 'Action_Plan_Detail__c' and Name =: TypeName  LIMIT 1];
        return rec.Id;
    }
}