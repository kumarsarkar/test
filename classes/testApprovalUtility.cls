@isTest
private class testApprovalUtility {
//Added by Chris Boulden, 9-13-17. Tests ApprovalUtiltiy
 
  
    
    
    @testSetup static void dataSetup(){
        //Declare data needed for tests
        UserRole newBossofBossRole = new UserRole(name ='testBossofBossRole');
		insert newBossofBossRole;        
        UserRole newBossRole = new UserRole(name ='testBossRole');
        insert newBossRole;		        
        UserRole newApproverRole = new UserRole(name ='testApproverRole' , parentRoleId = newBossRole.Id);
        Id ProfileId = [SELECT Id FROM Profile WHERE PermissionsModifyAllData = false Limit 1].Id;
        Id AdminProfileId = [SELECT Id FROM Profile WHERE PermissionsModifyAllData = true Limit 1].Id;
        insert newApproverRole;
        User newBossUser      = new User(firstName = 'test', LastName = 'tester', UserRoleId = newbossRole.Id, Username = 'MrTester@test.com', Email='MrTester@test.com', Alias = 'hdfsjkgh', CommunityNickname = 'dkjfhgkjds', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', ProfileId = ProfileId, LanguageLocaleKey = 'en_US');
        User newBossofBossUser= new User(firstName = 'test', LastName = 'tester', UserRoleId = newbossRole.Id, Username = 'MrTester4@test.com', Email='MrTester4@test.com', Alias = 'hh', CommunityNickname = 'dks', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', ProfileId = ProfileId, LanguageLocaleKey = 'en_US');
        User newApproverUser  = new User(firstName = 'test', LastName = 'tester', UserRoleId = newApproverRole.Id, Username = 'MrTester2@test.com', Email='MrTester2@test.com', Alias = 'hjkgh', CommunityNickname = 'dkjfhgdgfdkjds', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', ProfileId =  ProfileId, LanguageLocaleKey = 'en_US');
        User newApproverUser2 = new User(firstName = 'test', LastName = 'tester', UserRoleId = newApproverRole.Id, Username = 'MrTester3@test.com', Email='MrTester3@test.com', Alias = 'hj9gh', CommunityNickname = 'dkjf9fdkjds', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', ProfileId =  ProfileId, LanguageLocaleKey = 'en_US');        
        User newAdminUser     = new User(firstName = 'test', LastName = 'tester', Username = 'MrTester5@test.com', Email='MrTester3@test.com', Alias = 'hj9gryuh', CommunityNickname = 'dkjjds', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', ProfileId =  AdminProfileId, LanguageLocaleKey = 'en_US'); 
        User[] userList = new User[]{newBossUser,newApproverUser,newApproverUser2,newBossofBossUser,newAdminUser};
        insert userList;
    }    
    
    @isTest
    static void testGetApproverList(){
        
        Property_Survey__c ps = new Property_Survey__c(Status__c = 'In Progress', Survey_Source__c = 'Web');
        insert ps;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
		app.setObjectId(ps.Id);
		Approval.process(app);
		Id targObjId = [Select Id, TargetObjectId FROM ProcessInstance].TargetObjectId;
        System.assertEquals(targObjId,ps.ID);
        List<ID> testApproverList = new List<Id>(ApprovalUtility.getApproverList(ps.Id));
        
    }
    
    @isTest
    static void testIsApprover(){
        
        Property_Survey__c ps = new Property_Survey__c(Status__c = 'In Progress', Survey_Source__c = 'Web');
        insert ps;
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
		app.setObjectId(ps.Id);
		Approval.process(app);
		Id targObjId = [Select Id, TargetObjectId FROM ProcessInstance].TargetObjectId;
        System.assertEquals(targObjId,ps.ID);
        List<ID> testApproverList = new List<Id>(ApprovalUtility.getApproverList(targObjId));
        if(testApproverList.size() > 0){        
        	system.Assert(ApprovalUtility.isApprover(targObjId,testApproverList[0]));
        }
        User newAdminUser      = [Select Id FROM User WHERE Username = 'MrTester5@test.com' ];
        system.assert(ApprovalUtility.isApprover(targObjId,newAdminUser.Id));
    }
    
    @isTest
    static void testIsManager(){
        
        User newApproverUser   = [Select Id FROM User WHERE Username = 'MrTester2@test.com' ];
        User newApproverUser2  = [Select Id FROM User WHERE Username = 'MrTester3@test.com' ];
        User newBossUser       = [Select Id FROM User WHERE Username = 'MrTester@test.com'  ];
        User newBossofBossUser = [Select Id FROM User WHERE Username = 'MrTester4@test.com' ];
        
		System.assert(ApprovalUtility.isManager(newBossUser.Id,newApproverUser.Id));
		System.assert(ApprovalUtility.isManager(newBossofBossUser.Id,newApproverUser.Id));
		System.assert(!ApprovalUtility.isManager(newApproverUser.Id,newBossUser.Id));
        
    }
    
    @isTest
    static void testIsManagerList(){
        /*
        String[] usernameList     = new String[]{'MrTester2@test.com','MrTester3@test.com','MrTester@test.com','MrTester4@test.com'};
        List<User> userTempList   = new List<User>([Select Id, Username FROM User WHERE Username IN :usernameList ]);
        Map<String, User> userMap = new Map<String, User>();
        for(User u : userTempList){
            userMap.put(u.Username,u);
        }
        User newApproverUser   = userMap.get('MrTester2@test.com');
        User newApproverUser2  = userMap.get('MrTester3@test.com');
        User newBossUser       = userMap.get('MrTester@test.com');
        User newBossofBossUser = userMap.get('MrTester4@test.com');
		*/
        
        User newApproverUser   = [Select Id FROM User WHERE Username = 'MrTester2@test.com' ];
        User newApproverUser2  = [Select Id FROM User WHERE Username = 'MrTester3@test.com' ];
        User newBossUser       = [Select Id FROM User WHERE Username = 'MrTester@test.com'  ];
        User newBossofBossUser = [Select Id FROM User WHERE Username = 'MrTester4@test.com' ];
        
        List<Id> userList     = new Id[]{newApproverUser2.Id,newApproverUser.Id};
        List<Id> userBossList = new Id[]{newBossUser.Id};
		System.assert(!ApprovalUtility.isManager(newApproverUser.Id,userBossList));
		System.assert(ApprovalUtility.isManager(newBossUser.Id,userList));
		System.assert(ApprovalUtility.isManager(newBossofBossUser.Id,userList));
    }
    
}