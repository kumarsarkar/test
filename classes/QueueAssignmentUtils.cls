public class QueueAssignmentUtils {
    
    public static void AssignQueue(String SurveyorUserId,List<Queue__c> QueueList,List<Queue_configuration__c> QueueConfigList, List<Property_Survey__c> PropSurveyList){
        Queue__c queueObj = new Queue__c();
        List<String> SectorAssignList = new List<String>();
        List<String> SectorNotAssignList = new List<String>();
        List<String> MSAAssignList = new List<String>();
        List<String> MSANotAssignList = new List<String>();
        String query = '';
        String FieldQuery = '';
        String FromPart = '';
         Integer MaxOnHold;
        
        Surveyor__c surveyor = [Select Id,CCRC__c,User__c,Client_Services__c,Do_Not_Call__c,IAG__c,Multiples__c,New_Construction__c,Non_Published_MSAs__c,QA__c,Max_Surveys_Owned__c,(Select Id,Surveyor__c,Survey_Sector__c,Survey_Sector__r.Name,Cannot_Survey__c,Name,Can_Only_Survey__c from Sector_Skills__r),(Select Id,Name, Survey_MSA__c,Can_Only_Survey__c,Cannot_Survey__c,Surveyor__c from MSA_Skills__r) from Surveyor__c where Id =: SurveyorUserId];
        
        List<AggregateResult> AssignedQueueCountList=[select count(id) counter,status__c from queue__c where ownerid=:surveyor.User__c and status__c in ('In Progress','On Hold') group by status__c order by Status__c]; 
        system.debug('valueof'+AssignedQueueCountList);
         MaxOnHold=Surveyor.Max_Surveys_Owned__c==null?5:Integer.valueOf(Surveyor.Max_Surveys_Owned__c);
        if(AssignedQueueCountList.size()>0){
            for(Integer i=0;i<AssignedQueueCountList.size();i++){
                if(AssignedQueueCountList[i].get('status__c')=='In Progress'){
                    if(Integer.valueOf(AssignedQueueCountList[i].get('counter'))>=1){
                        return;
                    }
                }else if(AssignedQueueCountList[i].get('status__c')=='On Hold'){
                    //Anantha 21/08 Updated to check the max on hold surveys allowed RSI-482
                  //  MaxOnHold=Surveyor.Max_Surveys_Owned__c==null?5:Integer.valueOf(Surveyor.Max_Surveys_Owned__c);
                    if(Integer.valueOf(AssignedQueueCountList[i].get('counter'))>=MaxOnHold){
                        return;
                    }
                }
            }
        }
        
        system.debug('****surveyor '+surveyor);
        
        boolean ccrc = surveyor.CCRC__c;
        boolean iag = surveyor.IAG__c;
        boolean donotcall = surveyor.Do_Not_Call__c;
        String Status = 'Pending Assignment';
        
        List<Sector_Skills__c> listSkillset = surveyor.Sector_Skills__r;
        //  List<Sector_Skills__c> listSkillset = new List<Sector_Skills__c>();
        //   listSkillset = [Select Id,Surveyor__c,Survey_Sector__c,Survey_Sector__r.Name,Cannot_Survey__c,Name,Can_Only_Survey__c from Sector_Skills__c where Surveyor__c =: SurveyorUserId];
        system.debug('****listSkillset '+listSkillset );
        
        List<MSA_Skills__c> MSASkillsetList=surveyor.MSA_Skills__r;
        //    List<MSA_Skills__c> MSASkillsetList = new List<MSA_Skills__c>();
        //  MSASkillsetList = [Select Id,Name, Survey_MSA__c,Can_Only_Survey__c,Cannot_Survey__c,Surveyor__c from MSA_Skills__c where Surveyor__c =: SurveyorUserId];
        system.debug('****MSASkillsetList '+MSASkillsetList );
        
        List<Queue__c> listQueue =  new List<Queue__c> ();
        FieldQuery='Select Id,Aged_Priority__c, Queue_Assigned__c,Queue_Configuration__c,Status__c,CCRC__c,Survey_Sector__c,Survey_Sector__r.Name,Do_Not_Call__c,IAG__c,Client_Services__c,Multiples__c,New_Construction__c,Non_Published_MSAs__c,QA__c,Time_Zone__c,Can_Be_Assigned__c,AM_PM_Check__c,Priority__c,(Select Id,status__c from property_surveys__r) ';
        FromPart='from Queue__c';
        query = ' where  Status__c =\''+status+'\' and Can_Be_Assigned__c = true';
        //  		query = ' where  Status__c =\''+status+'\'';        
        
        //Sector LogicalHandler
        if(listSkillset.size()>0 ){
            for (Sector_Skills__c secSkill : listSkillset ){
                if (secSkill.Can_Only_Survey__c == true)
                {
                    SectorAssignList.add('\''+secSkill.Survey_Sector__r.Name+'\'');
                }
                else{
                    SectorNotAssignList.add('\''+secSkill.Survey_Sector__r.Name+'\'');
                }
                
            }
        }
        
        if(SectorAssignList.size() > 0)
            query = query + ' and Survey_Sector__r.Name  in ' + SectorAssignList;
        
        if (SectorNotAssignList.size() > 0)
            query = query + ' and Survey_Sector__r.Name not in ' + SectorNotAssignList;
        
        if(MSASkillsetList.size()>0){
            for(MSA_Skills__c MSASkill: MSASkillsetList){
                if(MSASkill.Can_Only_Survey__c==true){
                    MSAAssignList.add('\''+MSASkill.Name+'\'');
                }else{
                    MSANotAssignList.add('\''+MSASkill.Name+'\'');
                }
            }
            if(MSAAssignList.size()>0){
                query =FieldQuery+FromPart+query +
                    ' AND Id in (select queue_record__c from property_Survey__c where Survey_MSA__r.Name in '+MSAAssignList+') ' ;
                
                system.debug('****query1'+query);
            }else{
                query =FieldQuery+ FromPart+query +
                    ' AND Id not in (select queue_record__c from property_Survey__c where Survey_MSA__r.Name in '+MSANotAssignList+') ' ;
                system.debug('****query2'+query);
            }
        }else{
            query=FieldQuery+FromPart+query;
        }
        
        if(surveyor.Multiples__c != true){
            query = query + ' AND No_Of_Surveys__c= 1';
            
        }
        
        if (surveyor.CCRC__c == true)
        {
            
            query = query + ' and (CCRC__c = true ' ;      
        }
        if(surveyor.Do_Not_Call__c == true ){
            if(surveyor.CCRC__c == true){
                query = query + ' OR Do_Not_Call__c=true ' ;
            }else{
                query = query + ' and (Do_Not_Call__c = true ' ;
            }
            
        }
        
        if(surveyor.IAG__c == true){
            if(surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true){
                query = query + ' OR IAG__c =true ';
            }else{
                query = query + ' and (IAG__c = true ' ;
            }
        }
        
        if(surveyor.Client_Services__c == true){
            if(surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true || surveyor.IAG__c == true){
                query = query + ' OR Client_Services__c =true ';
            }else{
                query = query + ' and (Client_Services__c = true ' ;
            }
        }
        
        if(surveyor.New_Construction__c == true){
            if(surveyor.Client_Services__c == true || surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true || surveyor.IAG__c == true){
                query = query + ' OR New_Construction__c =true ';
            }else{
                query = query + ' and (New_Construction__c = true ' ;
            }
        }
        
        if(surveyor.Non_Published_MSAs__c == true){
            if(surveyor.New_Construction__c == true || surveyor.Client_Services__c == true || surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true || surveyor.IAG__c == true){
                query = query + ' OR Non_Published_MSAs__c =true ';
            }else{
                query = query + ' and (Non_Published_MSAs__c = true ' ;
            }
        }
        
        if(surveyor.QA__c == true){
            if(surveyor.Non_Published_MSAs__c == true || surveyor.New_Construction__c == true || surveyor.Client_Services__c == true || surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true || surveyor.IAG__c == true){
                query = query + ' OR QA__c =true ';
            }else{
                query = query + ' and (QA__c = true' ;
            }
        }
        system.debug('surveyor'+surveyor);
        if(surveyor.Non_Published_MSAs__c == true || surveyor.New_Construction__c == true || surveyor.QA__c == true || surveyor.Client_Services__c == true || surveyor.CCRC__c == true || surveyor.Do_Not_Call__c == true || surveyor.IAG__c == true){
       // 	query = query + ') ORDER BY Priority__c,Aged_Priority__c LIMIT 1' ; 
       		query = query + ') ORDER BY Priority__c,Aged_Priority__c LIMIT '+String.valueOf(MaxOnHold) ;
        }else{
       //     query = query + ' ORDER BY Priority__c,Aged_Priority__c LIMIT 1' ;
              query = query + ' ORDER BY Priority__c,Aged_Priority__c LIMIT '+String.valueOf(MaxOnHold) ;     
        }
        system.debug('****queryfinal'+query);
        system.debug(LoggingLevel.Info,'****queryfinal'+query);
        Map<Id,Queue__c> QueueMap=new Map<id,Queue__c>();
        listQueue = Database.query(query);
        system.debug('****listQueue '+listQueue );
        for(Queue__c QRec:listQueue){
           QueueMap.put(QRec.Id, QRec);
        }
        Queue__c QueueToAssign;
        for(Queue__c QRec:[Select Id,Priority__c,Aged_Priority__c from Queue__c Where status__c='Pending Assignment' and Id=:QueueMap.keySet() for update]){
           
            if(QueueToAssign==null){
                QueueToAssign=QRec;
            }else{
                if(QueueToAssign.Priority__c>QRec.Priority__c){
                    QueueToAssign=QRec;
                }else if(QueueToAssign.Priority__c==QRec.Priority__c){
                    if(QueueToAssign.Aged_Priority__c>QRec.Aged_Priority__c){
                        QueueToAssign=QRec;
                    }
                }
            }   
             System.debug('QueueToAssign=>'+QueueToAssign.Id+' Qrec=>'+QRec.Id);
        }
        
      //  if(listQueue.size() >0){
        if(QueueToAssign!=null){  
         //   queueObj = listQueue[0];
            queueObj = QueueMap.get(QueueToAssign.Id);  
            queueObj.OwnerId =  surveyor.User__c;
            queueObj.Status__c = 'In Progress';
            
            for(Property_Survey__c PropSurvey:queueObj.Property_Surveys__r){
                PropSurvey.Status__c='In Progress';
                PropSurveyList.add(PropSurvey);
            }
            
            
            try {
                
                // update queueObj;
                QueueList.add(queueObj);
                // List<Queue_Configuration__c> QueueConfigList=[select Id,Queue_Assigned__c from Queue_Configuration__c where Id=:queueObj.Queue_Configuration__c LIMIT 1];
                Queue_Configuration__c QueueConfig =new Queue_Configuration__c(Id=queueObj.Queue_Configuration__c);
                System.debug('Aged_Priority__c=>'+queueObj.Aged_Priority__c);
                System.debug('QueueConfig=>'+QueueConfig);
                System.debug('queueObj=>'+queueObj);
                if(queueObj.Aged_Priority__c==1){
                    QueueConfig.Queue_Assigned__c=0;
                }else{
                    QueueConfig.Queue_Assigned__c=queueObj.Queue_Assigned__c+1;
                }
                //update QueueConfig;
                QueueConfigList.add(QueueConfig);
                
            }
            catch (Exception e)
            {
                system.debug('exception=>'+e);
                throw(e);
            }
        }
        
    }
}