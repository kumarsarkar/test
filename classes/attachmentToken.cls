global class attachmentToken {
    
    Webservice static string getToken() {
        Blob key = Blob.valueOf('6179B71481E79186');
        Blob Iv = Blob.valueOf('AF7727F6381D48B4');
        
        Datetime dt = Datetime.now();
        Long l = dt.getTime();
        String strDate = l.format().remove(',');
        strDate = strDate.substring(0, strDate.length() -3);
        Blob ep = Blob.valueOf(strDate);
        
	    Blob cipherText = Crypto.encrypt('AES128', key, Iv, ep);
	   	String encodedCipherText = EncodingUtil.base64Encode(cipherText);

		//return dt.format() + '-' + l.format() + '-' + encodedCipherText;
        return encodedCipherText;
    }     

}