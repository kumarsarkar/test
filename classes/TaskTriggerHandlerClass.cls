public class TaskTriggerHandlerClass {
    public static void handleAfterInsert(Map<Id,Task> NewMap){
        System.Debug('<<<handleAfterInsert Method Called>>>'+NewMap);
        taskAssociationToPropertySurvey(NewMap);
    }
    public static void taskAssociationToPropertySurvey(Map<Id,Task> NewMap){
        System.Debug('<<<<taskAssociationToPropertySurvey>>>>');
        Map<Id,List<Task>> surveyContactTaskMap = new Map<Id,List<Task>>();
        Map<Id,Survey_Contact__c> surveyQueueContactMap = new Map<Id,Survey_Contact__c>();
        List<Call_Metrics__c> insertCallMetricsList;
        List<Call_Metrics_Surveys__c> insertCallMetricsSurveyList;
        String survey_Contact_prefix = Schema.SObjectType.Survey_Contact__c.getKeyPrefix();
        insertCallMetricsList = new List<Call_Metrics__c>();
        for(Task tsk : NewMap.Values()){
            System.Debug('<<<<tsk>>>>' + tsk);
            System.Debug('<<<<tsk.Whatid>>>>' + tsk.Whatid);
            if(tsk.Whatid != null){
                if(string.valueof(tsk.whatid).startsWith(survey_Contact_prefix)){
                    Call_Metrics__c callMetrics = new Call_Metrics__c();
                    callMetrics.Contact_Id__c = tsk.whatid;
                    callMetrics.ActivityDate_Time__c = tsk.ActivityDate;
                    callMetrics.Call_Duration_In_Seconds__c = tsk.CallDurationInSeconds;
                    callMetrics.Phone__c = tsk.Who.Phone;
                    insertCallMetricsList.add(callMetrics);
                    if(surveyContactTaskMap.containsKey(tsk.Whatid)) {
                        List<Task> tskList = surveyContactTaskMap.get(tsk.Whatid);
                        tskList.add(tsk);
                        surveyContactTaskMap.put(tsk.Whatid, tskList);
                    } else {
                        surveyContactTaskMap.put(tsk.Whatid,new List<Task>{tsk});
                    }
                }
            }
        }
        if(insertCallMetricsList!=null && insertCallMetricsList.size()>0){
            insert insertCallMetricsList;
        }
        List<Survey_Contact__c> surveyContactList = new List<Survey_Contact__c>([Select Id, Property_Survey__r.Queue_Record__c FROM Survey_Contact__c Where Id in : surveyContactTaskMap.keySet()]);
        for(Survey_Contact__c sc : surveyContactList){
            surveyQueueContactMap.put(sc.Property_Survey__r.Queue_Record__c,sc);
        }
        System.Debug('<<<<surveyQueueContactMap>>>>' + surveyQueueContactMap);
        List<Property_Survey__c> pSurvey = new List<Property_Survey__c>([SELECT Id, Queue_Record__c, Call_Status__c,Property_Id__c,PropertyId__c,Name,Status__c,Survey_Completed__c,Survey_MSA__c,Survey_Sector__c,Survey_Submarket__c  from Property_Survey__c where  Queue_Record__c in : surveyQueueContactMap.KeySet()]);
        Map<Id,List<Property_Survey__c>> queuePropertySurveyMap = new Map<Id,List<Property_Survey__c>>();
        for(Property_Survey__c psur : pSurvey){
            if(queuePropertySurveyMap.containsKey(psur.Queue_Record__c)) {
                List<Property_Survey__c> psurList = queuePropertySurveyMap.get(psur.Queue_Record__c);
                psurList.add(psur);
                queuePropertySurveyMap.put(psur.Queue_Record__c, psurList);
            } else {
                queuePropertySurveyMap.put(psur.Queue_Record__c,new List<Property_Survey__c>{psur});
            }
        }
        System.Debug('<<<<pSurvey>>>>' + pSurvey);
        List<Task> taskToInsert = new List<Task>();
        insertCallMetricsSurveyList = new List<Call_Metrics_Surveys__c>();
        for(Property_Survey__c psur : pSurvey){
            if(surveyQueueContactMap != null && surveyQueueContactMap.containsKey(psur.Queue_Record__c)){
                System.Debug('<<<<surveyQueueContactMap.get(psur.Queue_Record__c).Id>>>>' + surveyQueueContactMap.get(psur.Queue_Record__c).Id);
                if(surveyContactTaskMap.ContainsKey(surveyQueueContactMap.get(psur.Queue_Record__c).Id)){
                    System.Debug('<<<<pSurveyEnter>>>>' + psur);
                    for(Task tsk : surveyContactTaskMap.get(surveyQueueContactMap.get(psur.Queue_Record__c).Id)){
                        for(Call_Metrics__c cmetric : insertCallMetricsList){
                            for(String contactId : surveyContactTaskMap.keySet()){
                                if(cmetric.Contact_Id__c==contactId){
                                    Call_Metrics_Surveys__c callMetricSurvey = new Call_Metrics_Surveys__c();
                                    callMetricSurvey.Call_Metrics__c = cmetric.Id;
                                    callMetricSurvey.Call_Status__c = psur.Call_Status__c;
                                    
                                    callMetricSurvey.Property_Id__c  = psur.PropertyId__c;
                                    callMetricSurvey.Property_OID__c = psur.Property_Id__c;
                                    callMetricSurvey.Property_Survey_ID__c = psur.Name;
                                    callMetricSurvey.Status__c = psur.Status__c;
                                    callMetricSurvey.Survey_Completed__c = psur.Survey_Completed__c;
                                    callMetricSurvey.Survey_MSA__c = psur.Survey_MSA__c;
                                    callMetricSurvey.Survey_Sector__c = psur.Survey_Sector__c;
                                    callMetricSurvey.Survey_Submarket__c = psur.Survey_Submarket__c;
                                    
                                    insertCallMetricsSurveyList.add(callMetricSurvey);
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        if(insertCallMetricsSurveyList!=null && insertCallMetricsSurveyList.size()>0){
            insert insertCallMetricsSurveyList;
        }
    }
}