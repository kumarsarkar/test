/*
	@Name: Shift_MeaningfulActivity_Handler
	@Description: On completion of an activity, if the meaningful touched boolean is true then pass the touched date and activity owner up to the account
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

public with sharing class Shift_MeaningfulActivity_Handler {
	
	public static Map<String, Shift_MeaningfulActivity_ProfileExcl__c> profileExclusingSettings = Shift_MeaningfulActivity_ProfileExcl__c.getAll();
	
	/*** Methods for handling events **/
	public static void processEvents (List<Event> eventList) {
		List<Event> meaningfulEvents = new List<Event>();
		Set<Id> accountIds = new Set<Id>();
		Set<Id> assignToIds = new Set<Id>();
		
		//Loop through tasks and if they are complete and marked as meaningul add to new list
		for (Event event :eventList){
			if (event.Meaningful_Activity__c) {
				meaningfulEvents.add(event);
				accountIds.add(event.AccountId);
				assignToIds.add(event.ownerId);
			}	
		}
		
		updateAccountInfoEvent(meaningfulEvents, accountIds, assignToIds);	
	
	}
	
	public static void updateAccountInfoEvent (List<Event> eventList, Set<Id> accountIds, set<Id> assignToIds) {
		Map<Id, Id> userProfileMap = new Map<Id, Id>();
		Map<Id, Account> accountMap = new Map<Id, Account>();		
		accountMap = new Map<Id, Account>([SELECT Id, Last_Touch_Date__c, Last_Touch_By__c FROM Account WHERE Id IN :accountIds]); 
		
		for (user u :[SELECT Id, ProfileId FROM USER WHERE Id IN :assignToIds]) {
			userProfileMap.put(u.Id, u.ProfileId);
		}

		for (Event event :eventList) {
			if (!profileExclusingSettings.containsKey(userProfileMap.get(event.ownerId))) {
				if (event.AccountId != null && accountMap.containsKey(event.AccountId)) {			
					if ((accountMap.get(event.AccountId).Last_Touch_Date__c <= event.ActivityDate) || accountMap.get(event.AccountId).Last_Touch_Date__c == NULL) {
						accountMap.get(event.AccountId).Last_Touch_Date__c = event.ActivityDate;
						accountMap.get(event.AccountId).Last_Touch_By__c = event.ownerId;
					}
				}
			}	
		}
		
		system.debug('accountmap'+accountMap);

		try {
			update accountMap.values();
		} catch (dmlException e) {
			system.debug('Error: '+e);
		}
	}	
	
	/*** Methods for handling tasks **/
	public static void processTasks (List<Task> taskList){ 
		List<Task> meaningfulTasks = new List<Task>();
		List<Task> allTasks = new List<Task>();
		Set<Id> taskIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		Set<Id> assignToIds = new Set<Id>();
		
		//Loop through tasks and if they are complete and marked as meaningul add to new list
		for (Task task :taskList){
			allTasks.add(task);
			if (task.IsClosed && task.Meaningful_Activity__c) {
				meaningfulTasks.add(task);
				accountIds.add(task.AccountId);
				assignToIds.add(task.ownerId);
			}	
		}
		
		updateAccountInfoTask(meaningfulTasks, accountIds, assignToIds);
		//updateCustomTaskFields(allTasks);
		
	}
	
	public static void updateCustomTaskFields (List<Task> allTasks) {
		//Map<Id, Contact> contactMap = New Map<Id, Contact>();
		//Map<Id, Account> accountMap = New Map<Id, Account>();
		//Set<Id> contactIds = New Set<id>();
		//List<Task> allTasks = [Select Id, WhoId, WhatId, Contact_Phone__c, Contact_Email__c, Timezone__c, Account_Name__c, Account_Phone__c, Billing_State__c From Task Where Id In :allTaskIds];
		
		For (Task tsk :allTasks){
			If (tsk.WhoId <> Null) {
				Contact tskCnt = [SELECT Id, Phone, Email, Timezone__c FROM Contact WHERE Id = :tsk.WhoId];
				tsk.Contact_Phone__c = tskCnt.Phone;
				tsk.Contact_Email__c = tskCnt.Email;
				tsk.Timezone__c = tskCnt.Timezone__c;
				//contactMap.Add(tsk.Id, tskCnt);
			}
			If (tsk.WhatId <> Null) {
				If (String.ValueOf(tsk.WhatId).Substring(0, 3) == '001') {
					// This is attached to an account
					Account tskAct = [Select Name, Phone, BillingState From Account Where Id = :tsk.WhatId];
					tsk.Account_Name__c = tskAct.Name;
					tsk.Account_Phone__c = tskAct.Phone;
					tsk.Billing_State__c = tskAct.BillingState;
					//accountMap.Add(tsk.Id, tskAct);
				}
			}
			//Update tsk;
		}		
	}
	
	public static void updateAccountInfoTask (List<Task> taskList, Set<Id> accountIds, set<Id> assignToIds) {
		Map<Id, Id> userProfileMap = new Map<Id, Id>();
		//Query out accounts related to the activities
		Map<Id, Account> accountMap = new Map<Id, Account>();		
		accountMap = new Map<Id, Account>([SELECT Id, Last_Touch_Date__c, Last_Touch_By__c FROM Account WHERE Id IN :accountIds]); 
		
		for (user u :[SELECT Id, ProfileId FROM USER WHERE Id IN :assignToIds]) {
			userProfileMap.put(u.Id, u.ProfileId);
		}		
		
		//If new task touched date is more recent that the last touched date on the account, push the date and owner up to the account details
		for (Task task :taskList) {
			if (!profileExclusingSettings.containsKey(userProfileMap.get(task.ownerId))) {
				if (task.AccountId != null && accountMap.containsKey(task.AccountId)) {
					if ((accountMap.get(task.AccountId).Last_Touch_Date__c <= task.ActivityDate) || accountMap.get(task.AccountId).Last_Touch_Date__c == NULL) {
						accountMap.get(task.AccountId).Last_Touch_Date__c = task.ActivityDate;
						accountMap.get(task.AccountId).Last_Touch_By__c = task.ownerId;
					}
				}
			}	
		}
				
		try {
			update accountMap.values();
		} catch (dmlException e) {
			system.debug('Error: '+e);
		}
	}

}