@istest
public class BuildQueueUniqueContactTestClass {
    
@testsetup 
public static void BuldQueueSetup(){
    //Insert Sector Apt and Odc
    
    List<Survey_Sector__C> SurveySectorsList=new List<Survey_Sector__C>();
    Survey_Sector__C AptSurveySector=new Survey_Sector__C(Name='Apt',Description__c='Apartmesnts');
    Survey_Sector__C offSurveySector=new Survey_Sector__C(Name='off',Description__c='Office');
    
    SurveySectorsList.add(AptSurveySector);
    SurveySectorsList.add(offSurveySector);
    insert SurveySectorsList;
    system.debug('SurveySectorsList' +SurveySectorsList);
    
    //Create MSA 
    
    List<Survey_MSA__C> MSAList=new List<Survey_MSA__C>();
    
    For(Integer i=1;i<=3;i++){
        Survey_MSA__C MSA=new Survey_MSA__C(Name='APTMSA'+i,Sector__c='Apt',MSA_Status__c='Published',Foundation_Id__c='APTMSA'+i);
        MSAList.add(MSA);
    }
    
    For(Integer i=1;i<=3;i++){
        Survey_MSA__C MSA=new Survey_MSA__C(Name='offMSA'+i,Sector__c='off',MSA_Status__c='Published',Foundation_Id__c='offMSA'+i);
        MSAList.add(MSA);
    }
    
    insert MSAList;
    system.debug('MSAList' +MSAList);
    
    List<Survey_Submarket__c> SubmarketList=new List<Survey_Submarket__c>();
    for(Survey_MSA__C MSA:MSAList){
        for(integer i=1;i<=2;i++){
            Survey_Submarket__c Submarket=new Survey_Submarket__c(Name=MSA.Name+i,foundation_id__c=MSA.Name+i,MSA__C=MSA.Id,Sector__c=MSA.Sector__c);
            SubmarketList.add(Submarket);
        }
    }
    insert SubmarketList;
    system.debug('SubmarketList' +SubmarketList);
    
    List<Queue_Configuration__c> QueueConfigurationList=new List<Queue_Configuration__c>();
    Queue_Configuration__c AptQueueConfiguration=new Queue_Configuration__c(Survey_Records_With_Data__c=100,Aged_Property_Frequency__c=50,
                                                                        Aged_Timeframe__c=24,Month_1_of_Surveys_per_Submarket__c=1,
                                                                        Month_2_of_Surveys_per_Submarket__c=1,
                                                                        Month_1_Market_Coverage__c=10,
                                                                        Month_2_Market_Coverage__c=20,
                                                                        Quarter_Market_Coverage__c=50,Quarter_SubMarket_Coverage__c=50,
                                                                        Month_1_Submarket_Coverage__c=10,Month_2_Submarket_Coverage__c=20,
                                                                        Sector__c='Apt');
                                                                        
    Queue_Configuration__c offQueueConfiguration=new Queue_Configuration__c(Survey_Records_With_Data__c=100,Aged_Property_Frequency__c=50,
                                                                        Aged_Timeframe__c=24,Month_1_of_Surveys_per_Submarket__c=1,
                                                                        Month_2_of_Surveys_per_Submarket__c=1,
                                                                        Month_1_Market_Coverage__c=10,
                                                                        Month_2_Market_Coverage__c=20,
                                                                        Quarter_Market_Coverage__c=70,Quarter_SubMarket_Coverage__c=50,
                                                                        Month_1_Submarket_Coverage__c=10,Month_2_Submarket_Coverage__c=20,
                                                                        Sector__c='off');
    QueueConfigurationList.add(AptQueueConfiguration);
    QueueConfigurationList.add(offQueueConfiguration);
    
    insert QueueConfigurationList;
    system.debug('QueueConfigurationList' +QueueConfigurationList);
    
    Queue_Configuration__c AptQueueConfig=QueueConfigurationList[0];
    Queue_Configuration__c offQueueConfig=QueueConfigurationList[1];
    

    
    List<Queue_Exception__c> QueueExceptionList=new List<Queue_Exception__c>();
    Queue_Exception__c QueueException1=new Queue_Exception__c(Exception_Type__c='MSA',Target_Name__c='APTMSA1',Rule_Name__c='Month 1 Market Coverage %',
                                                              Rule_Value__c=20,Queue_Configuration__c=AptQueueConfiguration.id);
    Queue_Exception__c QueueException2=new Queue_Exception__c(Property_Type__c='Multi Tenant',Exception_Type__c='All',Rule_Name__c='Month 1 Market Coverage %',
                                                              Rule_Value__c=20,Queue_Configuration__c=offQueueConfiguration.Id);
                                                              
    Queue_Exception__c QueueException3=new Queue_Exception__c(Target_Name__c='offMSA11',Exception_Type__c='All',Rule_Name__c='Month 1 Market Coverage %',
                                                              Rule_Value__c=20,Queue_Configuration__c=offQueueConfiguration.Id);
    
    QueueExceptionList.add(QueueException1);
    QueueExceptionList.add(QueueException2);
    QueueExceptionList.add(QueueException3);
    
    insert QueueExceptionList;
    system.debug('QueueExceptionList' +QueueExceptionList);
    
    List<ops_Pool_Data__c> PoolDataList=new List<OPS_POOL_DATA__C>();
    for(Integer i=1;i<=30;i++){
        
        
        ops_Pool_Data__c PoolData=new ops_Pool_Data__c(sector__c='Apt',MSA__c='APTMSA1',Sub_market_id__c='APTMSA11',contact_id__c='APTMSA11Contact'+i,
                                              Property_id__c='AptMSA11Property'+i,Property_Status__c='Completed');
        PoolDataList.add(PoolData);
    }
    
    for(Integer i=1;i<=20;i++){
        
        
        ops_Pool_Data__c PoolData=new ops_Pool_Data__c(sector__c='Apt',MSA__c='APTMSA1',Sub_market_id__c='APTMSA12',contact_id__c='APTMSA12Contact'+i,
                                              Property_id__c='AptMSA12Property'+i,Property_Status__c='Completed');
        PoolDataList.add(PoolData);
    }
    
    
    for(Integer i=1;i<=20;i++){
        ops_Pool_Data__c PoolData=new ops_Pool_Data__c(sector__c='Apt',MSA__c='APTMSA2',Sub_market_id__c='APTMSA21',contact_id__c='APTMSA21Contact'+i,
                                              Property_id__c='AptMSA21Property'+i,Property_Status__c='Completed');
        PoolDataList.add(PoolData);
    }
    
    for(Integer i=1;i<=10;i++){
        ops_Pool_Data__c PoolData=new ops_Pool_Data__c(sector__c='Apt',MSA__c='APTMSA2',Sub_market_id__c='APTMSA22',contact_id__c='APTMSA22Contact'+i,
                                              Property_id__c='AptMSA22Property'+i,Property_Status__c='Completed');
        PoolDataList.add(PoolData);
    }
    
    for(Integer i=1;i<=20;i++){
        
        ops_Pool_Data__c PoolData=new ops_Pool_Data__c(sector__c='Apt',MSA__c='APTMSA3',Sub_market_id__c='APTMSA31',contact_id__c='APTMSA31Contact'+i,
                                              Property_id__c='AptMSA31Property'+i,Property_Status__c='Completed');
        PoolDataList.add(PoolData);
    }
    
    insert PoolDataList;
    system.debug('PoolDataList' +PoolDataList);
                                                              
}
/*This testmethod is to test when there are no records in the Metric Table. 
Expected result is all the records should be of Priority 2-High
    /*APTMSA1 - APTMSA11 : 30
     * APTMSA1 - APTMSA12 : 20
     * APTMSA2 - APTMSA21 : 20
     * APTMSA2 - APTMSA22 : 10
     * APTMSA3 - APTMSA31: 20
     * */

public static testmethod void BuildQueueTest_FirstRun(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Do_Not_Call__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,CCRC__c,IAG__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Non_Published_MSAs__c,QA__c,Property_Name__c,Client_Services__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
    List<String> sector=new List<String>{'Apt'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    test.startTest();
    CountPoolDataBatchClass BatchClass;
    String PoolQueueId=[select Id from group where name='Pool Queue'].Id;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,CCRC__c,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }
        
        }   
           Database.executeBatch(BatchClass, BatchSize); 
         //   Id BatchID = Database.executeBatch(BatchClass, BatchSize); 
            //System.abortJob(BatchID);
           
            
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name from queue__c where priority__c='2-High' group by Survey_sector__r.name];
    system.debug('QueueCount=>'+QueueCount);
}
    
     /*This testmethod is to test when there are more than the month target in the Metric Table. 
Expected result is all the records should be of Priority 2-High.
MSA : APTMSA1 : Total Quantity 30, Submarket : APTMSA11 : 8 properties are added in the Metric. If run in the month2,5,8,11 the value we check is 30%.
*/
public static testmethod void BuildQueueTest_2(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,CCRC__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
    List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=8;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    system.debug('MetricDataList=>'+MetricDataList);
    test.startTest();
    CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(10,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(10,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }   
         
         }      
            Id BatchID = Database.executeBatch(BatchClass, 200); 
                
            
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount2=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}
    
/*This testmethod is to test when there are more than the month target in the Metric Table. 
Expected result is all the records should be of Priority 2-High. Except for the below MSA and Submarket.
MSA : APTMSA1 : Total Quantity 30, Submarket : APTMSA11 : 10 properties are added in the Metric. If run in the month2,5,8,11 the value we check is 30%.
*/
public static testmethod void BuildQueueTest_3(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,CCRC__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
    List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=10;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    system.debug('MetricDataList=>'+MetricDataList);
    test.startTest();
    CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }   
        }
            Database.executeBatch(BatchClass, BatchSize); 
            Id BatchID = Database.executeBatch(BatchClass, BatchSize); 
            //System.abortJob(BatchID);
            
            
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount2=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}
    
    /*This testmethod is to test when there are more than the month target in the Metric Table. 
Expected result is all the records should be of Priority 2-High. Except for the below MSA and Submarket.
MSA : APTMSA1 : Total Quantity 30, Submarket : APTMSA11 : 30 properties are added in the Metric. If run in the month2,5,8,11 the value we check is 30%. The Market and Sub Market Month Target is met.
*/
public static testmethod void BuildQueueTest_4(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
    List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=30;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    test.startTest();
    CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }
                      
        }   
            Database.executeBatch(BatchClass, BatchSize);           
        
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount4=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}

/*Scenario APTMSA1 is completely assigned i.e. both SubMarket.
 * Market not met but each submarket within market met month target
 * 
 * */    
    public static testmethod void BuildQueueTest_5(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
        List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=10;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
        
        for(Integer i=1;i<=5;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA12').Id,
                                              Property_id__c='AptMSA12Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    test.startTest();
        CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }   
                
        }   
        Database.executeBatch(BatchClass, BatchSize);   
            Id BatchID = Database.executeBatch(BatchClass, BatchSize); 
            //System.abortJob(BatchID);
        
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount4=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}
    
    /*Scenario APTMSA1 is completely assigned i.e. both SubMarket.
 * Market and SubMarket Met month coverage. Market Not Met quarter Coverage and SubMarket met quarter coverage.
 * 
 * */    
   public static testmethod void BuildQueueTest_6(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=200;
        String PropertyStatus='Completed';
        List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=15;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
        
        for(Integer i=1;i<=10;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA12').Id,
                                              Property_id__c='AptMSA12Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    test.startTest();
       CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }   
      
        }   
            Database.executeBatch(BatchClass, BatchSize); 
            Id BatchID = Database.executeBatch(BatchClass, BatchSize); 
           // System.abortJob(BatchID);
            
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount4=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}
    
     /*Scenario APTMSA1 is completely assigned i.e. both SubMarket.
 * Market and SubMarket Met month coverage. Market Not Met quarter Coverage and SubMarket met quarter coverage.
 * 
 * */    
    public static testmethod void BuildQueueTest_7(){
        String ProcessQuery='select property_ID__c,Property_Type__c,Last_Survey_Date__c,Month_Built__c,MSA__c,Sub_Market_ID__c,Time_Zone__c,Sector__c,Select_Code__c,Year_Built__c,MSA_Name__c,Sub_Market_Name__c,Property_Name__c,Contact_ID__c,Property_Status__c,Construction_Start_Month__c,Construction_Start_Year__c from Ops_Pool_Data__c ';
        Integer Month=Date.today().Month();
        Integer BatchSize=1000;
        String PropertyStatus='Completed';
        List<String> sector=new List<String>{'Apt','off'};
        List<Queue_Configuration__c> QueueConfig=[Select Id,sector__c from Queue_Configuration__c];
    
    Map<String,Survey_Sector__c> SectorMap=new Map<String,Survey_Sector__c>();
    Map<String,Survey_MSA__C> MSAMap=new Map<String,Survey_MSA__C>();
    Map<String,Survey_SubMarket__c> SubmarketMap=new Map<String,Survey_SubMarket__c>();
    
    List<Survey_Sector__c> SectorList=[Select Id,Name from survey_sector__c];
    List<Survey_MSA__C> MSAList=[Select Id,Name from survey_MSA__c];
    List<Survey_SubMarket__c> submarketList=[Select Id,Name from Survey_SubMarket__c];
    for(Survey_Sector__C s:SectorList){
        SectorMap.put(s.Name,s);
    }
    
    for(survey_MSA__c m:MSAList){
        MSAMap.put(m.Name, m);
    }
    
    for(survey_submarket__c s:submarketList){
        SubmarketMap.put(s.Name, s);
    }
    
    List<survey_metric__c> MetricDataList=new List<survey_metric__c>();
    for(Integer i=1;i<=15;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA11').Id,
                                              Property_id__c='AptMSA11Property'+i);
        MetricDataList.add(MetricData);
    }
        
        for(Integer i=1;i<=10;i++){
        survey_metric__c MetricData=new survey_metric__c(Survey_sector__c=SectorMap.get('Apt').Id,MSA__c=MSAMap.get('APTMSA1').Id,survey_Submarket__c=SubmarketMap.get('APTMSA12').Id,
                                              Property_id__c='AptMSA12Property'+i);
        MetricDataList.add(MetricData);
    }
    insert MetricDataList;
    test.startTest();
        CountPoolDataBatchClass BatchClass;
        for(String s:sector){
            system.debug('Sector=>'+s);
            String SearchQuery;
            String PropertiesCountQuery='Select Id,sector__c,MSA__C,Sub_Market_Id__C,Property_Type__c from ops_pool_Data__C where sector__c=\''+s+'\' Order By sector__c,MSA__C,Sub_Market_Id__C';
            String MetricCountQuery='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c,Property_Type__c from survey_metric__C where survey_sector__r.Name=\''+s+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
            String WhereClause=' Where Property_Status__c=\''+PropertyStatus+'\' AND Sector__c=\''+s+'\'';
            SearchQuery=ProcessQuery+WhereClause;
            system.debug('SearchQuery'+SearchQuery);
            
            
            If(QueueConfig[0].Sector__c==s){
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[0].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }else{
                BatchClass=new CountPoolDataBatchClass(Month,s,QueueConfig[1].Id,BatchSize,SearchQuery,PropertiesCountQuery,MetricCountQuery);
            }
            system.debug('BatchClass' +BatchClass);
           
        }
         Database.executeBatch(BatchClass, BatchSize);   
            Id BatchID = Database.executeBatch(BatchClass, BatchSize); 
           // System.abortJob(BatchID);
            
    test.stopTest();
    List<AggregateResult> QueueCount=[select count(id),Survey_sector__r.name, priority__c from queue__c group by Survey_sector__r.name,priority__c];
    system.debug('QueueCount4=>'+QueueCount);
    
        List<AggregateResult> SurveyCount=[select count(id),queue_record__c from Property_Survey__c group by queue_record__c];
    system.debug('SurveyCount=>'+SurveyCount);
}
     

    
}