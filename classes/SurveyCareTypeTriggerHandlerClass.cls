public class SurveyCareTypeTriggerHandlerClass {
    
    public static void HandleAfterInsert(Map<Id,Survey_Care_Types__c> NewMap){
        
        populateUniqueString(NewMap);
    }
    
    
    //populate 36characters long string
    public static void populateUniqueString(Map<Id,Survey_Care_Types__c> NewMap){
        List<Survey_Care_Types__c> SurveyCareList=new List<Survey_Care_Types__c>();
        for(String SurveyCareId: NewMap.keyset()){
            if(NewMap.get(SurveyCareId).Id !=null && (NewMap.get(SurveyCareId).Space_Oid__c== '' || NewMap.get(SurveyCareId).Space_Oid__c== null)){
                Blob b = Crypto.GenerateAESKey(128);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                system.debug(guid);
                Survey_Care_Types__c Survey =new Survey_Care_Types__c(Id=NewMap.get(SurveyCareId).Id ,Space_Oid__c=guid);
                SurveyCareList.add(Survey);
            }
            
        }
        update SurveyCareList;
    }
    
}