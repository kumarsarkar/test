public class ApprovalUtility {
/*Created by Chris Boulden 9-7-17. Class enables us to readily determine who the approvers of a process are and whether a user is an approver
*/
	//getApproverList returns a list of users designated to approve a record and accepts the record ID as an argument
    public static List<ID> getApproverList(Id recordId){
        //Query for all approval processses and all approvers in those approval processes
        List<ProcessInstance> approvalProcesses = [SELECT Id, (Select Id, ActorId, ProcessInstanceId FROM Workitems) FROM ProcessInstance WHERE TargetObjectId = :recordId];
        List<ProcessInstanceWorkitem> workList = new List<ProcessInstanceWorkitem>();
        List<Id> approverList = new List<Id>();
        List<Id> queueList = new List<Id>();
        List<Id> roleList = new List<Id>();

        //Create a list of approval processes from the record, sort approvers
        for(ProcessInstance p : approvalProcesses){

            workList.addAll(p.WorkItems);

        }
        for(ProcessInstanceWorkItem wi : workList){

            if(wi.ActorId.getsobjecttype().getDescribe().getName() == 'User'){

	            approverList.add(wi.ActorId);

	        }else if(wi.ActorId.getsobjecttype().getDescribe().getName() == 'Group'){

	            queueList.add(wi.ActorId);

	        }else if(wi.ActorId.getsobjecttype().getDescribe().getName().contains('Role')){

	            roleList.add(wi.ActorId);

	        }
        }
        //Need to map group ids to children groups so that we can determine which children users are members of approver groups
        List<Id> tempQueueList = new List<Id>();
        List<GroupMember> members = new List<GroupMember>();
        Map<Id,Group> queueMap = new Map<Id,Group>([SELECT Id,RelatedId, Name, Type, (SELECT Id, GroupId, UserorgroupId FROM GroupMembers) From Group]);
        //While loop looks through hierarchy levels until bottoming out
        while(queueList.size() > 0){
        	//Finds all groups at the current level of the hierarchy by mapping the children of the last groups
        	for(Id q : queueList){
        		members.addAll(queueMap.get(q).GroupMembers);
        	}
        	//Loop through groupmembers to place them in appropriate category
	        for(GroupMember gm : members){
	        	System.debug(gm.UserorGroupId);
	        	System.debug(gm);
	        	System.debug(queueMap.get(gm.UserorGroupId));

	            if(gm.UserorGroupId.getsobjecttype() == User.sobjecttype){
		            approverList.add(gm.UserorGroupId);
		        }else if(queueMap.get(gm.UserorGroupId).type == 'Queue'){
		            tempQueueList.add(gm.Id);
		        }else if(queueMap.get(gm.UserorGroupId).type.contains('Role')){
		            roleList.add(queueMap.get(gm.UserorGroupId).relatedId);
		        }
	        }
        	queueList = tempQueueList;
        	tempQueueList.clear();
	        members.clear();
        }
        //Find all users in the role list, add them as well
        List<User> usersFromRoles = [Select Id FROM User WHERE UserRole.Id In :roleList];        	
        for(User u : usersFromRoles){
        	approverList.add(u.Id);
        }
        system.debug(approverlist);
        return approverList;
    }

	//isApprover returns whether a user is an approver of a record
    public static Boolean isApprover(Id recordId, Id userId){
    	Boolean userAuth = False;
    	List<Id> approverlist = getApproverList(recordId);
    	if((new Set<Id>(approverList)).contains(userId) || isManager(userId,approverList)){//Check to see if the user is in the list of approvers, or if the user is the maanger of an approver
    		userAuth =  True;
    	}else if(new List<Profile>([SELECT Id, Name 
FROM Profile WHERE PermissionsModifyAllData = true AND Id = :userInfo.getProfileId()]).size() > 0){//Check to see if user has modify all permissions
    		userAuth =  True;
    	}

        return userAuth;
    } 

    //isManager returns whether or not the first user is a manager of the second user
    public static Boolean isManager(Id managerId, Id userId){
    	Boolean isBoss = False;
    	//need to determine the role id of both the manager and subordinate user, and map every role to its parent role
    	Id managerRole 			 = [SELECT Id,UserRoleId FROM User WHERE Id = :managerId ].UserRoleId;
    	Id roleToCheck 			 = [SELECT Id,UserRoleId FROM User WHERE Id = :userId ].UserRoleId;
    	Map<Id,UserRole> roleMap = new Map<Id,UserRole>([SELECT Id, ParentRoleId FROM UserRole]);
    	//initialize the first role to look up from as teh user roleid
    	while(roleMap.get(roleToCheck).ParentRoleID != null){
    		roleToCheck = roleMap.get(roleToCheck).ParentRoleID;
    		if(roleToCheck == managerRole){
    			isBoss = True;
    			break;
    		}
    	}
        return isBoss;
    } 

    //Same as last method except that a list of users is accepted.
    public static Boolean isManager(Id managerId, List<Id> userId){
    	Boolean isBoss = False;
    	//need to determine the role id of both the manager and subordinate user, and map every role to its parent role
    	Id managerRole 		  = [SELECT Id,UserRoleId FROM User WHERE Id = :managerId ].UserRoleId;
    	System.debug(managerRole);
    	List<User> tempUList  = [SELECT Id,UserRoleId FROM User WHERE Id In :userId ];
    	System.debug(tempUList);
    	//need to assign users from list to set of ids
    	Set<Id> rolesToCheck  = new Set<ID>();

    	for(User ur : tempUList){

    		rolesToCheck.add(ur.UserRoleId);

    	}
    	Set<Id> tempSet          = new Set<Id>();
    	Map<Id,UserRole> roleMap = new Map<Id,UserRole>([SELECT Id, ParentRoleId FROM UserRole]);
    	//initialize the first role to look up from as the user roleid
    	while(!rolesToCheck.isEmpty()){

	    	System.debug(rolesToCheck.isEmpty() == false);
    		for(Id r : rolesToCheck){

		    	if(roleMap.get(r).ParentRoleID != null){

		    		System.debug(roleMap.get(r).ParentRoleID);
		    		tempSet.add(roleMap.get(r).ParentRoleID);

		    	}
	    		if(r == managerRole){

	    			isBoss = True;
	    			break;
	    		}   		
    		}
 	    	if(isBoss){

	    		break;
	    	}
	    	rolesToCheck.clear();
	    	rolesToCheck.addAll(tempSet);
	    	tempSet.clear();		    	 
    	}

        return isBoss;
    } 
}