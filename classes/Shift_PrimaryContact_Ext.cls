/*
	@Name: Shift_PrimaryContact_Ext
	@Description: Extension for PrimaryContactDetails component. This code grabs the contact details for the opportunities primary contact if it exists
	@Dependancies: Shift_PrimaryContactDetails_Component.page
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/ 

public class Shift_PrimaryContact_Ext {

	//public Contact primaryContact {get; set;}
	public String theOppId {get;set;}
	public List<OpportunityContactRole> oppContactRole {get;set;}
	public String OpportunityIdStr {get;set;}

	public Shift_PrimaryContact_Ext() {}

	public Contact getPrimaryContact() {
				//Find the primary contact if it exist
		//List<OpportunityContactRole> oppContactRole = new List<OpportunityContactRole>();
		oppContactRole = [SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId=:theOppId AND isPrimary=TRUE LIMIT 1];

		//If a primary contact is found, then return the contact details
		contact tempCon = new contact();
		if (!oppContactRole.isEmpty()) {
			tempCon = [SELECT Name, Phone, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE Id=:oppContactRole[0].ContactId];
		}

		return tempCon;
	}	
}