/*
    @Name: Shift_NEFEmailPreview_Ext
    @Description: Extension for NEF Email PRreview VF page
    @Dependancies: 
    @Version: 1.0.0

    ===VERSION HISTORY ===
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial
*/

public with sharing class Shift_NEFEmailPreview_Ext {

    private final Opportunity theOpp;

    public Shift_NEFEmailPreview_Ext(ApexPages.StandardController stdController) {
        this.theOpp = (Opportunity)stdController.getRecord();
    }

    /*** toggle the send flag from true to false **/
    public pageReference triggerEmailAlert() {
        this.theOpp.Send_NEF_Flag__c = true;

        try {
            update this.theOpp;
        } catch (dmlException e) {
            system.debug('Error: ' + e);
        }

        this.theOpp.Send_NEF_Flag__c = false;

        try {
            update this.theOpp;
        } catch (dmlException e) {
            system.debug('Error: ' + e);
        }   

        //Redirect page back to the opp
        PageReference pageRef = new PageReference('/'+this.theOpp.Id);
        pageRef.setRedirect(true);
        return pageRef;  
    }

    public pageReference theCancel() {
       //Redirect page back to the opp
        PageReference pageRef = new PageReference('/'+this.theOpp.Id);
        pageRef.setRedirect(true);
        return pageRef;   
    }
}