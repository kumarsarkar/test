public class QueueTriggerHandlerClass {
    
    public static void HandleAfterUpdate(Map<Id,Queue__c> NewMap,Map<Id,Queue__c> OldMap){
        ProcessCallStatusChange(NewMap,OldMap);
        for(String NewId:NewMap.keySet()){
            set<String> NonAssignStatusSet=new set<String>{'Pending Creation','Pending Assignment','In Progress'};
            if((NewMap.get(NewId).Status__c!=OldMap.get(NewId).status__c) && !NonAssignStatusSet.contains(NewMap.get(NewId).Status__c) && OldMap.get(NewId).status__c!='Pending Creation' && OldMap.get(NewId).status__c!='Pending Assignment'){
                AssignQueue();
                break;
            }else{
                if((NewMap.get(NewId).call_Status__c!=OldMap.get(NewId).call_status__c)){
                	AssignQueue();
                    break;
                }
            }
            
            //Logic to update the OwnerId on the Prop Surveys when it changes on Queue.
            
            if(NewMap.get(NewId).OwnerId!=OldMap.get(NewId).OwnerId){
                List<Property_Survey__c> PropSurveyList=new List<Property_Survey__c>();
                system.debug('PropSurvey');
                for(Property_Survey__c PropSurvey:[Select Id,OwnerId from Property_Survey__c where queue_record__c=:NewMap.keySet()]){
                    PropSurvey.OwnerId=NewMap.get(NewId).OwnerId;
                    PropSurveyList.add(PropSurvey);
                }
                update PropSurveyList;
            }
        }
    }
    
    public static void AssignQueue(){
        List<Surveyor__c> SurveyorRecList=[select Id from Surveyor__c where user__c=:UserInfo.getUserId() limit 1];
        system.debug('i am here+'+SurveyorRecList);
        if(SurveyorRecList.size()>0){
            String SurveyorId=SurveyorRecList[0].Id;
            List<Queue__c> QueueList=new List<Queue__c>();
            List<Queue_configuration__c> QueueconfigList=new List<Queue_configuration__c>();
            List<Property_Survey__c> PropSurveyList=new List<Property_Survey__c>();
            QueueAssignmentUtils.AssignQueue(SurveyorId,QueueList,QueueconfigList,PropSurveyList);
            try{
                update QueueList;
                update PropSurveyList;
                update QueueconfigList;
            }catch(exception e){
                throw(e);
            }
        }
    }
    
    public static void ProcessCallStatusChange(Map<Id,Queue__c> NewMap,Map<Id,Queue__c> OldMap){
   /*
        set<String> OnHoldCallStatus=new set<String>{'\'Reached Voicemail - Left Message\'','\'Reached Voicemail - No Message\'','\'Told to Call Back\'',
            '\'Ring Out\'','\'Busy Signal\'','\'Contact Not Available\'','\'Contact Not in Office\'','\'Left On Hold\''};
                set<String> OnMonthHoldCallStatus=new set<String>{'\'Sent to Web - No Info\'','\'E-mail - No Info\'','\'Fax - No Info\'',
                    '\'Number Blocked\'','\'Receptionist Refused - No Info\''};*/
        List<String> IdList=new List<String>();
        List<String> OnMonthHoldIdList=new List<String>();
        for(Id NewId: NewMap.keyset()){
            IdList.add(NewId);
        }
        Boolean isOnMonthHoldExist=false;
        Boolean isOnHoldExist=false;
        Map<Id,Queue__c> QueueMap=new Map<Id,Queue__c>([Select Id,call_status__c,ownerid,(Select Id,status__c,call_status__c from property_Surveys__r) from queue__c where Id=:IdList]);
        List<Property_Survey__c> UpdatePropSurveyList=new List<Property_Survey__c>();
      //  String PoolQueueId=[Select Id from Group where name='Pool Queue'].Id;
        for(Id Newid:NewMap.keySet()){
            if(NewMap.get(Newid).call_status__c!=OldMap.get(Newid).call_status__c){
                //              system.debug('OnHoldCallStatus=>'+OnHoldCallStatus);
                //            system.debug('NewMap.get(Newid).call_status__c=>'+NewMap.get(Newid).call_status__c);
                for(Property_Survey__c PropSurvey:QueueMap.get(Newid).property_surveys__r){
                    //	PropSurvey.Status__c='On Hold';    
                    	PropSurvey.call_status__c=NewMap.get(Newid).call_status__c;
                    UpdatePropSurveyList.add(PropSurvey);
                }
            /*    if(OnHoldCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
                    //                system.debug('Yes=>NewMap.get(Newid).call_status__c=>'+NewMap.get(Newid).call_status__c);
                    isOnHoldExist=true;
                    QueueMap.get(Newid).status__c='On Hold';
                    for(Property_Survey__c PropSurvey:QueueMap.get(Newid).property_surveys__r){
                    //	PropSurvey.Status__c='On Hold';    
                    	PropSurvey.call_status__c=NewMap.get(Newid).call_status__c;
                    }
                    UpdatePropSurveyList.addAll(QueueMap.get(Newid).property_surveys__r);
                }else if(OnMonthHoldCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
                    //      system.debug('Here1');
                    isOnMonthHoldExist=true;
                    QueueMap.get(Newid).ownerId=PoolQueueId;
                    QueueMap.get(Newid).status__c='Pending Creation';
                    OnMonthHoldIdList.add(Newid);
                    for(Property_Survey__c PropSurvey:QueueMap.get(Newid).property_surveys__r){
                    	PropSurvey.Status__c='Pending Creation';    
                    }
                    UpdatePropSurveyList.addAll(QueueMap.get(Newid).property_surveys__r);
                }*/
            }
        }
    /*    
        if(isOnHoldExist || isOnMonthHoldExist){
            //system.debug('Here2');
            if(isOnMonthHoldExist){
                // system.debug('Here3=>'+OnMonthHoldIdList);
                List<Property_Survey__c> PropSurveyList=[select Id,Property_id__c from property_Survey__c where queue_record__c=:OnMonthHoldIdList];
                List<String> OnMonthHoldPropId=new List<String>();
                for(Property_Survey__c PropSurvey:PropSurveyList){
                    OnMonthHoldPropId.add(PropSurvey.Property_Id__c);
                }
                // system.debug('Here4=>'+OnMonthHoldPropId);
                List<Ops_pool_data__c> PoolDataList=[Select Id,Next_Queue_Date__c from ops_pool_data__c where property_id__c=:OnMonthHoldPropId];
                for(Ops_pool_data__c PoolData:PoolDataList){
                    PoolData.Next_Queue_Date__c=Date.Today().addMonths(1);
                }
                // system.debug('Here5=>'+PoolDataList);
                update PoolDataList;
            }
            update QueueMap.values();
            update UpdatePropSurveyList;
        }*/
        update UpdatePropSurveyList;
    }
}