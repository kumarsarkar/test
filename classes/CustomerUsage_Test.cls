@isTest
public class CustomerUsage_Test {

	public static testMethod void testActivtyPreview() {
        Account b = new Account();
        b.Name = 'Test Parent1';
        insert b;
        
        Account a = new Account();
        a.Name = 'Test1';
        a.ParentId = b.Id;
        insert a;
        
        Customer_Usage_Analysis__c c1 = new Customer_Usage_Analysis__c();
        c1.Account__c = a.Id;
        insert c1;
        
        Customer_Usage_Analysis__c c2 = new Customer_Usage_Analysis__c();
        c2.Account__c = b.Id;
        insert c2;
        
        
        PageReference pageRef = Page.CustomerUsage;
        Test.setCurrentPage(pageRef);
        
        CustomerUsageController ctrl = new CustomerUsageController();
        string ids = CustomerUsageController.getChildrenIDs(a.Id);
        ids = CustomerUsageController.getChildrenIDs(b.Id);
        
        List<SelectOption> x = ctrl.UsageAnalysisAccounts;
        string d = ctrl.AlphaFilter;
        list<String> xy = ctrl.AlphaList;
        List<Account> ab = ctrl.UAAccounts;
        
        ctrl.SearchAccount();
        ctrl.ClearAll();
        ctrl.codeCoverage();
        
    }    
}