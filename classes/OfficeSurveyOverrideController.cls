public class OfficeSurveyOverrideController {
    public id recordId;
    public Property_Survey__c tempRecord {get;set;}
    public string saveResults {get;set;}
    public List<contactWrapper> contactList {get;set;}
    public static id staticRecordId{get;set;}
    public List<amenitiesWrapper> amenitiesList {get;set;}
    public List<Schema.FieldSetMember> secondFloorPlan {get;set;}
    public Survey_Amenities__c newAm {get;set;}

    //custom setter and getter to ensure when we save all the records are properly updated
    public List<sstWrapper> floorplans {
        get{
            return floorplans;
        }
        set{
            floorplans = value;
        }}

    //Methods to get the fields from the fieldsets
    public List<Schema.FieldSetMember> getSurveyContactFields(){
        return SObjectType.Survey_Contact__c.FieldSets.SurveyContactFields.getFields();
    }
    public List<Schema.FieldSetMember> getReisContactFields(){
        return sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields();
    }

    public List<Schema.FieldSetMember> getLocationFields(){
        return SObjectType.Property_Survey__c.FieldSets.PropertyLocation.getFields();
    }

    public List<Schema.FieldSetMember> getInformationFields(){
        return SObjectType.Property_Survey__c.FieldSets.PropertyInformation.getFields();
    }

    public List<Schema.FieldSetMember> getFloorpanHeaderFields(){
        Return SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurveyHeader.getFields();
    }

    //Sumedha: Added for Office and Industrial
    public List<Schema.FieldSetMember> getOfficeFloorpanHeaderFields(){
        Return SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeader.getFields();
    }

    //Sumedha: Added for Office and Industrial
    public List<Schema.FieldSetMember> getOfficeFloorpanHeaderAllFields(){
        Return SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeaderNew.getFields();
    }

    public List<fieldWrapper> getFloorPlanFields(){
        List<fieldWrapper> returnList = new List<fieldWrapper>();
        List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
        //Sumedha: Added for Office.
        if(tempRecord.RecordType.name == 'Office'){
            tempList = SObjectType.Property_Survey__c.FieldSets.OfficeFloorPlanSurveyHeaderNew.getFields();
        }
        if(tempRecord.RecordType.name == 'Apartments'){
            tempList = SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields();
        }
        Double counter = 0;
        Double total = tempList.size();
        for(Schema.FieldSetMember fsm :tempList){
            fieldWrapper fw = new fieldWrapper();
            if(counter/total < .5){
                fw.afield = fsm;
                fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
            }
            else{
                break;
            }
            counter++;
            returnList.add(fw);
        }
        return returnList;
    }

    //fieldWrapper is used with field sets for the survey space type records related list, allows us to set editable and readonly fields seperatly
    public class fieldWrapper{
        public Schema.FieldSetMember afield {get;set;}
        public Schema.FieldSetMember bfield {get;set;}

        public fieldWrapper(){
            //this.afield = new Schema.FieldSetMember();
            //this.bfield = new Schema.FieldSetMember();
        }
    }
    //getFields method only returns to us the fields of the property survey record
    public List<Schema.FieldSetMember> getFields(){
        List<Schema.FieldSetMember> returnList = new List<Schema.FieldSetMember>();
        returnList.addAll(getLocationFields());
        returnList.addAll(getInformationFields());
        return returnList;
    }

    //gets us the survey space type records we need to use in the related list, also uses field sets to get data
    //we can expand this to get different fieldsets based on record type
    public List<sstWrapper> getFloorPlanMap(){
        List<sstWrapper> returnMap = new List<sstWrapper>();
        String queryString = 'Select ';
        for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields()){
            QueryString += f.getFieldPath()+', ';
        }
        QueryString += 'Id, Name,Delete_By_Foundation__c,Apt_Space_Type__c,Reis_Space_Type__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
        for(Survey_Space_Type__c sst :Database.query(QueryString)){
                returnMap.add(new sstWrapper(sst));
        }
        return returnMap;
    }

    //uses custom contact wrapper, we get all survey contacts and reis contacts and put them into the wrapper
    // inoder for us to end the information we need to update the reis contact directly, there are also fields on the survey contact that need to be updated at times as well
    // this helps with that.
    public List<contactWrapper> getContacts(){
        list<contactWrapper> returnList = new list<contactWrapper>();
        map<String,Survey_Contact__c> emailMap = new map<String,Survey_Contact__c>();
        List<id> idList = new List<id>();
        String queryString = 'Select ';
        For(Schema.FieldSetMember f: This.getSurveyContactFields()){
            QueryString += f.getFieldPath()+', ';
        }
        QueryString += 'Id, Contact__c From Survey_Contact__c Where Property_Survey__c = \''+recordId+'\'';
        for(Survey_Contact__c s :Database.query(QueryString)){
            if(emailMap.get(s.email__c) == null){
                emailMap.put(s.email__c, s);
                idList.add(s.contact__c);
            }
        }
        String newQueryString = 'Select ';
        for(Schema.FieldSetMember f :sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields()){
            newQueryString += f.getFieldPath()+', ';
        }
        newQueryString += 'Id from Reis_Contact__c where id in :idList';
        for(Reis_Contact__c r:Database.query(newQueryString)){
            if(emailMap.get(r.email__c) != null){
                returnList.add(new contactWrapper(emailMap.get(r.email__c), r));
            }
        }
        Return returnList;
        //Return Database.query(QueryString);
    }

    public Property_Survey__c getTheRecord(){
        //gets us the fields we need to use for the viewing survey contact record, we use a field set, and some fields we think will always be on
        String QueryString = 'Select Id, ';
        For(Schema.FieldSetMember f: This.getFields()){
            QueryString += f.getFieldPath()+', ';
        }
        For(Schema.FieldSetMember fs: This.getOfficeFloorpanHeaderAllFields()){
            QueryString += fs.getFieldPath()+', ';
        }
        QueryString += 'Name,Unit_Mix_Estimated__c,Leasing_Incentives__c, Rent_ref__c,Rest_neg__c,Vac_Ref__c,Ref_Rest__c,DK_rest__c, Current_Available__c,Available_2_4_weeks__c,Total_Available_Units__c, Total_NC_Units__c, Facility_Name__c,Select_Code__c , RecordType.Name, Total_Units__c, Opex_New__c,Real_Estate_Taxes_New__c, Lease_Term_New__c, TI_Allowance_New_New__c, TI_Allowance_Renew_New__c, Free_Rent_New__c, Contract_Rent_Discount_New__c, Commission_New_New__c, Commission_Renew_New__c,Sold__c,Sold_Date__c,Sold_Amount_New__c From Property_Survey__c Where ID = \''+recordId+'\' LIMIT 1';

        Return Database.query(QueryString);
    }

    public void saveRecord(){

        try{
            updateAmenities();
            insertContacts();

            if(tempRecord.RecordType.name == 'Apartments'){
                apartmentSave();
            }
            update tempRecord;
        }
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
        }
    }

    public void newSST(){
        //adds a new survey space type to our wrapper/related list
        floorplans.add(new sstWrapper( new Survey_Space_Type__c(Property_Survey__c = recordid)));
        system.debug(floorplans.size());
    }

    public void apartmentSave(){
        //spesific save logic for the apartments record type
        List<Survey_Space_Type__c> insertList = new List<Survey_Space_Type__c>();
        for(sstWrapper s :floorplans){
            system.debug(s);
            s.sst.Reis_Space_Type__c = s.sst.Apt_Space_Type__c;
            //for(Survey_Space_Type__c sst :floorplans.get(s)){
                //system.debug(sst);
                insertList.add(s.sst);
            //}
        }
            upsert insertList;
    }


    public void AddToConList(){
        //adds a new contact to the contact wrapper / related list
        contactList = contactWrapper.addNewCon(contactList);
    }

    public void insertContacts(){
        //save method for the contacts, there was no indication that we had to check for items to change
        //so we just update all the records with the values we have
        List<Survey_Contact__c> insertList = new List<Survey_Contact__c>();
        List<Survey_Contact__c> updateSCList = new List<Survey_Contact__c>();
        List<Reis_Contact__c> updateRCList = new List<Reis_Contact__c>();
        List<Reis_Contact__c> newContactList = new List<Reis_Contact__c>();
        for(contactWrapper c :contactList){
            system.debug(c);
            if(c.newcon && (c.rcon.Name != null && c.rcon.Name != '')){
                Reis_Contact__c r = c.rCon;
                newContactList.add(r);
                system.debug(r);
                insertList.add(c.con);
            }
            else{
                updateSCList.add(c.con);
                updateRCList.add(c.rcon);
            }
        }

        if(insertList.size() > 0 && newContactList.size() > 0){
            try{
                insert newContactList;
                Integer count = 0;
                for(Survey_Contact__c c :insertList){
                    system.debug(c);
                    system.debug(newContactList[count]);
                        c.Property_Survey__c = recordId;
                        c.Contact__c = newContactList[count].id;
                        count++;
                }
                insert insertList;
            }
            catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
          ApexPages.addMessage(myMsg);
            }
        }
        if(updateSCList.size() > 0 && updateRCList.size() > 0){
            try{
                update updateSCList;
                update updateRCList;
            }
            catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
          ApexPages.addMessage(myMsg);
            }
        }
    }

    public PageReference sumbitForApproval(){
        //baisc submit for approval
    try{
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval'); // You can make comments dynamic
        req.setObjectId(RecordId);
        Approval.ProcessResult result = Approval.process(req);
    }
    catch (Exception ex){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
        ApexPages.addMessage(myMsg);
    }
    //return submitPageRef;
    return null;
}

    public OfficeSurveyOverrideController(ApexPages.StandardController controller) {
        //constructor
        system.debug('new run of the controller');
        recordId = controller.getId();
        staticRecordId = controller.getId();
        tempRecord = getTheRecord();
        /*tempRecord = [Select id, Opex_New__c, Real_Estate_Taxes_New__c,Address__c,Direct_Available_Current__c,Lease_Term_New__c, TI_Allowance_New_New__c, TI_Allowance_Renew_New__c,
                     Free_Rent_New__c, Contract_Rent_Discount_New__c, Commission_New_New__c, Commission_Renew_New__c
                     from Property_Survey__c where Id =: recordId];*/
        contactList = getContacts();
        generateAmenityList();
        if(tempRecord.recordtype.name == 'Apartments'){
            floorplans = getFloorPlanMap();
        }

    }
    public class sstWrapper{
        //survey space tpye wrapper so we can create new survey space type records in our related list
        public Survey_Space_Type__c sst {Get;set;}
        public boolean newSst {get;set;}

        public sstWrapper(){
            this.sst = new Survey_Space_Type__c();
            this.newSst =false;
        }

        public sstWrapper(Survey_Space_Type__c s){
            this.sst = s;
            this.newSst =false;
        }
    }



    @remoteAction
    public static void logACall(String a, String rid){
        //log a call remote action for use on the VF Page
        //depricated
        task t = new task();
        t.whatId = rid;
        t.subject = 'Call';
        t.subject =a;
        t.status = 'Completed';
        t.description = 'Call at: '+system.now();
        t.activityDate = system.today();
        insert t;
        system.debug(t.id);
    }

    @remoteAction
    public static void remoteSave(Boolean b, String rid){
        //used in out reocrd type change validation
        Property_Survey__c a = [select id, needs_qa_review__c from Property_Survey__c where id = :rid limit 1];
        a.needs_qa_review__c = b;
        update a;
    }

    @remoteAction
    public static void remoteAddComment(string newComment, String rid){
        //used on all most all of our validations
        Property_Survey__c a = [select id, needs_qa_review__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
        //a.QC_Notes_New__c += ' ('+system.now()+') '+newComment;
        if(a.Property_Notes_New__c == null){
            a.Property_Notes_New__c = ' ('+system.now()+') '+newComment;
        }
        else{
            a.Property_Notes_New__c += ' ('+system.now()+') '+newComment;
        }

        update a;
    }

    @remoteAction
    public static Boolean remoteSiteCheck(String toCheck, Boolean propertySite){
        //used on site fields to determine if they are on our restricted list, if they are, we blank out on the client side
        if(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()) != null){
            if(propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).property_site__c){
                return true;
            }
            else if(!propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).Survey_Site__c){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public class amenitiesWrapper{
        //amenitiesWrapper is used to add new records to our related lists
        // also contains the type field so we can determine which list to populate with which record
        public Survey_Amenities__c sa {get;set;}
        public String type {get;set;}

        public amenitiesWrapper(){
            this.sa = new Survey_Amenities__c();
            this.type = '';
        }

        public amenitiesWrapper(Survey_Amenities__c s, String t){
            this.sa = s;
            this.type = t;
        }
    }



    public void generateAmenityList(){
        //populate our amenity wrapper list
        //set our new amenity to a blank amenity so we can create new ones quickly
        newAm = new Survey_Amenities__c();
        amenitiesList = new List<amenitiesWrapper>();
        for(Survey_Amenities__c s :[select id,Amenity__r.Has_Quantity__c, Delete_By_Foundation__c,Apartment_Amenity__c,Industrial_Amenities__c,Quantity_New__c, Amenity__r.Name, Amenity__r.Type__c from Survey_Amenities__c where Property_Survey__c = :recordId]){
            system.debug(s);
            amenitiesList.add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
        }
    }

    public void addNewAmmenity(){
        //inserts our new amenity and updates our pages viewing list
        newAm.Property_Survey__c = recordId;
        newAm.Amenity__c = newAm.Apartment_Amenity__c;
        Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Apartment_Amenity__c limit 1];
        for(amenitiesWrapper a :amenitiesList){
            if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
                return;
            }
            else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
                return;
            }
        }
        insert newAm;
        generateAmenityList();
    }

    // Sumedha : Added for the Industrial Sector
    public void addNewAmmenityIndustry(){
        //inserts our new amenity and updates our pages viewing list
        newAm.Property_Survey__c = recordId;
        newAm.Amenity__c = newAm.Industrial_Amenities__c;
        Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Industrial_Amenities__c limit 1];
        for(amenitiesWrapper a :amenitiesList){
            if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
                return;
            }
            else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
                return;
            }
        }
        insert newAm;
        generateAmenityList();
}

    public void updateAmenities(){
        //updates any changes we made to existing amenities
        List<Survey_Amenities__c> updateList = new List<Survey_Amenities__c>();
        for(amenitiesWrapper aw :amenitiesList){
            updateList.add(aw.sa);
        }

        try{
            update updateList;
        }
        catch(Exception e){
            system.debug(e);
        }
    }

//depricated
    public String removalId {get;set;}
    public void removeAmenity(){
        system.debug(removalId);
        Survey_Amenities__c ps = [select id from Survey_Amenities__c where id = :removalId limit 1];
        delete ps;
       generateAmenityList();
    }
}