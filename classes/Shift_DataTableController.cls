public with sharing class Shift_DataTableController {
  public String userLocale {get;set;}

  public Shift_DataTableController() {
    userLocale = UserInfo.getLocale();
  }
}