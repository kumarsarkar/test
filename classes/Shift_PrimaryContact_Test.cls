/*
	@Name: Shift_PrimaryContact_Test
	@Description: Unit test for the Shift_PrimaryContact_Ext 
	@Dependancies: Shift_PrimaryContactDetails_Component.page, Shift_PrimaryContact_Ext.cls
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/ 

@isTest
public class Shift_PrimaryContact_Test {
	
	@isTest static void testGetPrimaryCon() {
		Account tempAcc = new Account(Name='Test');
		Insert tempAcc;

		Opportunity tempOpp = new Opportunity(
			AccountId=TempAcc.Id,
			Name='Test Opp',
			StageName='Closed',
			CloseDate=Date.Today()
		);
		insert tempOpp;

		Contact tempCon = new Contact(
			AccountId=tempAcc.Id,
			LastName='testLNAME'
		);
		insert tempCon;

		OpportunityContactRole ocr = new OpportunityContactRole(
			OpportunityId = tempOpp.Id,
			ContactId = tempCon.Id,
			isPrimary = true
		);
		insert ocr;

		Shift_PrimaryContact_Ext controller = new Shift_PrimaryContact_Ext();
		Controller.theOppId = tempOpp.Id;
		Contact tempContact = controller.getPrimaryContact();
		system.assertEquals(tempContact.Id, tempCon.Id);
	}
}