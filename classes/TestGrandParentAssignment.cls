@isTest
public class TestGrandParentAssignment {
    static testmethod void testGrandParent() {
        // Test: 
        // - Create Account AA
        // - Create Account BB, with AA as Parent
        // - Create Account CC, with BB as Parent
        // - Create Account DD, with CC as Parent
        // - Update CC set Parent to Null
        // - Update CC set Parent to BB
        Account aa = new Account(Name = 'AA', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association');
        insert aa;
        
        Account bb = new Account(Name = 'BB', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = aa.Id);
        insert bb;
        
        Account cc = new Account(Name = 'CC', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = bb.Id);
        insert cc;

        Account dd = new Account(Name = 'DD', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = cc.Id);
        insert dd;

        Account ee = new Account(Name = 'EE', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = dd.Id);
        insert ee;
        
        Account ff = new Account(Name = 'FF', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = ee.Id);
        insert ff;

        Account gg = new Account(Name = 'GG', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = ff.Id);
        insert gg;

        Account hh = new Account(Name = 'HH', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = gg.Id);
        insert hh;        
        
        Account ii = new Account(Name = 'II', Type = 'Affiliate', Account_Type__c = 'Active', Industry = 'Academic/Association', ParentId = hh.Id);
        insert ii;        

        cc.ParentId = bb.Id;
        update cc;             
        
        bb.ParentId = null;
        update bb;                
    }    
}