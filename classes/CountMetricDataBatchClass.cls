Global class CountMetricDataBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
    Map<String,Integer> SubMarketCounterMap;
    Map<String,Integer> MarketCounterMap;
    Map<String,Integer> PropertiesSubMarketCounterMap;
    Map<String,Integer> PropertiesMarketCounterMap;
    Map<String,Integer> PropertiesSubMarketRentCounterMap;
    Map<String,Integer> PropertiesSubMarketVacancyCounterMap;
    String Sector;
    String QueueConfigId;
    Integer BatchSize;
    String ProcessQuery,MetricsQuery;
    Integer Month;
    Integer NoOfDaysinQuarter;
    public CountMetricDataBatchClass(Integer NoOfDaysinQuarter,Integer Month,String Sector,String QueueConfigId,Integer BatchSize,Map<String,Integer> PropertiesMarketCounterMap,Map<String,Integer> PropertiesSubMarketCounterMap,String ProcessQuery,String MetricsQuery){
    	this.Sector=Sector;
        this.SubMarketCounterMap=new Map<String,Integer>();
        this.MarketCounterMap=new Map<String,Integer>();
        //Anantha 21/07/2017
       // this.PropertiesSubMarketRentCounterMap=new Map<String,Integer>();
       // this.PropertiesSubMarketVacancyCounterMap=new Map<String,Integer>();
        
        this.QueueConfigId=QueueConfigId;
        this.BatchSize=BatchSize;
        this.PropertiesSubMarketCounterMap=PropertiesSubMarketCounterMap;
        this.PropertiesMarketCounterMap=PropertiesMarketCounterMap;
        this.ProcessQuery=ProcessQuery;
        this.MetricsQuery=MetricsQuery;
        this.Month=Month;
        this.NoOfDaysinQuarter=NoOfDaysinQuarter;
    }
    global Database.querylocator start(Database.BatchableContext BC){
        
      //  String Query='Select Id,survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c from survey_metric__C where survey_sector__r.Name=\''+Sector+'\' Order By survey_sector__r.Name,MSA__r.foundation_id__c,Survey_Submarket__r.foundation_id__c';
        return Database.getQueryLocator(MetricsQuery);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for(sObject scopeitem:scope){
            survey_metric__C MetricRecord=(survey_metric__C)scopeitem;
            String SubMarketKeyString=MetricRecord.survey_sector__r.Name+';'+MetricRecord.Property_Type__c+';'+MetricRecord.MSA__r.foundation_id__c+';'+MetricRecord.Survey_Submarket__r.foundation_id__c;
            String MarketKeyString=MetricRecord.survey_sector__r.Name+';'+MetricRecord.Property_Type__c+';'+MetricRecord.MSA__r.foundation_id__c;
            
            if(SubMarketCounterMap.containsKey(SubMarketKeyString)){
                Integer Tempcount=SubMarketCounterMap.get(SubMarketKeyString);
                SubMarketCounterMap.put(SubMarketKeyString, TempCount+1);
            }else{
                SubMarketCounterMap.put(SubMarketKeyString, 1);
            }
            
            if(MarketCounterMap.containsKey(MarketKeyString)){
                Integer Tempcount=MarketCounterMap.get(MarketKeyString);
                MarketCounterMap.put(MarketKeyString, TempCount+1);
            }else{
                MarketCounterMap.put(MarketKeyString, 1);
            }
            
            
            //Anantha 21/07/2017
            /* Commented as we drive it through the Survey_Completed__c flag.
            if(MetricRecord.Rent__c){
                if(PropertiesSubMarketRentCounterMap.containsKey(SubMarketKeyString)){
                    Integer TempCount=PropertiesSubMarketRentCounterMap.get(SubMarketKeyString)+1;
                    PropertiesSubMarketRentCounterMap.put(SubMarketKeyString, TempCount);
                }else{
                    PropertiesSubMarketRentCounterMap.put(SubMarketKeyString, 1);
                }
            }
            if(MetricRecord.Vacancy__C){
                if(PropertiesSubMarketVacancyCounterMap.containsKey(SubMarketKeyString)){
                    Integer TempCount=PropertiesSubMarketVacancyCounterMap.get(SubMarketKeyString)+1;
                    PropertiesSubMarketVacancyCounterMap.put(SubMarketKeyString, TempCount);
                }else{
                    PropertiesSubMarketVacancyCounterMap.put(SubMarketKeyString, 1);
                }
            }*/
            
            
        }
    }
    global void finish(Database.BatchableContext BC){
        System.debug('CounterMapsize=>'+SubMarketCounterMap.size());
        system.debug('CounterMap=>'+SubMarketCounterMap);
        BuildQueue_Batch Batchclass=new BuildQueue_Batch(NoOfDaysinQuarter,Sector,Month,ProcessQuery,QueueConfigId,PropertiesSubMarketCounterMap,SubMarketCounterMap,PropertiesMarketCounterMap,MarketCounterMap);
        Database.executeBatch(Batchclass, 1000);
    }
}