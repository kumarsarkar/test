@isTest
public class DeleteQueueBatchTestClass {
    @testsetup
    static void setupdata(){
        Survey_Sector__c AptSurveySector=new Survey_Sector__C(Name='Apt',Description__c='Apartments');
 insert AptSurveySector;

Queue__c queue1 = new Queue__c(Survey_Sector__c = AptSurveySector.id,Status__c = 'Pending Assignment' , Batch_Id__c= '875494',Priority__c = '2-High',No_Of_Surveys__c = 1);
insert queue1;

Queue__c queue2 = new Queue__c(Survey_Sector__c = AptSurveySector.id,Status__c = 'Pending Creation' ,Batch_Id__c= '123456', Priority__c = '2-High',No_Of_Surveys__c = 0);
insert queue2;


Surveyor__c surveyor = new Surveyor__c( User__c = UserInfo.getUserId(), IAG__c = true,QA__c = true);
insert surveyor;

 
 
 Survey_MSA__C MSA=new Survey_MSA__C(Name='APTMSA',Sector__c='Apt',MSA_Status__c='Published',Foundation_Id__c='APTMSA');
 insert MSA;
 
 Survey_Submarket__c Submarket=new Survey_Submarket__c(Name=MSA.Name,foundation_id__c=MSA.Name,MSA__C=MSA.Id,Sector__c=MSA.Sector__c);
 insert Submarket;

List<Property_Survey__c> SurveyList = new List<Property_Survey__c>();
 Property_Survey__c propsurvey1=new Property_Survey__c( Survey_Data_Can_Be_Purged__c = true,Survey_Data_Needed__c = true,Survey_Sector__c=AptSurveySector.id, status__c ='In QA Review', Queue_Record__c = queue2.id,INCOMING_SURVEY_DATA__C = 'test q23',Property_Id__c = '173456' );
  Property_Survey__c propsurvey2=new Property_Survey__c(Survey_Data_Can_Be_Purged__c = true,Survey_Submarket__c = Submarket.id,Opex__c = '2424',Survey_MSA__c = MSA.Id,Survey_Sector__c=AptSurveySector.id,status__c ='In QA Review', Queue_Record__c = queue1.id, INCOMING_SURVEY_DATA__C = 'test 3',Property_Id__c = '12345645'  );
  SurveyList.add(propsurvey1);
  SurveyList.add(propsurvey2);
  insert SurveyList;
    }
    
static TestMethod void deleteQueueTest(){
system.debug('***enter');


  /*
  SurveyList[0].Survey_Data_Can_Be_Purged__c = true;
  SurveyList[1].Survey_Data_Can_Be_Purged__c = true;
  update SurveyList;
  */
  Test.startTest();
    		Queue_Configuration__c QueueConfig=new Queue_Configuration__c(Sector__c='Apt');
    insert QueueConfig;
			
          //  DeleteQueueBatchClass obj = new DeleteQueueBatchClass('123456','Apt',QueueConfig.Id);
          //  DataBase.executeBatch(obj); 
            
  Test.stopTest();
  
  
}

}