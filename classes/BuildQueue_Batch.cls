Global class BuildQueue_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
    String ProcessQuery;
    String TotalCount;
    Map<String,Integer> PropertiesCountMap,MetricCountMap,PropertiesMarketCountMap,MetricsMarketCountMap;
    String PropertiesQuery,MetricsQuery;
    String QueueConfigId;
    Integer Month;
    String Sector;
    String PoolQueueId;
    //Anantha 21/07/2017 
//    Map<String,Integer> PropertiesSubMarketRentCounterMap;
//    Map<String,Integer> PropertiesSubMarketVacancyCounterMap;
    //Anantha 24/07/2017
    Map<String,Queue_Exception__c> QueueExceptionMap;
    Map<String,Boolean> CoverageResult;
    Queue_Configuration__c QueueConfig;
    
    List<Survey_Log__c> lSurveyLogList;
    Map<String,Survey_MSA__C> MSAMap;
    Map<String,Survey_Submarket__c> SubMarketMap;
    Map<String,Survey_Sector__c> SectorMap;
    Integer NoOfDaysinQuarter;
    
    public BuildQueue_Batch(Integer NoOfDaysinQuarter,String Sector,Integer Month,String ProcessQuery,String QueueConfigId,Map<String,Integer> PropertiesCountMap,Map<String,Integer> MetricCountMap,Map<String,Integer> PropertiesMarketCountMap,Map<String,Integer> MetricsMarketCountMap/*,Map<String,Integer> PropertiesSubMarketRentCounterMap,Map<String,Integer> PropertiesSubMarketVacancyCounterMap*/){
        lSurveyLogList=new List<Survey_Log__c>();
        this.ProcessQuery=ProcessQuery;
        this.TotalCount=TotalCount;
        this.QueueConfigId=QueueConfigId;
        this.PropertiesCountMap=PropertiesCountMap;
        this.MetricCountMap=MetricCountMap;
        this.PropertiesMarketCountMap=PropertiesMarketCountMap;
        this.MetricsMarketCountMap=MetricsMarketCountMap;
        this.Month=Month;
        this.Sector=Sector;
        MSAMap=new Map<String,Survey_MSA__C>();
        SubMarketMap=new Map<String,Survey_Submarket__c>();
        SectorMap=new Map<String,Survey_Sector__c>();
        
        //Anantha 24/07/2017
        this.QueueExceptionMap=new Map<String,Queue_Exception__c>();
        this.CoverageResult=new Map<String,Boolean>();
        
        //Anantha 21/07/2017 Added
   //     this.PropertiesSubMarketRentCounterMap=PropertiesSubMarketRentCounterMap;
   //     this.PropertiesSubMarketVacancyCounterMap=PropertiesSubMarketVacancyCounterMap;
   		this.NoOfDaysinQuarter=NoOfDaysinQuarter;     
        
    }
    global Database.querylocator start(Database.BatchableContext BC){
       
        System.debug('Counter1=>'+PropertiesCountMap);
        System.debug('Counter2=>'+MetricCountMap);
        PoolQueueId=[select Id from group where name='Pool Queue'].Id;
        for(Survey_Sector__C SurveyRec:[Select Id,Name,Description__c from survey_sector__c]){
            SectorMap.put(SurveyRec.Name, SurveyRec);
        }
        for(Survey_MSA__C MSARec:[Select Id,Name,Foundation_ID__c from Survey_MSA__C where sector__c=:Sector]){
            MSAMap.put(MSARec.Foundation_ID__c, MSARec);
        }
        for(Survey_Submarket__c SubMarketRec:[Select Id,Name,Foundation_ID__c from Survey_Submarket__c where sector__c=:Sector]){
            SubMarketMap.put(SubMarketRec.Foundation_ID__c, SubMarketRec);
        }
        
        //Anantha 24/07/2017 Moved from batchUtils
        for(Queue_Exception__c QueueExceptionRec:[Select Id,Exception_Type__c,Property_Type__c,Rule_Name__c,Rule_Value__c,Target_Name__c from Queue_Exception__c where Queue_Configuration__c=:QueueConfigId]){
            QueueExceptionMap.put(QueueExceptionRec.Property_Type__c+QueueExceptionRec.Exception_Type__c+QueueExceptionRec.Target_Name__c+QueueExceptionRec.Rule_Name__c, QueueExceptionRec);
        }
        system.debug('QueueExceptionMap=>'+QueueExceptionMap);
        QueueConfig=[select Id,Month_1_Market_Coverage__c,Quarter_Market_Coverage__c,Quarter_SubMarket_Coverage__c,Month_2_Market_Coverage__c,Month_1_of_Surveys_per_Submarket__c,Month_2_of_Surveys_per_Submarket__c,Aged_Property_Frequency__c,Aged_Timeframe__c,Survey_Records_With_Data__c,Month_1_Submarket_Coverage__c,Month_2_Submarket_Coverage__c,BatchJobId__c from Queue_Configuration__c where Id=:QueueConfigId][0];
        //Anantha 28/07/2017 RSI-416
        if(QueueConfig.BatchJobId__c==null || QueueConfig.BatchJobId__c==''){
        	Queue_Configuration__c QueueConfig1=new Queue_Configuration__c();
        	QueueConfig1.BatchJobId__c=BC.getJobId();
        	QueueConfig1.Id=QueueConfig.Id;
        	update QueueConfig1;    
        }else{
            Survey_Log__c oLog=new Survey_Log__c();
            oLog.Error_Message__c='There is already a batch that is in progress. Please try after sometime.';
            oLog.Batch_Id__c=BC.getJobId();
            oLog.Batch_Name__c='BuildQueue_Batch';
            oLog.Status__c='Error';
            //Anantha 27/07/2017
            oLog.Sector__c=Sector;
            insert oLog;
            throw new REISException('There is already a batch that is in progress. Please try after sometime.');
        }
        
        
        BuildQueue_getCoverageUtils.getSectorCoverage(Month, Sector, CoverageResult, PropertiesCountMap, MetricCountMap, PropertiesMarketCountMap, MetricsMarketCountMap, MSAMap, SubMarketMap, QueueConfig, QueueExceptionMap/*, PropertiesSubMarketRentCounterMap, PropertiesSubMarketVacancyCounterMap*/);
        system.debug('CoverageResult=>'+CoverageResult);
        return Database.getQueryLocator(ProcessQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('PropertiesCountMap=>'+PropertiesCountMap);
        system.debug('MetricCountMap=>'+MetricCountMap); 
        system.debug('BatchJobIdExe=>'+BC.getJobId());
        try{
           // List<String> a;
           // a.add('Temp');
        	BuildQueue_BatchUtils.CreateQueue(PoolQueueId,Month,scope,SectorMap,MSAMap,SubMarketMap,PropertiesCountMap,MetricCountMap,PropertiesMarketCountMap,MetricsMarketCountMap,BC.getJobId(),QueueConfig,QueueExceptionMap,CoverageResult/*,PropertiesSubMarketRentCounterMap,PropertiesSubMarketVacancyCounterMap*/);    
        }catch(Exception e){
            Survey_Log__c oLog=new Survey_Log__c();
            oLog.Error_Message__c=e.getMessage() + string.valueof(e.getLineNumber()) + e.getStackTraceString();
            oLog.Batch_Id__c=BC.getJobId();
            oLog.Batch_Name__c='BuildQueue_Batch';
            oLog.Status__c='Error';
            //Anantha 27/07/2017
            oLog.Sector__c=Sector;
            String sPropertyIdList;
            for(sObject ScopeRecord:scope){
            	Ops_Pool_Data__c PoolRecord=(Ops_Pool_Data__c)ScopeRecord;
                sPropertyIdList+='\''+PoolRecord.Property_ID__c+'\',';
            }
            oLog.Additional_Information__c=sPropertyIdList.left(32500);
            lSurveyLogList.add(oLog);
            throw e;
        }
      	
    }
    
    global void finish(Database.BatchableContext BC){
        Set<String> StatusSet=new set<String>{'Pending Creation','Pending Assignment'};
        try{
        	 Decimal SurveyDataNeeded=[select Id,Survey_Records_With_Data__c from queue_Configuration__c where Id=:QueueConfigId].Survey_Records_With_Data__c;
       		 system.debug('SurveyDataNeeded=>'+SurveyDataNeeded + '=>'+String.valueOf(Integer.valueOf(SurveyDataNeeded)));
            //Anantha 03/08/2017 added th eorder by
        	 String Query='select Id,(select Id,Queue_Record__c,Survey_Data_Needed__c,status__c from Property_Surveys__r) from queue__c Where Survey_Sector__r.Name=\''+Sector+'\' and status__c IN (\'Pending Creation\') and Batch_Id__c=\''+BC.getJobId()+'\' Order By Priority__c limit '+String.valueOf(Integer.valueOf(SurveyDataNeeded));
        	 system.debug('Query=>'+Query);
             system.debug('BatchJobIdExe=>'+BC.getJobId());
           // List<queue__c> QueueRecList=Database.query(Query);
        //	 system.debug('QueueRec=>'+QueueRecList);
      /*  	 if(QueueRecList.size()>0){
            	List<Property_Survey__c> PropertySurveyList=new List<Property_Survey__c>();
            	for(Queue__c QueueRec: QueueRecList){
                	for(Property_Survey__c PropSurvey: QueueRec.Property_Surveys__r){
                        if(PropSurvey.Status__c=='Pending Creation'){
                        	PropSurvey.Survey_Data_Needed__c=true;
                			PropertySurveyList.add(PropSurvey);    
                        }
                    	    
                	}
            	}
            	update PropertySurveyList;
        	} */
            UpdateSurveyDataNeededBatch SurveyDataBatch=new UpdateSurveyDataNeededBatch(Query,Sector,QueueConfigId);
            Database.executeBatch(SurveyDataBatch, Integer.valueOf(BatchSize_Setting__c.getValues('UpdateSurveyDataNeededBatch').Size__c));
        }catch(Exception e){
            Survey_Log__c oLog=new Survey_Log__c();
            oLog.Error_Message__c=e.getMessage() + string.valueof(e.getLineNumber()) + e.getStackTraceString();
            oLog.Batch_Id__c=BC.getJobId();
            oLog.Batch_Name__c='BuildQueue_Batch';
            oLog.Status__c='Error';
            //Anantha 27/07/2017
            oLog.Sector__c=Sector;
            String sPropertyIdList;
            oLog.Additional_Information__c='Error While updating the Pending Assignment status';
            lSurveyLogList.add(oLog);
       //     throw e;
        }   
       
        if(lSurveyLogList.size()>0){
            insert lSurveyLogList;
        }
 /* Anantha 30/08 Separated it and will be run prior ro Queue Build Process.    */
/*   DeleteQueueBatchClass DelClass=new DeleteQueueBatchClass((String)BC.getJobId(),Sector,QueueConfig.Id);
		Database.executeBatch(DelClass, 200);*/
        UpsertProgressRecord();

        //Anantha 30/08 Moved this from DeleteQueueBatchClass as that will be runindependently.
    /*    Queue_Configuration__c QueueConfig1=new Queue_Configuration__c();
        QueueConfig1.BatchJobId__c=null;
        QueueConfig1.Id=QueueConfigId;
        update QueueConfig1;*/
    }
    
     public void UpsertProgressRecord(){
         system.debug('NoOfDaysinQuarter=>'+NoOfDaysinQuarter);
         String inClause = String.format( '(\'\'{0}\'\')', 
                         new List<String> { String.join( new List<String>(PropertiesCountMap.keySet()) , '\',\'') });
		String sQuery='select Id,M1_Total_Count__c,RecordKey__c,M2_Total_Count__c,M3_Total_Count__c from Survey_Progress__c where RecordKey__c in '+inClause+' and createddate=LAST_N_DAYS:'+String.valueOf(NoOfDaysinQuarter); 
        system.debug('sQuery=>'+sQuery);
         List<Survey_Progress__c> SurveyProgressList=Database.query(sQuery);
       // String LastnDays='LAST_N_DAYS:'+String.valueOf(NoOfDaysinQuarter); 
       // List<Survey_Progress__c> SurveyProgressList=[select Id,M1_Total_Count__c,RecordKey__c,M2_Total_Count__c,M3_Total_Count__c from Survey_Progress__c where RecordKey__c =:PropertiesCountMap.keySet() and createddate=LAST_N_DAYS::NoOfDaysinQuarter]; 
        Map<String,Survey_Progress__c> SurveyProgressMap=new Map<String,Survey_Progress__c>();
        Map<Integer,String> MonthToFieldMap=new Map<Integer,String>{1=>'M1_Total_Count__c',4=>'M1_Total_Count__c',7=>'M1_Total_Count__c',10=>'M1_Total_Count__c',
            														2=>'M2_Total_Count__c',5=>'M2_Total_Count__c',8=>'M2_Total_Count__c',11=>'M2_Total_Count__c',
            														3=>'M3_Total_Count__c',6=>'M3_Total_Count__c',9=>'M3_Total_Count__c',12=>'M3_Total_Count__c'};
        Map<String,String> TotalFieldToCompleteFieldMap=new Map<String,String>{'M1_Total_Count__c'=>'M1_Complete_Count__c','M1_Total_Count__c'=>'M1_Complete_Count__c','M1_Total_Count__c'=>'M1_Complete_Count__c','M1_Total_Count__c'=>'M1_Complete_Count__c',
            														'M2_Total_Count__c'=>'M2_Complete_Count__c','M2_Total_Count__c'=>'M2_Complete_Count__c','M2_Total_Count__c'=>'M2_Complete_Count__c','M2_Total_Count__c'=>'M2_Complete_Count__c',
            														'M3_Total_Count__c'=>'M3_Complete_Count__c','M3_Total_Count__c'=>'M3_Complete_Count__c','M3_Total_Count__c'=>'M3_Complete_Count__c','M3_Total_Count__c'=>'M3_Complete_Count__c'};
        
        List<Survey_Progress__c> NewSurveyProgressList=new List<Survey_Progress__c>();
        for(Survey_Progress__c SurveyProgressRec: SurveyProgressList){
            SurveyProgressMap.put(SurveyProgressRec.RecordKey__c, SurveyProgressRec);
        }
        for(String CounterKey:PropertiesCountMap.keySet()){
            Integer CompletedCountValue=0;
            Integer TotalCountValue=PropertiesCountMap.get(CounterKey);
            if(MetricCountMap.containsKey(CounterKey)){
                CompletedCountValue=MetricCountMap.get(CounterKey);
            }else{
                CompletedCountValue=0;
            }
            
            if(SurveyProgressMap.containsKey(CounterKey)){
                system.debug('MonthToFieldMap.get(Month)=>'+MonthToFieldMap.get(Month));
                system.debug('PropertiesCountMap.get(CounterKey)=>'+PropertiesCountMap.get(CounterKey));
                // Anantha RSI-511. Updated to set to the daily count.
                //Integer ExistingValue=Integer.valueOf(SurveyProgressMap.get(CounterKey).get(MonthToFieldMap.get(Month)));
				//Integer FinalValue=ExistingValue>PropertiesCountMap.get(CounterKey)?ExistingValue:PropertiesCountMap.get(CounterKey);                                                      
                //SurveyProgressMap.get(CounterKey).put(MonthToFieldMap.get(Month),FinalValue);
                
                SurveyProgressMap.get(CounterKey).put(MonthToFieldMap.get(Month),TotalCountValue);
                SurveyProgressMap.get(CounterKey).put(TotalFieldToCompleteFieldMap.get(MonthToFieldMap.get(Month)),CompletedCountValue);
            }else{
                String[] CounterKeyContents=CounterKey.split(';');
                String PropertyType=CounterKeyContents[1]==null?'':CounterKeyContents[1];
                String MSA=MSAMap.get(CounterKeyContents[2])==null?null:MSAMap.get(CounterKeyContents[2]).Id;
                String SubMarket=SubMarketMap.get(CounterKeyContents[3])==null?null:SubMarketMap.get(CounterKeyContents[3]).Id;
                
                Survey_Progress__c SurveyProgress=new Survey_Progress__c(Survey_Sector__c=SectorMap.get(CounterKeyContents[0]).Id,Survey_MSA__c=MSA,
                                                                         Survey_Submarket__c=SubMarket,Property_Type__c=PropertyType,RecordKey__c=CounterKey);
                SurveyProgress.put(MonthToFieldMap.get(Month),TotalCountValue);
                SurveyProgress.put(TotalFieldToCompleteFieldMap.get(MonthToFieldMap.get(Month)),CompletedCountValue);
                NewSurveyProgressList.add(SurveyProgress);
            }
        }
         system.debug('SurveyProgressMap=>'+SurveyProgressMap);
         update SurveyProgressMap.values();
         insert NewSurveyProgressList;
    }
    
}