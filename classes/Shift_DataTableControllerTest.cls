@isTest
private class Shift_DataTableControllerTest {
  
  @isTest static void test_method_one() {
    Profile p = [select id from profile where name='Standard User'];
        User u = new User(
      alias = 'testUsr',
      email='test@testUsr.com',
      emailencodingkey='UTF-8', 
      lastname='Testing', 
      languagelocalekey='en_US',
      localesidkey='en_US', 
      profileid = p.Id,
      timezonesidkey='America/Los_Angeles',
      username='test@testUsr.com'
        );

        System.runAs(u) {
           Shift_DataTableController con = new Shift_DataTableController();
           System.assertEquals(con.userLocale, 'en_US', 'Invalid user profile assigned');
        }
  }
  
}