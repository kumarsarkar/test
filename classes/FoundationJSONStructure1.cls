public class FoundationJSONStructure1{
    Public PropertyBuilding building;
    Public PropertySurveyData surveyData;
    Public PropertySectorSurveyData sectorSurveyData;
    Public PropertySectorSpaceData sectorSpaceData;
    Public PropertyNewConstructionSurvey newConstructionSurvey;
    Public PropertySectorSurveySpaceData sectorSurveySpaceData;
    Public PropertyAddress address;
    Public PropertyCont[] contact;
    Public PropertyInsur[] insurance;
    Public PropertyAmen[] amenities;
    Public PropertyLeasingIncentive[] leasingIncentives;
    Public PropertyHeatingSource[] heatingSources;
    Public PropertyUnitDesignation[] unitDesignations;
    Public PropertyRentBy[] rentBys;
    Public PropertyHUD[] hUDs;
    Public PropertyNote[] notes;
    Public PropertyTenant[] tenants;
    Public string survey;
    Public string surveySpace;
    
    public class PropertyBuilding{
        Public PropertyBuildingBld bld;
    }
    public class SurveyCodeOutType{}
    public class PropertySurveyData{
        Public String contactOid;//Required
        Public String surveyDate;//Required
        Public String surveyQuarter;
        Public String surveySource;//Required
        Public String website;//Required
        Public Boolean isTermsGrid;
        Public String surveyor;//Required
        Public String surveyCodeOut;
        Public Boolean rentalRateRefused;
        Public Boolean totalVacancyRefused;
        Public Boolean missingInfo;
        Public Boolean contactRefusedSurvey;
        Public Boolean allInfoNegotiable;
        Public String qcNote;
        Public Boolean isNewConstruction;
        Public Integer incomplete;
        Public String propertyNote;
        Public Boolean hasRenovationNote;
        Public Boolean forSaleOrSold;
        Public String saleDate;
        Public Decimal salePrice;
        Public String callStatus;
    }
    public class SalePriceType{}
    
    Public Class PropertySectorSurveyData {
        Public PropertySectorSurveyDataStoSurveyData stoSurveyData;
        Public PropertySectorSurveyDataSthSurveyData sthSurveyData;
        Public PropertySectorSurveyDataSnrSurveyData snrSurveyData;
        Public PropertySectorSurveyDataRetSurveyData retSurveyData;
        Public PropertySectorSurveyDataIndSurveyData indSurveyData;
        Public PropertySectorSurveyDataOffSurveyData offSurveyData;
        Public PropertySectorSurveyDataAptSurveyData aptSurveyData;
    }
    Public Class PropertySectorSpaceData {
        Public PropertySectorSpaceDataStoSpaceData[] stoSpaceData;
        Public PropertySectorSpaceDataSthSpaceData[] sthSpaceData;
        Public PropertySectorSpaceDataSnrSpaceData[] snrSpaceData;
        Public PropertySectorSpaceDataRetSpaceData[] retSpaceData;
        Public PropertySectorSpaceDataIndSpaceData[] indSpaceData;
        Public PropertySectorSpaceDataOffSpaceData[] offSpaceData;
        Public PropertySectorSpaceDataAptSpaceData[] aptSpaceData;
    }
    Public Class PropertyNewConstructionSurvey{
        Public PropertyNewConstructionSurveyNC nc;
    }
    Public Class PropertySectorSurveySpaceData{
        Public PropertySectorSurveySpaceDataStoSurveySpaceData[] stoSurveySpaceData;
        Public PropertySectorSurveySpaceDataSthSurveySpaceData[] sthSurveySpaceData;
        Public PropertySectorSurveySpaceDataSnrSurveySpaceData[] snrSurveySpaceData;
        Public PropertySectorSurveySpaceDataRetSurveySpaceData[] retSurveySpaceData;
        Public PropertySectorSurveySpaceDataIndSurveySpaceData[] indSurveySpaceData;
        Public PropertySectorSurveySpaceDataOffSurveySpaceData[] offSurveySpaceData;
        Public PropertySectorSurveySpaceDataAptSurveySpaceData[] aptSurveySpaceData;
    }
    Public Class PropertyAddress{
        Public PropertyAddressAdd add;
    }
    Public Class PropertyCont{
        Public String contactOid;//required needs to be generated on sf side for new contacts
        Public String title;
        Public String firstName;
        Public String lastName;
        Public String workPhone;
        Public String ext;
        Public String mobilePhone;
        Public String miscPhone;
        Public String fax;
        Public String address;
        Public String eMail;
        Public String webSite;
        Public String note;
        Public String source;
        Public String contactSource;
        Public String name;
        Public String contactDate;
        Public Integer doNotCall;
        Public String jobRoleOid;
        Public String jobRole;
        Public String companyOid;//null for new company, required for existing company being associated with new contact
        Public String companyName;
        Public Integer companyDoNotCall;
        Public String companyDoNotCallDate;
        Public String companyMainPhone;
        Public String companyWebSite;
    }
    Public Class PropertyInsur{
        Public Integer insuranceOid;//Required
        Public String insurance;
        Public String selected;
        Public Integer insurancePropertyOid;
        Public Boolean insurancePropertyOidSpecified;
    }
    Public Class PropertyAmen{
        Public Integer amenityPropertyOid;//Required
        Public Integer amenityid;
        Public Boolean hasQuantity;
        Public String description;
        Public Integer quantity;
        Public String selected;
        Public String amenityType;
        Public String lastUpdate;//Anantha 17/08/2017
    }
    Public Class PropertyLeasingIncentive{
        Public String lIOid;
        Public String lIDescription;
       //Anantha 30/08 Commented as it is no longer required.
       //  Public String selected;//added after discussion with Samie.
    }
    Public Class PropertyHeatingSource{
        Public Integer oID;
        Public String heatSourcesId;
        Public String name;
        Public String selected;
    }
    Public Class PropertyUnitDesignation{
        Public Integer oID;
        Public String unitDesignations;
        Public String name;
        Public String selected;
    }
    Public Class PropertyRentBy{
        Public Integer recID;
        Public Integer rentBysId;
        Public String description;
        Public String selected;
    }
    Public Class PropertyHUD{
        Public String oid;
        Public String id;
    }
    Public Class PropertyLeasingIncentives{
        Public PropertyLeasingIncentivesLeasingIncentive leasingIncentive;
    }
    Public Class PropertyNote{
        Public String noteTyp;//required
        Public String createDate;
        Public Integer month;//required for conversion notes
        Public Integer year;//required for conversion notes
        Public Boolean isExpansion;
        Public String notes;//Required
    }
    Public Class PropertyTenant{
        Public String tenantPropertyOid;
        Public String tenantOid;//Required
        Public String name;
        Public String size;
        Public Boolean groundLease;
        Public String ownedLeased;
        Public Boolean anch;
        Public Boolean outparcel;
        Public String tenantType;
        Public Boolean IsVacant;
    }
    Public Class PropertyBuildingBld{
        Public String propertyOid;//required
        Public String propertyId;
        Public String name;
        Public String sector;
        Public String sectorTitle;
        Public Boolean competative;
        Public String note;
        Public String enterDate;
        Public String lastUpdate;
        Public String printDate;
        Public String classField;
        Public String website;
        Public Integer yrBuilt;
        Public Integer monthBuilt;
        Public Integer estComYrBuilt;
        Public Integer estComMonthBuilt;
        Public String estimateComStatus;
        Public Integer conStartYr;
        Public Integer conStartMonth;
        Public String conStatus;
        Public String owner;
        Public String developer;
        Public String selectCode;
        Public String selectCodeDescription;
        Public Boolean finalNCStamp;
        Public String reitCode;
        Public String reit;
        Public String propertyTypeCode;
        Public String propertyType;
        Public String propStatus;
        Public String propertySubType;
        Public String portfolio;
        Public PortfolioType portfolioty;
        Public PropertyBuildingBldStoDetails stoDetails;
        Public PropertyBuildingBldSthDetails sthDetails;
        Public PropertyBuildingBldSnrDetails snrDetails;
        Public PropertyBuildingBldRetDetails retDetails;
        Public PropertyBuildingBldIndDetails indDetails;
        Public PropertyBuildingBldOffDetails offDetails;
        Public PropertyBuildingBldAptDetails aptDetails;
    }
    public class PropertySubTypeType{}
    public class PortfolioType{}
    
    Public Class PropertySectorSurveyDataStoSurveyData{
        Public Decimal totalCCAvailable;
        Public Decimal totalNCCAvailable;
        Public Integer moveIn;
        Public Integer moveOut;
        Public Decimal totalAvailable;
        Public Decimal freeRent;
        Public Integer unitsReceivedFreeRent;
        Public String qcNote;
        Public Decimal rentForgone;
        Public Integer amt;
    }
    public class QcNoteType{}
    public class RentForgoneType{}
    
    Public Class PropertySectorSurveyDataSthSurveyData{
        Public Decimal averageRent;
        Public String recallDate;
        Public Decimal newTentants;
        Public Decimal percentPreleased;
   //     Public NewTentantsType newTentantsT;
        Public Integer percentStudents;
   //     Public PercentPreleasedType percentPreleasedT;
        Public Decimal monthsFreeRent;
        Public Decimal percentRentUtilities;
        Public Integer totalVacantBeds;
        Public Integer totalVacantUnits;
        Public Decimal leaseTerm;
        Public Decimal freeRentPercent;
        Public Decimal baseRent;
        Public Decimal reducedLeaseTerm;
        Public Decimal reducedRentDiscount;
        Public Decimal reducedFreeRentPercent;
        /*Public BaseRentType baseRentTy;
        Public ReducedLeaseTermType reducedLeaseTermTy;
        Public ReducedRentDiscountType reducedRentDiscountTy;
        Public ReducedFreeRentPercentType reducedFreeRentPercentTy;*/
        Public Boolean prelease;
        Public String rentType;
    }
    public class NewTentantsType{}
    public class PercentPreleasedType{}
    public class BaseRentType{}
    public class ReducedLeaseTermType{}
    public class ReducedRentDiscountType{}
    public class ReducedFreeRentPercentType{}
    
    Public Class PropertySectorSurveyDataSnrSurveyData{
        Public Integer totalAvailable;
        Public String annualRentSetDayMonth;
        Public Integer entryFeeLow;
        Public Integer entryFeeHigh;
        Public Decimal entryFeeAvg;
        Public String insuranceAccepted;
        Public Decimal expectedPercentIncrease;
        Public Integer hold;
        Public Integer available;
    }
    public class InsuranceAcceptedType{}
    
    public class AverageRentType{}
    public class GroundAverageRentType{}
    public class GroundRentHighType{}
    public class GroundRentLowType{}
    public class GroundLeaseTermType{}
    
    public class OutPclAverageRentType{}
    public class OutPclRentLowType{}
    public class RentLowType{}
    public class RentHighType{}
    public class AnchorSizeType{}
    public class NonAnchorSizeType{}
    public class LeaseTermType{}
    public class AvgFlagType{}
    public class RentTermTYpe{}
    public class OutPclRentHighType{}
    public class RenewalCommissionAmountType{}
    public class AnchorCommissionAmountType{}
    public class NonAnchorCommissionAmountType{}
    Public Class EstSizeType{}
    Public Class PctRentType{}
    Public Class FoodAvailType{}
    Public Class PassThruType{}
    Public Class FoodLeaseTermType{}
    Public Class OutPclLeaseTermType{}
    Public Class FoodRentLowType{}
    Public Class FoodRentHighType{}
    Public Class FoodRentAvgType{}
    Public Class CrdAnchType{}
    Public Class PtCodeType{}
    
    Public Class PropertySectorSurveyDataRetSurveyData{
        Public Integer totalSize;
        Public Decimal averageRent;
        Public Decimal operatingExpense;
        Public Decimal reTax;
        Public Decimal freeRentAnchor;
        Public Decimal freeRentNonAnchor;
        Public Decimal anchorCommissionPercent;
        Public Integer amount;
        Public Decimal groundAvail;
        Public String tiCode;
        
        Public Decimal groundAverageRent;
        Public Decimal groundRentHigh;
        Public Decimal groundRentLow;
        Public Decimal groundLeaseTerm;        
        Public Decimal anchorLeaseTerm;
        Public Decimal nonAnchorLeaseTerm;
        Public Decimal anchRentAvg;
        Public Decimal anchRentHigh;
        Public Decimal anchRentLow;
        Public Decimal nonAnchRentAvg;
        Public Decimal nonAnchRentHigh;
        Public Decimal nonAnchRentLow;
        Public Decimal outPclAvail;
        Public Decimal outPclAverageRent;
        Public Decimal outPclRentLow;
        Public Decimal rentLow;
        Public Decimal rentHigh;
        Public Integer size;
        
        Public Decimal anchorSize;
        Public Decimal nonAnchorSize;
        Public Decimal leaseTerm;
        
        Public Decimal tiAnchor;
        Public Decimal tiNonAnchor;
        Public Decimal contractRentNonAnchor;
        Public Decimal contractRentAnchor;
        
        Public String avgFlag;
        Public String rentTerm;
        Public Integer dAvgFlag;
        Public Integer sAvgFlag;
        Public Decimal outPclRentHigh;
        
        Public Decimal renewalCommissionAmount;
        Public Decimal anchorCommissionAmount;
        Public Decimal nonAnchorCommissionAmount;
        Public String avgNonAnchFlag;
        Public String avgAnchFlag;
        Public String estSize;
        Public Decimal availNonAnchor;
        Public Decimal availAnchor;
        Public Decimal nonAnchorCommissionPercent;
        Public Decimal pctRent;
        Public String rentBasis;
        Public Decimal commonAreaMaint;
        Public Decimal foodAvail;
        Public Decimal passThru;
        Public Decimal renewalCommissionPercent;
        Public Decimal foodLeaseTerm;
        
        Public Decimal outPclLeaseTerm;
        Public Decimal foodRentLow;
        Public Decimal foodRentHigh;
        Public Decimal foodRentAvg;
        Public Decimal crdAnch;
        Public Decimal ptCode;
    }
    Public Class LossFactorType{}
    Public Class RawTiType{}
    Public Class PropertySectorSurveyDataIndSurveyData{
        Public Decimal lossFactor;
        Public Boolean passThrough;
        Public Boolean escalator;
        Public Boolean elec;
        Public Boolean janitorial;
        Public Decimal lease;
        Public Decimal rawTi;
        Public Decimal newTi;
        Public Decimal renewal;
        Public Decimal months;
        Public Decimal contactRent;
        Public Decimal commissionNew;
        Public Decimal commissionRenew;
        Public String qcNote;
        Public Boolean isSubletOnly;
    }
    
    Public Class PropertySectorSurveyDataOffSurveyData{
        Public Boolean elec;
        Public Boolean janitorial;
        Public Decimal lease;
        Public Decimal rawTi;
        Public Decimal newTi;
        Public Decimal renewal;
        Public Decimal months;
        Public Decimal contactRent;
        Public Decimal commissionNew;
        Public Decimal commissionRenew;
        Public Decimal lossFactor;
        Public Boolean passThrough;
        Public Boolean escalator;
        Public Boolean isSubletOnly;
    }
    
    
    Public Class PropertySectorSurveyDataAptSurveyData{
        Public Decimal freeRent;
        Public Boolean isAffordableHousingSurvey;
        Public Decimal lihtcFreeRent;
        Public Decimal percentPreleased;
        Public Integer totalVacantUnits;
    }
    Public Class PropertySectorSpaceDataStoSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Decimal length;
        Public Decimal width;
        Public Decimal height;
        Public Integer quantity;
        Public String dimensions;
        Public Boolean climateControlled;
        Public Decimal sqft;
        Public Integer floor;
        Public Boolean rentable;
        Public Boolean boatRV;
        Public Boolean elevatorAccess;
        Public Boolean driveUpAccess;
        Public Boolean inside;
        Public String estimateStatus;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public String spaceType;
        
    }
    Public Class PropertySectorSpaceDataSthSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Integer totalUnits;
        Public Integer totalBeds;
        Public String spaceType;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public String estimateStatus;
        Public Integer sQFTLow;
        Public Integer sQFTHigh;
        Public Decimal sQFTAvg;
        Public String bathParity;
        Public Integer brSQFTLow;
        Public Integer brSQFTHigh;
        Public Decimal brSQFTAvg;
        
    }
    
    Public Class PropertySectorSpaceDataSnrSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Integer units;
        Public Integer sQFTHigh;
        Public Integer sQFTLow;
        Public Decimal sQFTAvg;
        Public String estimateStatus;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public Boolean hasUnits;
        Public String spaceType;
        
    }
    Public Class StringnonAnchorSizeType{}
    Public Class PropertySectorSpaceDataRetSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String estimateStatus;
        Public String propertyStatus;
        Public String description;
        Public Integer total;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public Boolean rentable;
        Public Boolean isOutParcel;
        Public String spaceType;
        Public Decimal anchorSize;
        Public Decimal nonAnchorSize;
        Public Decimal yearBuilt;
        Public Decimal monthBuilt;
        Public Decimal groundBreakYear;
        Public Decimal groundBreakMonth;
        Public Integer outParcel;
        Public Integer foodCourt;
    }
    Public Class PropertySectorSpaceDataIndSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Integer total;
        Public Boolean rentable;
        Public String estimateStatus;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public String spaceType;
    }
    Public Class PropertySectorSpaceDataOffSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Integer total;
        Public Boolean rentable;
        Public String estimateStatus;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String groundBreakDate;
        Public String startDate;
        Public String spaceType;
    }
    Public Class PropertySectorSpaceDataAptSpaceData{
        Public String spaceOid;//required – create guid for new space
        Public String description;
        Public Integer compTotal;
        Public Integer nonCompTotal;
        Public Integer units;
        Public Integer sqFt;
        Public String spaceType;
        Public Boolean expansion;
        Public Boolean verified;
        Public String expansionDate;
        Public String startDate;
        Public String estimateStatus;
        Public Integer sQFTLow;
        Public Integer sQFTHigh;
        Public Decimal sQFTAvg;
        Public Integer ami30;
        Public Integer ami40;
        Public Integer ami50;
        Public Integer ami60;
        Public Integer secEightUnits;
        Public Integer totalUnits;
        Public Integer ami70;
        Public Integer amiNonConventional;
        Public Decimal utilityCap;
        Public Boolean hasAmi30;
        Public Boolean hasAmi40;
        Public Boolean hasAmi50;
        Public Boolean hasAmi60;
        Public Boolean hasAmi70;
        Public Boolean hasAmiNonConventional;
        Public Integer unknownAmi;
        Public Boolean hasUnknownAmi;
        Public Boolean hasSecEightUnits;
        Public Boolean hasUtilityCap;
        Public Integer lihtcSqftLow;
        Public Integer lihtcSqftHigh;
        Public Decimal lihtcSQFTAvg;
        Public Integer secEightSqftLow;
        Public Integer secEightSqftHigh;
        Public Decimal secEightSQFTAvg;
    }
    Public Class PropertyNewConstructionSurveyNC{
        Public String nCOid;
        Public Integer completeMonth;
        Public Integer completeYear;
        Public Integer startMonth;
        Public Integer startYear;
        Public String startEstimate;
        Public String completeEstimate;
        Public Decimal percentPreleased;
        Public String rentType;
        Public Decimal unitsAvailable;
        Public String propertyStatus;
        Public String proprtyStatusDesc;
    }
    
    Public Class PropertySectorSurveySpaceDataStoSurveySpaceData{
        Public String spaceOid;//Required
        Public Integer directAvail;
        Public Decimal directHigh;
        Public Decimal directLow;
        Public Decimal directAvg;
        Public String dAvgFlag;
        Public Decimal vacancyRate;
        Public Decimal streetRent;
        Public Integer vacantUnits;
        Public Integer mode;
        Public Integer quantity;
    }
    Public Class PropertySectorSurveySpaceDataSthSurveySpaceData{
        Public String spaceOid;
        Public Integer vacantUnits;
        Public Integer vacantBeds;
        Public Decimal currentRentLow;
        Public Decimal currentRentHigh;
        Public Decimal currentRentAvg;
        Public Integer currentPeriodIndex;
        Public Integer currentPeriodYear;
        Public Decimal futureRentLow;
        Public Decimal futureRentHigh;
        Public Decimal futureRentAvg;
        Public Integer futurePeriodIndex;
        Public Integer futurePeriodYear;
        Public String currentRentAvgFlag;
        Public String futureRentAvgFlag;
    }
    Public Class PropertySectorSurveySpaceDataSnrSurveySpaceData{
        Public String spaceOid;//Required
        Public Integer avail;
        Public String avgFlag;
        Public Integer marketRentHigh;
        Public Integer marketRentLow;
        Public Decimal marketRentAvg;
        Public Integer baseRentLow;
        Public Integer baseRentHigh;
        Public Decimal baseRentAvg;
        Public String baseAvgFlag;
        Public Decimal totalAvg;
        Public Integer hold;
    }
    Public Class PropertySectorSurveySpaceDataRetSurveySpaceData{
        Public String spaceOid;//Required
        Public Decimal avail;
        Public Decimal rentLow;
        Public Decimal rentHigh;
        Public Decimal averageRent;
        Public Integer size;
        Public Decimal leaseTerm;
        Public Decimal ti;
        Public String tiCode;
        Public String avgFlag;
        Public String rentTerm;
        Public Integer dAvgFlag;
        Public Integer sAvgFlag;
    }
    Public Class PropertySectorSurveySpaceDataIndSurveySpaceData{
        Public String spaceOid;//Required
        Public Integer directAvail;
        Public Integer directContig;
        Public Decimal directLow;
        Public Decimal directHigh;
        Public Decimal directAvg;
        Public Integer subletAvail;
        Public Decimal subletLow;
        Public Decimal subletHigh;
        Public Decimal subletAvg;
        Public Decimal gross;
        Public Decimal operating;
        Public Decimal taxes;
        Public String rentTerm;
        Public String dAvgFlag;
        Public String sAvgFlag;
        Public Decimal directVacancyRate;
        Public Integer subletVacancyRate;
    }
    Public Class PropertySectorSurveySpaceDataOffSurveySpaceData{
        Public String spaceOid;//Required
        Public Decimal directAvail;
        Public Decimal directContig;
        Public Decimal directLow;
        Public Decimal directHigh;
        Public Decimal directAvg;
        Public Decimal subletAvail;
        Public Decimal subletLow;
        Public Decimal subletHigh;
        Public Decimal subletAvg;
        Public Decimal gross;
        Public Decimal operating;
        Public Decimal taxes;
        Public String rentTerm;
        Public String dAvgFlag;
        Public String sAvgFlag;
        Public Decimal directVacancyRate;
        Public Decimal subletVacancyRate;
    }
    Public Class PropertySectorSurveySpaceDataAptSurveySpaceData{
        Public String spaceOid;//Required
        Public Integer units;
        Public Decimal size;
        Public Integer avail;
        Public String avgFlag;
        Public Decimal leaseTerm;
        Public Integer months;
        Public Decimal discount;
        Public Decimal averageRent;
        Public Integer vacant;
        Public Decimal rentLow;
        Public Decimal rentHigh;
        Public Decimal sec8RentAvg;
        Public Decimal ami30RentAvg;
        Public Decimal ami40RentAvg;
        Public Decimal ami50RentAvg;
        Public Decimal ami60RentAvg;
        Public Decimal ami70RentAvg;
        Public Decimal amiNonConventionalRentAvg;
        Public Integer sec8Vac;
        Public Integer ami30Vac;
        Public Integer ami40Vac;
        Public Integer ami50Vac;
        Public Integer ami60Vac;
        Public Integer ami70Vac;
        Public Integer totalAmiVac;
        Public Integer amiNonConventionalVac;
        Public Decimal leaseTermLihtc;
        Public Decimal discountLihtc;
        Public Decimal unknownAmiRentAvg;
        Public Integer unknownAmiVac;
    }
    Public Class PropertyAddressAdd{
        Public String addressOid;//required
        Public String streetAddress;
        Public String city;
        Public String county;
        Public String zip;
        Public Decimal latitude;
        Public Decimal longitude;
        Public String mSA;
        Public String subName;
        Public String state;
        Public Integer subId;
        Public Boolean subIdSpecified;
        Public String timeZone;
        Public String betaAddress;
        Public String affordableMsa;
        Public String affordableSubName;
        Public Integer affordableSubId;
        Public Boolean affordableSubIdSpecified;
    }
    Public Class PropertyLeasingIncentivesLeasingIncentive{
        Public String lIOid;
        Public String lIDescription;
    }
    Public Class PropertyBuildingBldStoDetails{
        Public Integer floors;
        Public Integer bldgs;
        Public Integer climateControlUnits;
        Public Integer nonClimateControlUnits;
        Public Integer totalUnits;
        Public Integer totalBoatRV;
        Public Decimal acres;
        Public Integer gLA;
    }
    Public Class PropertyBuildingBldSthDetails{
        Public Integer totalSize;
        Public Integer bldgs;
        Public String nonPurposeBuilt;
        Public String onCampus;
        Public Integer totalUnits;
        Public Integer floors;
        Public Integer totalBedCapacity;
        Public Integer beds;
        Public String totalNonComp;
        Public String totalComp;
        Public String rentType;
        Public String universityName;
        Public String universityOwned;
        Public PropertyBuildingBldSthDetailsPT[] propertyTypes;
    }
    Public Class PropertyBuildingBldSnrDetails{
        Public Integer floors;
        Public Integer bldgs;
        Public Integer totalUnits;
        Public Integer totalLicensedUnits;
    }
    Public Class ShoppingCenterPhoneType{}
    Public Class PropertyBuildingBldRetDetails{
        Public Integer superID;
        Public String shoppingCenterPhone;
        Public Integer totalSize;
        Public Boolean isEnclosed;
    }
    
    Public Class PropertyBuildingBldIndDetails{
        Public Integer floors;
        Public Integer bldgs;
        Public Boolean typeValidated;
        Public String tenancy;
    }
    
    Public Class PropertyBuildingBldOffDetails{
        Public Integer floors;
        Public Integer bldgs;
        Public String tenancy;
    }
    Public Class PricingSystemCodeType{}
    Public Class PricingSystemType{}
    Public Class PropertyBuildingBldAptDetails{
        Public Decimal avgSize;
        Public Integer nonCompUnits;
        Public Integer floors;
        Public Integer totalUnits;
        Public String pricingSystemCode;
        Public String pricingSystem;
        Public Integer bldgs;
        Public String unitMixEstimated;
        Public Boolean hasAffordableUnits;
        Public String resType;
        Public Integer taxCreditYearPlacedInService;
        Public Integer taxCreditComplianceYearsRemaining;
        Public String contractStartDate;
        Public String contractEndDate;
        Public String projectOrigin;
        //Anantha 11/07 Added new fields as per samie confirmarion
        public Boolean hasCommercialSpace;
       	Public Decimal minimumEligibleIncome;
        Public Decimal maximumEligibleIncome;
        Public Boolean propertyHasWaitList;
        Public Integer waitListLength;
        Public Integer totalNonCompUnits;

    }
    
    Public Class PropertyBuildingBldSthDetailsPT{
        Public String code;
        Public String shortDesc;
        Public String myType;
    }
}