/*
    @Name: Shift_ActvityPreview_Test
    @Description: Test method for the Shift_AccountActivityPreview, Shift_ActivityPreview_Component.component, Shift_ActivityPreview_Controller.cls
    @Dependancies: Shift_AccountActivityPreview.page --> This page is used for unit testing, should be included by default in package
    @Version: 1.0.0 
    
    === VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial
*/

@isTest
public class Shift_ActvityPreview_Test {

	public static testMethod void testActivtyPreview() {
		List<Event> eventList = new List<Event>();
		List<Task> taskList = new List<Task>();
		
	    //Query if custom customs are already created or not (avoid duplicate values due to seeAllData)
		Integer customSettingsCount = [SELECT count() FROM Shift_ActivitiesTableSetting__c];
		//If there are existing custom settings
		if (customSettingsCount == 0) {		
			//Create & insert custom settings
			Shift_ActivitiesTableSetting__c customSettings = new Shift_ActivitiesTableSetting__c (
				Max_Events__c = 500, 
				Max_Tasks__c = 500
			);
			insert customSettings;
		}			
		
		Account tempAcc = new Account( 
			Name = 'TestAcc' 
		);
		insert tempAcc; 
	
		//Create & Insert 50 tasks/events		
		for (Integer i=0; i<50; i++) {
			
			Event event = new Event(
				WhatId = tempAcc.Id,
				StartDateTime = Date.Today(),
				EndDateTime = Date.Today()
			);
			eventList.add(event);
			
			Task task = new Task(
				WhatId = tempAcc.Id,
				Subject = 'TEST',
				Status = 'Completed',
				Priority = 'Normal'
			);
			taskList.add(task);
						
		} 
		insert eventList; 
		insert taskList;		
		
		//Set the current page
		PageReference pageRef = page.Shift_AccountActivityPreview;
		Test.SetCurrentPageReference(pageRef);
		pageRef.getParameters().put('id', tempAcc.Id);
		
		//Instiatiate the controller for successful operation
		Shift_ActivityPreview_Controller controller = new Shift_ActivityPreview_Controller();

		//Call controller methods 
		controller.init(); 
		
		List<SelectOption> ObjectSelections = controller.getObjectSelections();	
		
	}

}