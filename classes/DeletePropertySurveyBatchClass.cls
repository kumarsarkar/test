Global class DeletePropertySurveyBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
     Map<String,Queue__c> UpdateQueueMap;
    public DeletePropertySurveyBatchClass(){
         UpdateQueueMap=new Map<String,Queue__c>();
    }
	 
    global Database.querylocator start(Database.BatchableContext BC){
       //String ProcessQuery='select queue_record__r.Owner.Name,OPSPoolId__c,Status__c,Select_Code_New__c,Select_Code__c,Survey_Completed__c,Rent_Captured__c,Vacancy_Captured__c,Name,Id,queue_record__r.ownerId,Survey_Sector__r.Name,Survey_MSA__r.foundation_Id__c,Survey_submarket__r.foundation_id__c,Non_Anchor_Average_Rent_New__c,Direct_Available_New__c,Direct_Average_Rent_New__c,Anchor_Lease_Term_New__c,Sublet_Available_New__c,Anchor_Average_Rent_New__c,Total_Anchor_Available_New__c,Total_Non_Anchor_Available_New__c,Aged_Property__c,CAM__c,Survey_Data_Can_Be_Purged__c,Survey_Source__c,Pre_Leased_Percentage_New__c,Pre_Leased__c,Real_Estate_Taxes_New__c,Rent_By__c,Tax_Credit_Year_Placed_Into_Service__c,TI_Allowance_New_New__c,TI_Allowance_Renew_New__c,Current_Available__c,Wait_List_Length__c,Property_Id__c,Property_Type__c,Survey_Sector__c,Contract_Rent_Discount_Anchor_New__c,Survey_Submarket__c,Survey_MSA__c,Commission_New_New__c,Commission_Renew_New__c,Contract_Rent_Discount_New__c,Contract_Rent_Discount_NonAnchor_New__c,Entry_Fee_Average_New__c,Free_Rent_New__c,Free_Rent_Anchor_New__c,Free_Rent_Non_Anchor_New__c,Has_Wait_List__c,Insurance__c,Expected_Increase__c,Rent_Utilities_New__c,Students_New__c,Opex__c,(Select Id,Type__c,Average_Rent_New__c,Lease_Term_New__c,Leasing_Incentives__c,Unit_Available_New__c,High_Rent_New__c,Low_Rent_New__c,Total_Available__c,Vacant_Beds_New__c,Current_Available__c from Survey_Floor_Plans1__r),Surveyor__c,queue_record__c,queue_record__r.No_Of_Surveys__c,(Select Id from Survey_Amenities__r),(Select Id,contact__r.Salesforce_Created__c,contact__c,contact__r.REIS_Company__c,contact__r.REIS_Company__r.Salesforce_Created__c from Survey_Contacts__r),(Select Id,Reis_Tenant__r.salesforce_created__c,REIS_Tenant__c from Tenants__r) from property_Survey__c where (status__c=\'Ready for Publish\' OR status__c=\'Pending Creation\') and Survey_Data_Can_Be_Purged__c = true';
         String ProcessQuery='select queue_record__r.Owner.Name,Id,queue_record__r.ownerId,(Select Id from Survey_Floor_Plans1__r),queue_record__c,queue_record__r.No_Of_Surveys__c,(Select Id from Survey_Amenities__r),(Select Id,contact__r.Salesforce_Created__c,contact__c,contact__r.REIS_Company__c,contact__r.REIS_Company__r.Salesforce_Created__c from Survey_Contacts__r),(Select Id,Reis_Tenant__r.salesforce_created__c,REIS_Tenant__c from Tenants__r) from property_Survey__c where  Survey_Data_Can_Be_Purged__c=true';
        system.debug('ProcessQuery=>'+ProcessQuery);
        return Database.getquerylocator(ProcessQuery);
    }
    
     global void execute(Database.BatchableContext BC, List<Property_Survey__c> scope){
         system.debug('scopesize=>'+scope.size());
         
         if(scope.size() > 0){
         //Anantha 01/09/2017 Commented as part of RSI-502. Create Metric should be called when the Survey is submitted.
        // createMetricRecords.createMetric(scope);
         
         Set<Queue__c> DeleteQueueSet=new Set<Queue__c>(); 
     //  Map<String,Queue__c> UpdateQueueMap=new Map<String,Queue__c>();
         List<Survey_Space_Type__c> SurveyFloorPlanList=new List<Survey_Space_Type__c>();
         List<Survey_Amenities__c> SurveyAmenityList=new List<Survey_Amenities__c>();
         List<Survey_Contact__c> SurveyContactList=new List<Survey_Contact__c>();
         Map<String,REIS_Contact__c> REISContactMap=new Map<String,REIS_Contact__c>(); 
         Map<String,company__c> CompanyMap=new Map<String,company__c>();
         Map<String,REIS_Tenant__c> REISTenantMap=new Map<String,REIS_Tenant__c>();
         List<Tenants__c> Tenants = new List<Tenants__c>();
         
          for(Property_Survey__c PropSurvey:scope){
             SurveyFloorPlanList.addAll(PropSurvey.Survey_Floor_Plans1__r);
             SurveyAmenityList.addAll(PropSurvey.Survey_Amenities__r);
             SurveyContactList.addAll(PropSurvey.Survey_Contacts__r);
              for(Survey_Contact__c SurveyContact:PropSurvey.Survey_Contacts__r){
                  if(SurveyContact.Contact__r.salesforce_created__c){
                      //REISContactList.add(new REIS_Contact__c(Id=SurveyContact.Contact__c,salesforce_created__c=false));
                      REISContactMap.put(SurveyContact.Contact__c,new REIS_Contact__c(Id=SurveyContact.Contact__c,salesforce_created__c=false));
                  }
                  if(SurveyContact.Contact__r.REIS_Company__r.salesforce_created__c){
                      //CompanyList.add(new company__c(Id=SurveyContact.Contact__r.REIS_Company__c,salesforce_created__c=false));
                      CompanyMap.put(SurveyContact.Contact__r.REIS_Company__c,new company__c(Id=SurveyContact.Contact__r.REIS_Company__c,salesforce_created__c=false));
                  }
              }
             //SurveyCareTypes.addAll(PropSurvey.Survey_Senior_Housing_Care_Types__r);
             Tenants.addAll(PropSurvey.Tenants__r);
              for(Tenants__c Tenant:PropSurvey.Tenants__r){
                  if(Tenant.REIS_Tenant__r.salesforce_created__c){
                    // REISTenantList.add(new REIS_Tenant__c(Id=Tenant.REIS_Tenant__c,salesforce_created__c=false));
                     REISTenantMap.put(Tenant.REIS_Tenant__c,new REIS_Tenant__c(Id=Tenant.REIS_Tenant__c,salesforce_created__c=false));
                  }
              }
             //SurveyStudentHousing.addAll(PropSurvey.Survey_Student_Housing_Space_Types__r);
             //SurveyRetailOptions.addAll(PropSurvey.Survey_Retail_Options__r);
              if(PropSurvey.Queue_Record__r.No_Of_Surveys__c<=1){
                  queue__c QueueRec=new Queue__c(Id=PropSurvey.queue_record__c);
                  DeleteQueueSet.add(QueueRec);
                  if(UpdateQueueMap.containsKey(PropSurvey.queue_record__c)){
                      UpdateQueueMap.remove(PropSurvey.queue_record__c);
                  }
              }else{
                  if(UpdateQueueMap.containsKey(PropSurvey.queue_record__c)){
                      if(PropSurvey.Queue_Record__c!=null){
                     	queue__c QueueRec=new Queue__c(Id=PropSurvey.queue_record__c,No_Of_Surveys__c=UpdateQueueMap.get(PropSurvey.queue_record__c).No_Of_Surveys__c-1); 
                     	UpdateQueueMap.put(PropSurvey.queue_record__c, QueueRec);
                      }
                  }else{
                      if(PropSurvey.Queue_Record__c!=null){
                      	queue__c QueueRec=new Queue__c(Id=PropSurvey.queue_record__c,No_Of_Surveys__c=PropSurvey.Queue_Record__r.No_Of_Surveys__c-1);
                      	UpdateQueueMap.put(PropSurvey.queue_record__c, QueueRec);    
                      }
                      
                  }
              }
         }
             
          for(Queue__c QueueRec:UpdateQueueMap.values()){
             if(QueueRec.No_Of_Surveys__c<=0){
             	DeleteQueueSet.add(QueueRec);
                UpdateQueueMap.remove(QueueRec.Id);
             }
          }
             
             
       //  List<Queue__c> DelQueueList=new List<Queue__c>(DeleteQueueSet);
       //  DelQueueList.addAll(DeleteQueueSet);
         system.debug('new List<Queue__c>(DeleteQueueSet) => '+new List<Queue__c>(DeleteQueueSet));
            
         Delete new List<Queue__c>(DeleteQueueSet);
         update UpdateQueueMap.values();
         Delete SurveyFloorPlanList;
         Delete SurveyAmenityList;
         Delete SurveyContactList;
         //Delete SurveyCareTypes;
         Delete Tenants;
         //Delete SurveyStudentHousing;
         //Delete SurveyRetailOptions;
         Delete scope;
         //update CompanyList;
         update CompanyMap.values();
        // update REISTenantList;
        update REISTenantMap.values();
         //update REISContactList;
         update REISContactMap.values();

     }
    }
     global void finish(Database.BatchableContext BC){
         
     }
}